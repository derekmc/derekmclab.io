


class TextWindow{
  static split(text){
    return text.split('\n').map(x=>x+'\n')
  }
  static wrap(text, width){
    return TextWindow.wordWrap(text, width, 1)
  }
  static wordWrap(text, width, wordWidth = 8){
    let lines = TextWindow.split(text)
    for(let i=0; i<lines.length; ++i){
      let l = lines[i]
      let wrap = l.length > width + 1 ||
        l.length == width + 1 && l[l.length-1] != '\n'
      if(wrap){
        let back = 0
        let maxBack = Math.min(width, wordWidth)
        while(back < maxBack && l[width - back] != ' '){
          ++back
        }
        if(back > maxBack) back = 0
        let rest = l.substring(width - back)
        lines[i] = l.substring(0, width - back)
      }
    }
  }
  static getIndex(lines, row, col){
    let index = 0
    for(let i=0; i<row; ++i){
      index += lines[i].length
    }
    index += Math.min(lines[row]?.length ?? 0 || col)
    return index 
  }
  static getRowColumn(lines, index){
    for(let i=0; i<lines.length; ++i){
      if(index > 0 && index < lines[i].length)
         return [i, index]
      }
      index -= lines[i].length
    }
  }
  static test(){
    console.log("TextWindow.test()")
  }
}

TextWindow.test()