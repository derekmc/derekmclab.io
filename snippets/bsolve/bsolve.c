// flatratecode.com
 
#include <stdio.h>
#include <math.h>

struct solve_data {
  double y;
  double x; // only modify this
  double x0;
  double x1;
  double step;
  double error;
}; 
typedef struct solve_data SolveData;
 
double square(double x);
int bsolve(double f(double), SolveData * data);

int main(int argc, char * argv){
  SolveData data;
  
  data.y = 73;
  data.x0 = 8.0;
  data.x1 = 9.0;
  data.step = 0.01;
  data.error = 0.001 * 0.001;
  data.x = data.x0;
 
  // this is sort of a "fixed point" algorithm,
  // call the same function until it is finished,
  // process the intermediate results.
  while(bsolve(square, &data)){
     printf("\nsquare(%f) = %f", data.x, data.y);
  }
  return 0;
  printf("\nsquare solve finished.\n");
  // solve
  data.y = 0.333;
  data.x = data.x0;
  data.x1 = 10;
  while(bsolve(sin, &data)){
    printf("\nsin(%f) = %f", data.x, data.y);
  }
  printf("\nSolver finished.");
}
 
double square(double x){ return x*x; }
 
// binary solve, find values of x between x0 and x1 that satisfy y = f(x)
int bsolve(double f(double), SolveData * data){
    double sqerr = data->error * data->error;
    double a = data->x + data->error;
    double b = a + data->step;
    do{
       // printf("a, f(a): %f, %f; ", a, f(a));
       double c = (a + b)/2;
       double err1 = f(a) - data->y;
       double err2 = f(b) - data->y;
       if(err1 * err2 < 0){
         if(err1*err1 < sqerr){
           data->x = a;
           return 1;
         }
         if(err2*err2 < sqerr){
           data->x = b;
           return 1;
         }
         double err3 = f(c) - data->y;
         if(err1*err3 < 0){
           b = c;
           continue;  
         } 
         if(err2*err3 < 0){
           a = c;
           continue;
         }
       }
       data->x += data->step;
       a = data->x;
       b = data->x + data->step;
    }while(data->x < data->x1 + data->error);
    return 0;
}
