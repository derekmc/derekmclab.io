#!/usr/bin/env python3

import os
import shutil
import datetime

template_folder = ["templates", "monochrome"]
default_folder = ["templates", "default"]
out_folder = "out"
static_folder = "static"
gentime = datetime.datetime.now().strftime("%a %b %w, %Y - %I:%M %p")


variables = {
  "site_title": "Test Website",
  "page_title": "Test Page",
  "gen_date": gentime
}

sections = {}
pages = {}
indexes = {}

default_sections = {}
default_pages = {}
default_indexes = {}


def preload():
  if os.path.exists(out_folder):
    shutil.rmtree(out_folder)
  shutil.copytree(static_folder, out_folder)

  # Pages: home, article, page, form
  # Sections: head, foot, top, bottom, index
  # Indexes: articles, pages, help

  page_names = ["home", "article", "page", "form"] 
  section_names = ["head", "foot", "top", "bottom", "index"]
  index_names = ["articles", "pages", "help"]

  # Current Template
  for name in page_names:
    try:
      with open(os.path.join(*template_folder, "pages", name + ".html")) as file:
        pages[name] = file.read()
    except:
      pass

  for name in section_names:
    try:
      with open(os.path.join(*template_folder, "sections", name + ".html")) as file:
        sections[name] = file.read()
    except:
      pass

  for name in index_names:
    try:
      with open(os.path.join(*template_folder, "indexes", name + ".html")) as file:
        indexes[name] = file.read()
    except:
      pass

  # Default Template
  for name in page_names:
    try:
      with open(os.path.join(*default_folder, "pages", name + ".html")) as file:
        default_pages[name] = file.read()
    except:
      pass

  for name in section_names:
    try:
      with open(os.path.join(*default_folder, "sections", name + ".html")) as file:
        default_sections[name] = file.read()
    except:
      pass

  for name in index_names:
    try:
      with open(os.path.join(*default_folder, "indexes", name + ".html")) as file:
        default_indexes[name] = file.read()
    except:
      pass



# process_indexes: turn each index into a section. 
def process_indexes():
  sections["pageindex"] = ""

  
# Simple Templates include two types of substitutions: sections, and variables.
#  A section is a subtemplate, and may include variables, but not other sections.
#  A variable is a single value which may be determined during static page generation,
#  dynamic page load, or client side by javascript.

def simple_template(page, sections={}, variables={}):
  # fill in sections
  for name, text in sections.items():
    page = page.replace("%" + name + "%", text) 

  #fill in variables
  for name, value in variables.items():
    page = page.replace("{{" + name + "}}", value)

  return page


def main():
  preload()
  process_indexes()
  print("pages", pages.keys())
  print("sections", sections.keys())
  page_list = ["home"]
  file_list = [os.path.join(out_folder, "index.html")]
  for i in range(len(page_list)):
    pagename = page_list[i]
    filename = file_list[i]
    with open(filename, "w") as file:
      page = simple_template(pages[pagename], sections=sections, variables=variables)
      file.write(page)
      print(page)
      print(f"page '{pagename}' written to '{filename}'.")

if __name__ == "__main__":
  main()
