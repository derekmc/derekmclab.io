// message based game design
// - send
// - receive

const InBrowser = (typeof window !== "undefined");

let queue = [];
let users = [];
let callbacks = [];

const DialogDelay = 2000;
const MinStones = 5;
const MaxStones = 13;

function sendfrom(from, to, text){
  if(to === null || to === undefined || to < 0){
    broadcast(from, text);
    to = 0;  // send to host as well.
  }
  from = from ?? 0;
  let message = [from, to, text];
  queue.push(message);
}

async function process(){
  while(queue.length){
    let message = queue.shift();
    if(!message) break;

    let [from, to, text] = message;
    let handle = users[to];
    if(handle){
      let response = await handle(from, text);
      if(response){
        console.log('finished processing messages: ', response);
        break;
      }
    }
  }
  return true;
}

// by default does not send to host (user 0)
function broadcast(from, text){
  for(let i=1; i<users.length; ++i){
    send(from, i, text);
  }
}

function start(){
  sendfrom(0, 0, "start");
  process();
}
function toX(s){
  return JSON.parse(s);
}
function toS(x){
  return JSON.stringify(x);
}

function nimHost(playernumber, pubstate, privstate){
  let send = (to, text) => sendfrom(playernumber, to, text)
  return async function nimHost(from, text){
    if(text == "start"){
      await Alert("Welcome to nim!");
      let state = {
        turn: 1,
        stones: MinStones + Math.floor((MaxStones - MinStones)* Math.random())
      };
      send(1, toS(state));
    }
    if(text == "done"){
      await Alert("Game finished.");
    }
  }
}

function nimUser(playernumber, pubstate, privstate){
  let send = (to, text)=> sendfrom(playernumber, to, text)
  return async function(from, text){
    let state = toX(text);
    //await Alert("Turn: " + state.turn);
    let take;
    if(state.stones > 1){
      take = await ChooseIndex(
        `Nim Player ${playernumber}\n Stones remaining: ${state.stones}`,
        "Take 1", "Take 2");
    }else if(state.stones > 0){
      take = await ChooseIndex(
        `Nim Player ${playernumber}\n Stones remaining: ${state.stones}`,
        "Take 1");
      await Alert(`You lost. (Player ${playernumber})`);
    }else{
      await Alert(`You won! (Player ${playernumber})`);
      await Alert(`Restarting...`);
      send(0, "start");
      return;
    }

    state.stones -= take;
    ++state.turn;
    let next = state.turn;
    while(next > (users.length-1)) next -= (users.length-1);
    send(next, toS(state));
  }
}

function nimAI(playernumber, pubstate, privstate){
  let send = (to, text)=> sendfrom(playernumber, to, text);
  return async function(from, text){
    let state = toX(text);
    //await Alert("Turn: " + state.turn);
    let take;
    let n = state.stones;
    if(n > 1){
      if(n % 3 == 1) take = Math.floor(2*Math.random()) + 1;
      if(n % 3 == 2) take = 1;
      if(n % 3 == 0) take = 2;
    }else if(n > 0){
      take = 1;
    }else{
      await Alert(`The AI won! (Player ${playernumber})`);
      await Alert(`Restarting...`);
      send(0, "start");
      return;
    }

    await Alert(
      `Nim AI (Player ${playernumber})\n` +
      `Stones remaining: ${state.stones}\n` +
      `\n` + 
      `The AI takes ${take} stones.`);

    //${state.stones}\n The AI takes ${take} stones.`);

    state.stones -= take;
    if(state.stones == 0)
      await Alert(`The AI lost. (Player ${playernumber})`);
    ++state.turn;
    let next = state.turn;
    while(next > (users.length-1)) next -= (users.length-1);
    send(next, toS(state));
  }
}


function escapeHTML(text){
  return text.replaceAll("&", "&#38;")
             .replaceAll("<", "&lt;")
             .replaceAll(">", "&gt;")
             .replaceAll("\n", "<br>\n");
}


function Sleep(n){
  if(!n) n = 1;

  return new Promise((y, n)=>{
    setTimeout(y, n*1000);
  })
}

function Alert(text){

  if(InBrowser)
    document.body.innerHTML = `
      <div>${escapeHTML(text)}</div>
      <br>
      <button onclick="callbacks[0]();">Okay</button>
      <!-- <button onclick="callbacks[0]();">Cancel</button> -->
    `
  else{
    console.log(`Message: "${text}"`);
    setTimeout(callbacks[0](), DialogDelay);
  }

  callbacks = [];
  return new Promise((resolve, reject)=>{
    callbacks[0] = ()=>{
      document.body.innerHTML="Alert finished.";
      resolve();
    }
  })
}

function list(items, tmpl, sep){
  if(!tmpl) tmpl = (x => x);
  if(!sep) sep = "";
  return items.map(tmpl).join(sep);
}

async function ChooseText(text, ...options){
  let [answer, _] = await Choose(text, ...options);
  return answer;
}

async function ChooseIndex(text, ...options){
  let [_, i] = await Choose(text, ...options);
  return i;
}

function Choose(text, ...options){

  callbacks = [];

  function optionTemplate(text, index){
    return `
      <button onclick="callbacks[${index}]()">${text}</button>
    `
  }

  if(InBrowser)
    document.body.innerHTML = `
      <div> ${escapeHTML(text)} </div>
      <p>
        ${list(options, optionTemplate, "\n")}
      </p>
      <!--<li>
        <button onclick="callbacks[${options.length}]()">Cancel</button>
      </li>-->
    `
  else{
    console.log(`Choose:\n ${list(options, (x,i)=>`${i}: ${x}`, "\n")}`);
    let choice = Math.floor(Math.random() * options.length) + 1;
    setTimeout(()=> callbacks[choice](), DialogDelay);
  }

  return new Promise((resolve, reject)=>{
    let i;
    for(i=0; i<options.length; ++i){
      callbacks[i] = ((choice)=>{
        let j;
        for(j=0; j<options.length; ++j){
          if(choice == options[j]) break;
        }
        return ()=> resolve([choice, j+1]);
      })(options[i])
    }
    callbacks[i] = reject;
  })
}

function init(){
  //let pubstate = {};
  let playernumber = 0;
  users = [
    nimHost(0),
    nimUser(++playernumber),
    nimAI(++playernumber),
  ];
  start();
}


function test(){
  console.log("Message Game Test.");
  init();
}

if(InBrowser)
  window.addEventListener("load", init);
else
  test();
