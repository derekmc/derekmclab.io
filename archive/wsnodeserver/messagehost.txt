﻿// This is the pseudo-code for a message game host.
// It is “table” based.
// 
// There is a custom programming language for defining rules.
// It is entirely command expression based.  Each host message
// peer messages are simply passed between peers.
//
//

let game = {
  hostRules: [rule language],
  playerRules: [rule language],
}

let table = {
  seats: [websocket]
}
  

let tables = {};

function newTable(){
  
}

// assigned a seat
function joinTable(){
}

function leaveTable(){
}

function assignHost(table){
}


