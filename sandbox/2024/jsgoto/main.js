
let rl = require('readline-promise').default.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true,
});

function ask(message){
  return rl.questionAsync(message);
}

// instead of callbacks, these use "sendforwards"
// sendforwards prefer use of global variables,
// and they terminate further exectution in the current
// context by throw a "task abort" error.

let {BEGIN, END, EXIT, Alert, Confirm, Prompt, Choose, ChooseGoto, Wait, Delay, DelayMessage, Goto} = (()=>{
  //let [Alert, Confirm, Prompt, Choose, END] = (()=>{
  //let targets = []; // global
  __Goto.BEGIN = BEGIN;
  __Goto.END = END;
  __Goto.EXIT = EXIT;
  __Goto.Alert = Alert;
  __Goto.Confirm = Confirm;
  __Goto.Prompt = Prompt;
  __Goto.Choose = Choose;
  __Goto.Wait = Wait;
  __Goto.Delay = Delay;
  __Goto.DelayMessage = DelayMessage;
  __Goto.Goto = __Goto;

  return __Goto;

  function BEGIN(target){
    wrapTask(target)();
  }

  function EXIT(){
    let e = new Error("Goto.EXIT called.  Terminating Program.");
    e.task = "EXIT";
    e.taskFinished = true;
    throw e;
  }


  function END(){
    let e = new Error("Goto.END called.  All tasks finished.");
    e.task = "END";
    e.taskFinished = true;
    throw e;
  }

  function die(task_name){
    let e = new Error(`\"Goto.${task_name}\" aborted further actions.`);
    e.task = task_name;
    e.taskFinished = true;
    throw e;
  }

  // TODO, this die stuff doesn't work, because everything is promises.
  function __Goto(target){
    target();
    die("Goto");
  }

  function wrapTask(target, arg){
    return () => {
      try{
        target(arg);
      } catch(e){
        if(!e.hasOwnProperty('task') || !e.hasOwnProperty('taskFinished')){
          console.log('non-task error');
          throw e;
        } else if(e.task == "EXIT"){
          console.log("Program terminated.");
          process.exit(0);
        }
      }
    }
  }



  function Alert(message, after){
    message += " \n";
    if(typeof after == "number"){
      return new Promise((yes, no)=>{
        setTimeout(wrapTask(next), after * 1000);
        function next(){
          console.log(message);
          yes();
        }
      })
    } else if(typeof after == "function"){
      console.log(message);
      after();
      die("Alert");
    }
  }
    
  function Confirm(message, yes, no){
    ask(message + " (Y/n) ").then(x => {
      if(x.length == 0 || x.trim().toLowerCase() == "y"){
        wrapTask(yes)();
      } else {
        wrapTask(no)();
      }
    })
    die("Confirm");
  }

  function Wait(timeout, after){
    (new Promise((yes, no)=>{ setTimeout(yes, timeout*1000) })).then(()=>{
      wrapTask(after)();
    })
    die("Wait");
  }


  function DelayMessage(timeout, message){
    setTimeout(()=>console.log(message), 1000 * timeout);
  }

  // this does not abort the current execution context.
  function Delay(timeout, after){
    if(typeof after == "string"){
      DelayMessage(timeout, after);
    } else if (typeof after == "function"){
      (new Promise((yes, no)=>{ setTimeout(yes, 1000 * delay) })).then(()=>{
        wrapTask(after)();
      })
    }
  }

  function Prompt(message, after){
    ask(message + " ").then(x => {
      wrapTask(after, x)();
    })
    die("Prompt");
  }
  function ChooseGoto(message, ...choices){
    let alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let longmessage = "Choose a " + message + "\n" + choices.map((x,i) => `  ${alpha[i]} : ${x}`).join('\n') + "\n > ";
    ask(longmessage).then(x => {
      let name, branch;
      for(let i=0; i<choices.length; i+=2){
        name = choices[i];
        branch = choices[i+1];
        if(x.toUpperCase() == alpha[i/2]){
          break;
        }
      }
      if(branch === undefined){
        // repeat selection if choice is invalid.
        return wrapTask(()=>{ChooseGoto(message, ...choices); })();
      } else {
        wrapTask(after,val)();
      }
    })
    die("Choose");
  }

  function Choose(message, after, ...choices){
    let alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let longmessage = "Choose a " + message + "\n" + choices.map((x,i) => `  ${alpha[i]} : ${x}`).join('\n') + "\n > ";
    ask(longmessage).then(x => {
      let val = null;
      for(let i=0; i<choices.length; ++i){
        if(x.toUpperCase() == alpha[i]){
          val = choices[i]
          break;
        }
      }
      if(val == null){
        // repeat selection if choice is invalid.
        return wrapTask(()=>{Choose(message, after, ...choices); })();
      } else {
        wrapTask(after,val)();
      }
    })
    die("Choose");
  }
})()


// note that goto functions should never take parameters.
start = ()=>{
  DelayMessage(0, "\nWelcome to the number guessing game.");
  DelayMessage(1.8, "The game will begin in");
  DelayMessage(2.1, "\n 3...");
  DelayMessage(3.1, "\n 2..");
  DelayMessage(4.1, "\n 1.\n");
  Wait(5.1, settings);
}
settings = ()=>{
  Choose("Maximum Value", reset, 3, 10, 100, 1000);
}
reset = (n)=>{
  N = n
  X = 1 + Math.floor(Math.random() * N)
  R = Math.floor(Math.log(n)/Math.log(2)) + 1;
  Wait(0.5, guess);
}

guess = ()=>{
  Prompt(`\nGuess the number, up to ${N}:`, test);
}

test = (g)=>{
  if(g == X) win()
  else if(--R == 0) lose();
  else Alert((g < X? "Higher. " : "Lower. ") + R + " Tries Remain.", guess);
}

//await 
win = ()=>{
  Alert("You won.  Congratulations!", playagain);
}

lose = ()=>{
  Alert("You lost. Better Luck Next Time.", playagain);
}

playagain = ()=>{
  Confirm("Play Again? ", settings, credits);
}

credits = ()=>{
  DelayMessage(0.5, "\n\nThank you for playing.");
  DelayMessage(2.0, "\nCopyright 2021 -- DM");
  DelayMessage(3.5, "\nTHE END");
  DelayMessage(5.0, "\n...");
  DelayMessage(6.5, "\nNo really, it is.\n\n");
  Wait(9.0, EXIT);
}


BEGIN(start);
