
let level = require('level');
let dbname = "app-db";

let dbwrap = (_db)=>{
  let x = {};

  x.get = async (k, defaultvalue) =>{
    try{
      return JSON.parse(await _db.get(k));
    } catch(e){
      return defaultvalue;
    }
  }
  x.add = async (k, v) => {
    try{
      if(!await _db.get(k)){
        await _db.put(k, v);
        return true;
      }
      return false;
    } catch(e){
      await _db.put(k, v);
      return true;
    }
  }
  x.append = async (k, v, divider) => {
    if(!divider) divider = "; ";
    let oldval = await _db.get(k) ?? "";
    if(oldval.length) oldval += divider;
    return await _db.put(k, oldval + v);
  }
  x.put = async (k, v) => {
    return await _db.put(k, JSON.stringify(v));
  }
  x.del = async (k) =>{
    return await _db.del(k);
  }
  return x;
};




main();

async function main(){
  let db = dbwrap(level(dbname));
  //console.log('this', this);
  //console.log(db);

  //await db.del('a');
  //let a = await db.get('a');
  //console.log("\na: " + a);
  await db.put('a', {b: {c: [1,2,3]}});
  let a = await db.get('a');
  console.log("\n get a: " + JSON.stringify(a));

  return;
}
