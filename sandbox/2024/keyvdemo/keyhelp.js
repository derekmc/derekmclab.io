
module.exports = {
  tableKey: (table, index)=>
    `${table}|${index}`,
  getRefKey: async (refname, refval, lookup)=>{
    let reftext = `ref:${refname}=${refval}`
    return await lookup(reftext);
  },
  setRefKey: async (refname, refval, key, assign)=>{
    let reftext = `ref:${refname}=${refval}`
    await assign(reftext, key);
  }
}
