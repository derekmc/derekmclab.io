import java.awt.event.*;
import javax.swing.*;

public class Demo{
  public static void main(String[] args){
    JFrame f = new JFrame();
    JTextField text = new JTextField();
    text.setBounds(100,300,120,60);
    char c = 'a';
    JButton b = new JButton("click");
    b.addActionListener( new ActionListener(){
      int clicks = 0;
      public void actionPerformed(ActionEvent e){
        text.setText("Button Clicked: " + (++clicks));
      }
    });
    b.setBounds(100,100,80,40);
    f.add(b);
    f.add(text);
    f.setSize(400,500);
    f.setLayout(null);
    f.setVisible(true);
  }
}
