
if('serviceWorker' in navigator){
  window.addEventListener("load", loadsw);
}

function loadsw(){
  navigator.serviceWorker.register("./app/sw.js").then((registration)=>{
    console.log('sw registered. scope: ', registration.scope);
  }, (err)=>{
    console.log("registration failed.", err);
  })
}
