VERBOSE = False
import random

# This program searches for an optimal solution to a linear program
# using three techniques: random dartboard, random walk(inbounds),
# and a random ascent "hiking" algorithm

def dot(a, b):
    z = 0
    n = max(len(a), len(b))
    for i in range(n):
       x = (a[i] if i < len(a) else 0)
       y = (b[i] if i < len(b) else 0)
       z += x*y
    return z


def constraint(x):
    A = [
        [1, 1, 0, 1],
        [0, 2, 1, 0.4],
        [2, 0, 0, 2],
        [1, 1, 1, 1]
    ]
    B = [5, 7, 5, 12]

    assert len(A) == len(B)

    # x_i > 0
    for a in filter(lambda a: a < 0, x):
        return False


    for i in range(len(A)):
        a = A[i]
        b = B[i]
        y = dot(a, x)
        if y > b:
            return False

    return True

def objective(x):
    C = [0.4, 1, 1.1, 2.5]
    return dot(C, x)


# Search 
def dartSearch(n):
  best = -99999
  best_x = None
  max_x = 12 
  xlen = 4

  print("\nDartboard search!")
  for i in range(n):
    x = [random.uniform(0, max_x) for i in range(xlen)]
    if constraint(x):
      #print("in bounds: " + ", ".join(map(str, x)))
      val = objective(x)
      if val > best:
        if VERBOSE: print("dart best: " + str(val))
        best = val
        best_x = x.copy()
    else:
      pass
      # print("out of bounds: " + ", ".join(map(str, x)))

  
  print("dart best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))

# randomly walk within the bounds
def walkSearch(n):
    best = -99999
    best_x = [0, 0, 0, 0]

    delta = 0.03
    x = [0, 0, 0, 0]
    print("\nRandom walk search!")
    for i in range(n):
      next_x = x.copy()
      for j in range(len(x)):
        if bool(random.getrandbits(1)):
          next_x[j] += delta
        else:
          next_x[j] -= delta
      if constraint(next_x):
        x = next_x
        val = objective(x)
        if val > best:
          best = val
          best_x = x
          if VERBOSE: print("walk best: " + str(val))
          # print("best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))

    print("walk best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))

"""
"""
# randomly ascend within the bounds
def hikeSearch(n):
    best = -99999
    best_x = [0, 0, 0, 0]

    delta = 0.03
    x = [0, 0, 0, 0]
    print("\nRandom hike search!")
    for i in range(n):
      next_x = x.copy()
      for j in range(len(x)):
        if bool(random.getrandbits(1)):
          next_x[j] += delta
        else:
          next_x[j] -= delta
      if constraint(next_x):
        val = objective(next_x)
        if val > best:
          # very similar to walk search, except we keep the last value unless we do better.
          x = next_x
          best = val
          best_x = x
          if VERBOSE: print("hike best: " + str(val))
          # print("best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))

    print("hike best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))

def progressiveHike(n):
    best = -99999
    best_x = None
    restriction = 0.85

    delta = 0.03
    x = [0, 0, 0, 0]
    print("\nProgressive hike search!")
    for i in range(n):
      next_x = x.copy()
      for j in range(len(x)):
        if bool(random.getrandbits(1)):
          next_x[j] += delta
        else:
          next_x[j] -= delta
      if constraint(next_x):
        val = objective(next_x)
        if val > restriction * best:
          # very similar to hike search, except we relax the limits on walking
          x = next_x
        if val > best:
          best = val
          best_x = x
          if VERBOSE: print("hike best: " + str(val))
          # print("best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))

    print("progressive hike best: " + ", ".join(map(str, best_x)) + "; val: " + str(best))





def main():
  n = 500*1000
  dartSearch(n)
  walkSearch(n)
  hikeSearch(n)
  progressiveHike(n)

main()

