
module.exports = {
  DefaultDir: "logdb",
  DefaultDB: "logdb",
  BackupExtension: ".backup",
  MaxLogLen: 1000,
}

