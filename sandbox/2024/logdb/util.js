
module.exports = {
  assert: (test, message) => {
    if(!test) throw new Error("Assertion failed: " + message);
  },
  isInt: (x) => {
    return !isNaN(x) && Math.round(x) == x;
  },
  inBounds: ([min, max], value) => {
    // console.log(`inbounds (${min}, ${max}, ${value})`);
    return value >= min && value < max;
  },
  parse: (s)=>{
    try{ return JSON.parse(s); }
    catch(e){
      if(("" + s).trim().length)
        console.warn(`Failed parsing json: ${s}`);
      return null;
    }
  },
}

