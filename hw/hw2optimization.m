#disp('here')
A = [1 1 1 1; 1 -1 -1 1; 0 1 0 1; -1 -1 1 1];
x = transpose(A) \ [0; 2; -3; 4]
[1 1 0 -1] * x
[1 -1 1 -1] * x


function problem3_2_1_i
  A = [1 1 1 1; 1 -1 -1 1; 0 1 0 1];
  null(A);
  AT = transpose(A);
  I = eye(4);
  P = I - AT*(A * AT)^-1 * A
  x1 = [1; 3; 1; 2];
  x2 = [0; -2; -3; 4];
  p1 = P*x1
  q1 = x1 - p1
  p2 = P*x2
  q2 = x2 - p2
endfunction

function problem3_2_1_iii
  A = [1 1 1 1; 1 -1 -1 1];
  B = null(A)
  AT = transpose(A);
  I = eye(4);
  P = I - AT*(A * AT)^-1 * A
  x1 = [1; 1; 1; 1];
  x2 = [1; -1; -1; 1];
  p1 = P*x1
  q1 = x1 - p1
  p2 = P*x2
  q2 = x2 - p2
endfunction

function problem3_2_1_ii
  A = [1 1 1 1];
  null(A)
  AT = transpose(A);
  I = eye(4);
  P = I - AT*(A * AT)^-1 * A
  x1 = [-2; 4; 5; -2];
  x2 = [7; 5; -13; 1];
  p1 = P*x1
  q1 = x1 - p1
  p2 = P*x2
  q2 = x2 - p2
endfunction

problem3_2_1_iii()



      