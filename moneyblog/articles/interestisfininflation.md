[Derek's Money Blog](./index.html)

# Interest: A Financially Indexed Inflation Measure

A lot of consideration is given to the question of 
how: by what mechanism, or how much: to what degree,
interest rates affect consumer inflation.

Much less attention is paid to a question of how interest and inflation are definitionally necessarily related in a specific quantitative way.
Specifically, interest is by definition a financial index of the depreciation or inflation of the money unit.
Interest is the inflation of the money unit compared
to some financial asset or financial assets in general.

### All Measurement is relative

Interest is the relative measurement of inflation
to the money unit indexed against financial assets. Just like inflation can be measured by indexing against consumer goods or foreign currencies, it can similarly be indexed against a set of particular financial assets, and their average rate of interest is precisely and definitionally the rate of inflation or depreciation to the money unit in relative terms.

If interest is the amount that a financial asset appreciates,
it necessarily means that it is the amount that the money
unit depreciates relative to that financial asset.

This relativity of measurement applies to all forms of measurement,
not just money and financial assets. If a basketball hoop is 10
feet tall, then the foot, as a unit, is one tenth of a basketball
hoop short. If 8 hours is a third of a day, then three 8 hour
periods fill up a day.

The same is true for financial assets and their change in
relative value. If a stock yields 10 percent, then you
lose 10% relative value by holding physical cash instead.
 

## Can We Decrease Consumer Inflation By Increasing Financial Inflation?

The question of the "how" and "how much", between
interest rates and inflation, thus becomes a question
of whether we can cleverly increase one kind of inflation
to decrease another kind of inflation. Can we decrease
consumer inflation by increasing financially indexed inflation?

The fisher equation tells us that this is theoretically
possible. An increase to nominal interest rates, 
which is accompanied by an even larger change to
cpi adjusted rates, called the "real" rate of interest,
would involve a reduction in consumer inflation.

So it is possible in some circumstances for one kind
of inflation to correspond with, or even potentially cause,
a decrease in another type of inflation.

However, the important part of this is "in some circumstances",
Brett Johnson has popularly hypothesized that, for example,
consumer inflation in the US Dollar, can correspond with a strengthening
of the dollar against foreign currencies, as inflation is often a global
phenomenon, and relatively stable and wealthy countries like the
US have many kinds of advantages under these conditions.

So even though consumer inflation increases, the measure of the dollar
may do well against a basket of foreign currencies.

## Duration vs Credit Restriction

Conventionally, interest rates have been seen as a tool to effect
credit restriction. However, I would argue that this can only
be a short term effect immediately around the period of a change
in interest rates. This is because credit deals with relative
purchasing power over a fixed amount of real goods and services
in the economy.  So while people may be retiscent to borrow
immediately after a rate hike, or wait if they expect an imminent
cut, they will not have this same reaction to a high interest
rate that is constant over the long run.

Another possible mechanism for interest rates or financially indexed inflation
to interact with consumer inflation, is what is known as duration.
The most direct example of duration is simply treasury bonds,
as they are assets typically that pay out a fixed amount upon
their maturity.  But even assets like homes may experience a loss
of equity value during an increase in interest rates, as 
potential purchasers on credit may not be able to afford as high
of a sticker price, if a greater portion of the total cost
of puchase goes to interest payments.

It is difficult to find a mechanism or possibility whereby
financially indexed inflation and consumer inflation can move in
opposite directions over the long run.  For this reason,
I am generally skeptical of the current approach to monetary
policy involving extreme changes to interest rates such
as suggested by the Taylor rule and similar ideas.

Instead, I view rate increases as a way to potentially
delay or smooth out inflation pressures over the short
run, while more direct approaches can be used over the
long run.  Because when you gain money in financial assets,
then money itself is losing relative value, and it's hard
for consumer deflation, to keep outpacing an elevated interest rate or rate of
financially indexed inflation, as that sets an unreasonably high standard for
cpi adjusted financial returns.
