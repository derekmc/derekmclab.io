window.ConfigJS = ()=>{
  let cfg = {};


  // helper functions
  const id = x=>document.getElementById(x);
  const remove = x=> x.parent.removeChild(x);
  const clearDiv = x=>x.innerHTML = '';

  const configDivId = "configjs-div";
  const buttonClass = "configjs-button";
  const cancelButtonClass = "configjs-button-cancel";
  const selectedClass = "configjs-selected";
  const cmp = (x, y)=> x < y? -1 : 1;
  const LongListCutoff = 18;

  let container = null;
  let choices = null;
  let currentItem = null; 
  let updateCallback = null;

  let configStyle = `
    <style>
      #configjs-div{
        position: absolute;
        top: 0; left: 0;
        bottom: 0; right: 0;
        overflow: auto;
        display block;
        color: blue;
        background: rgba(255,255,255,0.75);
        text-align: center;
        user-select: none;
        cursor: default;
        font-family: sans-serif;
      }
      #configjs-div .configjs-footer{
        background: rgba(30,30,30,0.75);
        height: 90%;
      }
      #configjs-div h1, #configjs-div h3{
        background: black;
        color: white;
        margin: 0;
        padding: 6px;
      }
      #configjs-div button{
        background: black;
        color: white;
        border: none;
        padding: 4px 10px;
        margin: 3px;
        font-weight: bold;
        border-radius: 9px;
        border: 2px solid black;
      }
      #configjs-div button.configjs-button-cancel{
        background: #446;
        border: 3px solid #446;
      }
      #configjs-div button.configjs-selected{
        background: #006;
        border: 3px solid #88f;
      }
    </style>
  `

  // for a long list, place done or back button at top and bottom of options.
  let mainMenu = ()=>`
    <div id='configjs-div'>
      <h1> Settings </h1>
      <h3> Choose Variable </h3>
      ${(Object.keys(choices).length < LongListCutoff)? "" : `<button class='${cancelButtonClass}'>Back</button>`}
      <br>

      ${Object.keys(choices).map((name,i)=>`<button class='${buttonClass}'>${name}</button>`).join('')}
      <br>
      <button class='${cancelButtonClass}'>Done</button>
      <br><br><br>
      <div class='configjs-footer ${cancelButtonClass}'>
        &nbsp;
      </div>
    </div>
    ${configStyle}
  `
  let itemMenu = ()=>`
    <div id='configjs-div'>
      <h1> '${currentItem}' </h1>
      <h3> Choose Value </h3>
      ${(choices[currentItem].length < LongListCutoff)? "" : `<button class='${cancelButtonClass}'>Back</button>`}
      <br>

      ${choices[currentItem].slice(0).sort(cmp).map((name)=>`<button class='${buttonClass + (name == choices[currentItem][0]? ' ' + selectedClass : '')}'>${name}</button>`).join('')}
      <br>
      <button class='${cancelButtonClass}'>Back</button>
      <br><br><br>
      <div class='configjs-footer ${cancelButtonClass}'>
        &nbsp;
      </div>
    </div>
    ${configStyle}
  `


  cfg.choices = (x)=>{
    if(x) choices = x;
    return choices;
  }
  cfg.container = (x)=>{
    if(x) container = x;
    return container;
  }
  cfg.currentItem = (x)=>{
    if(x) currentItem = x;
    return currentItem;
  }
  cfg.updateCallback = (x)=>{
    if(x) updateCallback = x;
    return updateCallback;
  }


  cfg.init = ()=>{
    if(!container){
      container = document.createElement("div");
      document.body.appendChild(container);
    }
    showMenu();
  }
  cfg.hide = ()=>{
    container.style.display = "none";
  }
  cfg.show = ()=>{
    currentItem = null;
    showMenu();
  }
  cfg.destroy = ()=>{
    clearDiv(container);
  }
  return cfg;

  function registerButtons(){
    let buttons = document.getElementsByClassName(buttonClass);
    for(let i=0; i<buttons.length; ++i){
      let b = buttons[i];
      b.addEventListener("click", configButton);
    };
    let cancel_buttons = document.getElementsByClassName(cancelButtonClass);
    for(let i=0; i<cancel_buttons.length; ++i){
      let b = cancel_buttons[i];
      b.addEventListener("click", cancelButton);
    };
  }
  function showMenu(name){
    if(!name){
      container.innerHTML = mainMenu();
      currentItem = null;
      id(configDivId).scrollIntoView();
      window.scrollTo(0,0);
    } else {
      currentItem = name;
      container.innerHTML = itemMenu();
      let selected = document.getElementsByClassName(selectedClass);
      if(selected.length){
        selected[0].scrollIntoView();
        id(configDivId).scrollBy(0, -window.innerHeight/2);
        window.scrollBy(0, -window.innerHeight/8);
      } else {
        id(configDivId).scrollIntoView();
        window.scrollTo(0,0);
      }
    }
    registerButtons();
    setHeight();
  }
  function setHeight(){
		var body = document.body,
    html = document.documentElement;

		var height = Math.max( body.scrollHeight, body.offsetHeight, 
													 html.clientHeight, html.scrollHeight, html.offsetHeight );
    id(configDivId).style.height = (height + 100) + 'px';
  }

  function cancelButton(e){
    e.preventDefault();
    if(currentItem == null){
      cfg.hide();
      let config_result = {};
      for(let k in choices){
        config_result[k] = choices[k][0];
      }
      updateCallback(config_result);
      return;
    } else {
      showMenu();
    }
    return null;
  }

  function configButton(e){
    e.preventDefault();
    let name = e.target.innerHTML.toLowerCase();
    if(currentItem === null){
      showMenu(name);
    } else {
      let array = choices[currentItem];
      do{
        array.unshift(array.pop());
      }while(array[0] != name);
      showMenu(currentItem);
    }
    return null;
  }

  // TODO  keyboard events
  function keydown(e){
  }
}
