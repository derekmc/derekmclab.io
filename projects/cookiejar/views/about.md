## Cookie Jar
### A Distributed Custodial Based Ledger Payment System.


With cookiejar, the network is distributed through independently hosted custodial chains.
You can transfer token accounts between custodians as you wish, according to which
custodians you trust.
This is much like conventional banking, in that you take your funds out of one
bank, and put them in another, except with a few key differences:

 * Every token account has an issuer and a custodian.
 * Custodians track token transfers, on same site chain or to other chains.
 * All custodians make their database public, albeit with user anonymized data.
 * Custodial accounts are automatically reverted to an account's custodial
   successor, should the current custodian go offline.


### Advantages Over Traditional Banking

Cookie Jar has many advantages over traditional banking:

 * Custodial databases are shared publicly and can be audited and backed up.
 * Custodial successorship is automatic, unlike traditional bank failure,
   if a custodian goes offline, your accounts are automatically reverted
   to a custodial successor.
 * Everything can run automatically and digitally, with minimal cost and overhead
   using open source software.

### Advantages Over Cryptocurrencies

Cookie Jar has many advantages over popular established cryptocurrencies:

 * Any unit of account can be used.
 * Instant payments with the custodian.
 * Payment infrastructure can be localized, no need to rely on a global chain.
 * Local custodians can survive network fractures or outages, and even operate offline.
 * Develops trust networks which can integrate with governance and financing efforts.
 * Old history can be removed, once a token's lifecycle completes.


### Chainless Tokens

Additionally, cookie jar supports "chainless tokens" which are token accounts
tied to a hash.  In order to "onchain" these accounts, a custodian claims the
coins by incrementally revealing the prehash.  You can hold chainless token
accounts indefinitely without a custodian, storing only the pre-hash secret.

