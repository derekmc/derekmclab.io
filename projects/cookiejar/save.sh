#!/bin/sh
wget --recursive \
     --no-clobber \
     --page-requisites \
     --html-extension \
     --convert-links \
     --directory-prefix=save \
     --restrict-file-names=windows \
     --no-parent localhost:8081

