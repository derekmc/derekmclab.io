
let level = require('level');
let constants = require('./constants.js');
let keys = constants.keys
let dbname = "cookiejar-db";
let passwordHash = require("password-hash");


function clean(str){
  return str.replace(/[^A-Za-z_0-9]/g, "");
}

let dbwrap = (_db)=>{
  let x = {};

  x.get = async (k, defaultvalue) =>{
    try{
      return JSON.parse(await _db.get(k));
    } catch(e){
      return defaultvalue;
    }
  }
  x.add = async (k, v) => {
    try{
      if(!await _db.get(k)){
        await _db.put(k, v);
        return true;
      }
      return false;
    } catch(e){
      await _db.put(k, v);
      return true;
    }
  }
  x.append = async (k, v, divider) => {
    if(!divider) divider = "; ";
    let oldval = await _db.get(k) ?? "";
    if(oldval.length) oldval += divider;
    return await _db.put(k, oldval + v);
  }
  x.put = async (k, v) => {
    return await _db.put(k, JSON.stringify(v));
  }
  x.del = async (k) =>{
    return await _db.del(k);
  }
  return x;
};

let db;
try{
  db = dbwrap(level(dbname));
} catch(e){
  console.error(e);
}

function randomString(len, chars){
  if(!chars) chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let result = "";
  for(let i=0; i<len; ++i){
    result += chars[Math.floor(Math.random() * chars.length)]; }
  return result;
}

async function newSession(username){
  if(!username) username = ""; // guest
  let cookie = randomString(constants.CookieLen);
  let k1 = keys.usersessions + username;
  let k2 = keys.cookies + cookie;
  if(username.length){
    db.append(k1, cookie, ",");
  }
  db.put(k2, username);
  return cookie;
}

// returns username for logged in users, and cookie for guests.
async function userId(cookie){
  let username = await db.get(keys.cookies + cookie);
  return username && username.length? username : cookie;
}

// returns [ cookie, {global, user, users: {userid: userdata}} ]
async function appData(appname, cookie){
  let userid;
  if(cookie){
    userid = await userId(cookie);
  } else {
    cookie = await newSession("");
    userid = cookie;
  }
  let result = {
    global : null,
    userdata : null,
    users : {},
  }
  let userdata = await db.get(keys.appuserdata + appname + "|" + userid, "");
  let globaldata = await db.get(keys.appdata + appname, "");
  result.userdata = userdata;
  result.users[userid] = userdata;
  return [cookie, result];
}

// returns [ success, message ]
async function newUser(username, password, confirmpassword){
  if(password != confirmpassword){
    return [false, "Passwords do not match"]; }

  let key = keys.userlogin + username;
  let passhash = passwordHash.generate(password);
  let login = {username, passhash};

  console.log('login, key', login, key);
  if(!await db.add(key, login)){
    return [false, "User already exists."]; }

  return [true, `User '${clean(username)}' added. Please login.`];
}

// returns [ cookie, message ]
async function userLogin(username, password, cookie){
  let key = keys.userlogin + username;
  let login = await db.get(key);
  console.log('login, key', login, key);
  if(!login){
    return [false, `No such user "${clean(username)}".`];
  }
  if(username != login.username){
    return [false, "Unexpected stored username mismatch."];
  }
  if(!passwordHash.verify(password, login.passhash)){
    return [false, "Incorrect password."]
  }
  if(cookie){
    [cookie, _] = await userLogout(cookie);
  } else {
    cookie = await newSession(username);
  }

  await db.append(keys.usersessions + username, cookie);
  await db.put(keys.cookie + cookie, username);
  
  return [cookie, `User ${clean(username)} login correct.`];
}

// returns a new session cookie
async function userLogout(cookie){
  await db.put(keys.cookie + cookie, "");
  return await newSession();
}


let data = {
  newSession,
  userId, newUser,
  userLogin, userLogout,
  appData, clean
}
  

module.exports = data;

