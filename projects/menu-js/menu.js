let Menu = {};

__init_menu__(Menu);

function __init_menu__(module){

  const Delay = 60;
  module.Alert = Alert;
  module.AlertTry = AlertTry;
  module.Choose = Choose;
  module.Confirm = Confirm;
  module.Prompt = Prompt;
  module.PromptFile = PromptFile;
  //module.__menu_resolve__ = menuResolve;
  //module.__menu_abort__ = menuAbort;
  
  module.__closeAlert = closeAlert;
  module.__selectChoice = selectChoice;
  module.__handlePrompt = handlePrompt;
  module.__cancelPrompt = cancelPrompt;
  module.__affirmConfirm = affirmConfirm;
  module.__denyConfirm = denyConfirm;
  module.activeWindow = "";

  //let option_list = [];
  let choice_names = [];
  let choice_select = null;
  let alert_resolve = null;
  let choice_abort = null;
  let confirm_resolve = null;
  let prompt_resolve = null;
  let menuKeyActions = null;

  function initMenuWindow(title){
    module.activeWindow = title;
    window.addEventListener("keydown", menukeydown, true);
    setClass(document.body, "menuActive", true);
  }
  function cleanupMenuWindow(){
    try{
      module.activeWindow = "";
      window.removeEventListener("keydown", menukeydown, true);
      menuKeyActions = null;
      let elem = document.querySelector(".menuContainer");
      elem.remove();
      setClass(document.body, "menuActive", false);
    }catch(e){
      console.error("Error: " + e);
    }
  }

  function Alert(msg, title){
    if(!title) title = "Alert";
    initMenuWindow(title);
    menuKeyActions = {
      "escape": closeAlert,
      "enter": closeAlert,
    }
    return new Promise((yes, no)=>{
      let container = document.createElement("div")
      container.className = "menuContainer";
      container.innerHTML = `
        <h2 class="menuTitle">${title}</h2>
        <div class="menuBg">
          ${msg}
          <br><br>
          <button class="neutral" onclick="Menu.__closeAlert();">Close</button>
        </div>
      `
      document.body.appendChild(container);
      alert_resolve = yes;
    })
  }

  function affirmConfirm(){
    cleanupMenuWindow();
    confirm_resolve(true);
  }

  function denyConfirm(){
    cleanupMenuWindow();
    confirm_resolve(false);
  }
  function closeAlert(){
    cleanupMenuWindow();
    alert_resolve();
  }

  function cancelPrompt(){
    document.getElementById("menu_prompt_input").value = "";
    handlePrompt();
  }


  function handlePrompt(){
    let value = "";
    try{
      let input = document.querySelector("#menu_prompt_input");
      if(input.type == "file"){
        value = input.files;
      } else {
        value = input.value;
      }
    }catch(e){
      console.error("Error: " + e);
    }
    cleanupMenuWindow();
    prompt_resolve(value);
  }
  function selectChoice(choice){
    let choicen = 0;
    if(typeof choice == "string"){
      for(let i=0; i<choice_names.length; ++i){
        let name = choice_names[i];
        if(name == choice){
          choicen = i;
          break;
        }
        if(i == choice_names.length - 1){
          console.warn(`Menu.__selectChoice(choice): invalid choice '${choice}'`);
        }
      }
    }else if(!isNaN(choice)){
      choicen = choice;
    }else {
      console.warn(`Menu.__selectChoice(choice): invalid choice '${choice}'`);
    }

    cleanupMenuWindow();

    let yes = choice_select ?? function(){}
    let no = choice_abort ?? function(){}
    if(choicen < 0 ||
      choicen >= choice_names.length ||
      choicen === null) return yes("");
    yes(choice_names[choicen]);
  }
  function ls(list, template, separator){
    if(!template) template = (x => "" + x);
    if(!separator) separator = " ";
    if(!list || !list.length) return "";
    let s = "";
    for(let i=0; i<list.length; ++i){
      let htmlsrc = template(list[i], i);
      s += htmlsrc;
    }
    return s;
  }

  function AlertTry(action, ...args){
    try{
      action(...args);
    }catch(e){
      alert(e);
    }
  }
  function PromptFile(prompt, title){
    return Prompt(prompt, title, true);
  }
  function Prompt(prompt, title, isfile){
    if(!title) title = "Enter Value";
    initMenuWindow(title);
    menuKeyActions = {
      "enter": handlePrompt,
      "escape" : cancelPrompt,
    }
    return new Promise((yes, no) => {
      let container = document.createElement("div")
      container.className = "menuContainer";
      container.innerHTML = `
        <h2 class="menuTitle">${title}</h2>
        <div class="menuBg">
          <p>${prompt}</p>
          <input type="${isfile? 'file': 'text'}" id="menu_prompt_input"></input>
          <br><br>
          <button class=""
            onclick="Menu.__handlePrompt();">Done</button>
          <button class="neutral"
            onclick="Menu.__cancelPrompt();">Cancel</button>
        </div>
      `
      document.body.appendChild(container);
      //setClass(maindiv, "blur", true);
      prompt_resolve = yes;
    })
  }
  function Choose(options, title){
    if(!title) title = "Select Choice";
    initMenuWindow(title);
    function optionButton(name, i){
      return `<button onclick="Menu.__selectChoice(${i})">${name}</button>&nbsp; `;
    }
    if(!options) return;
    let choiceKeys = {};
    let optionsLinkText = [];
    menuKeyActions = {
      "escape": () => selectChoice(-1),
    };
    for(let i=0; i<options.length; ++i){
      let option = options[i];
      for(let j=0; j<option.length; ++j){
        let cc = option[j];
        let c = cc.toLowerCase();
        if((/\s/).test(cc)) continue;
        let keycode = null;
        if(c == "<") keycode = 37;
        if(c == "^") keycode = 38;
        if(c == ">") keycode = 39;
        if(!(c in choiceKeys)){
          choiceKeys[c] = 
            optionsLinkText.push(
              option.substring(0,j) + `<u>${cc}</u>` +
              option.substring(j+1));
          menuKeyActions[keycode ?? c] = (k,c) => Menu.__selectChoice(option);
          break;
        }
        if(j == option.length - 1){
          optionsLinkText.push(options[i]);
        }
      }
    }
    return new Promise((yes, no)=>{
      let container = document.createElement("div")
      container.className = "menuContainer";
      container.innerHTML = `
        <h2 class="menuTitle">${title}</h2>
        <div class="menuBg">
          ${ls(optionsLinkText, optionButton, "<br>")}
          <br><br>
          <button class="neutral" onclick="Menu.__selectChoice(-1);">Close</button>
        </div>
      `
      document.body.appendChild(container);
      choice_names = options.map( x => x.split(" ")[0]);
      choice_select = yes;
      choice_abort = no;
    })
  }
  function Confirm(msg, title){
    if(!title) title = "Confirm";
    initMenuWindow(title);

    menuKeyActions = {
      "y": affirmConfirm,
      "n": denyConfirm,
      "escape": denyConfirm,
    }

    return new Promise((yes, no)=>{
      let container = document.createElement("div")
      container.className = "menuContainer";
      container.innerHTML = `
        <h2 class="menuTitle">${title}</h2>
        <div class="menuBg">
          ${msg}
          <br><br>
          <button class="neutral" onclick="Menu.__affirmConfirm();"><u>Y</u>es</button>
          <button class="neutral" onclick="Menu.__denyConfirm();"><u>N</u>o</button>
        </div>
      `
      document.body.appendChild(container);
      confirm_resolve = yes;
    })
  }
  function menukeydown(e){
    let key = e.key.toLowerCase();
    let keycode = e.keyCode;
    if(menuKeyActions){
      if (key in menuKeyActions){
        e.preventDefault();
        setTimeout(menuKeyActions[key], Delay, key, keycode);
        return false;
      } else if(keycode in menuKeyActions){
        e.preventDefault();
        setTimeout(menuKeyActions[keycode], Delay, key, keycode);
        return false;
      }
    }
  }

  function setClass(elem, name, value){
    if(!elem) return;
    if(value === undefined) value = true;
    if(value)
      elem.classList.add(name);
    else
      elem.classList.remove(name);
  }

}
