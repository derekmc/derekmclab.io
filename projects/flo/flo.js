
const flo = window.flo = {}

//(()=>{
  let updaters = []
  let default_data = {}
	let copy = (obj)=>{
		return JSON.parse(JSON.stringify(obj))
  }
  let comparator = (obj1, obj2)=>{
    let t1 = typeof obj1, t2 = typeof obj2

    // this will match numbers and strings, for instance
    if(t1 != t2) return obj1 == obj2

    if(Array.isArray(obj1)){
      if(obj1.length != obj2.length) return false
      for(let i = 0; i<obj1.length; ++i){
        if(!comparator(obj1[i], obj2[i])) return false}
      return true
    }

    if(t1 !== "object" || t2 !== "object"){
      return obj1 == obj2 }

    for(let k in obj2){ 
      if(!comparator(obj1[k], obj2[k])){
        return false }}
    return true
  }

  flo.clear = ()=>{
    updaters = []
  }
  flo.setComparator = (_comparator)=>{
    comparator = _comparator
  }


  flo.update = (data)=>{
    if(!data) data = default_data
    for(let i=0; i<updaters.length; ++i){
      updaters[i](data)
    }
  }
  flo.updater = ({elem, render, textOnly = false})=>{
    let last_value = null
    let id = (typeof elem == "string"? elem : elem.id)
		let render_count = 0
    updaters.push((data)=>{
      let value = render(data, id, render_count)
      if(value == null || value == undefined || value == last_value){
        return
      }
      last_value = copy(value)
      applyUpdates(elem, value, textOnly)
    })
  }
  function setText(elem, s){
    let txt = document.createTextNode(s)
    elem.innerHTML = ""
    elem.appendChild(txt)
  }
  function setHtml(elem, s){
    elem.innerHTML = s
  }

  function applyUpdates(elem, updates, textOnly){
    if(typeof elem == "string"){
      elem = document.getElementById(elem)
    }
    if(typeof updates == "string"){
      if(textOnly){
        setText(elem, updates)
      } else {
        setHtml(elem, updates)
      }
    } else if(typeof updates == "object"){
      for(let k in updates){
        let value = updates[k]
				let value_list = value.split(/\s+/)
        if(k == "html"){
          setHtml(elem, value)
        } else if(k == "text"){
          setText(elem, value)
        } else if(k == "addClass"){  // add or remove css classes.
					for(let item of value_list)
          	elem.classList.add(item)
        } else if(k == "removeClass"){
					for(let item of value_list)
          	elem.classList.remove(item)
        } else if(k == "toggleClass"){
					for(let item of value_list)
          	elem.classList.toggle(item)
        } else if(k == "style"){
          if(typeof value != "object"){
            console.warn("flo.applyUpdates(elem, updates, textOnly): updates.style prop should be object")
            continue
          }
          for(let j in value){
            elem.style[j] = value[j]
          }
        } else {
          elem[k] = value
        }
      }
    } else {
      console.warn("flo applyUpdates(elem, updates, textOnly): updates must be string or object")
    }
  }
//})()
