import os, re

folder = "data"
passwd_file = "admin_password.txt"
cookies_file_suffix = "_cookie.txt"

AnyUser = True # allow any username, only check the password.
UserRegex = "^[A-Za-z0-9]+$"
AdminUser = "admin"

# TODO

import string, random

admin_name = "admin"

def randstr(n=10, chars = string.ascii_lowercase):
  return "".join(random.choice(chars) for _ in range(n))

def mkfolder():
  if not os.path.exists(folder):
    os.mkdir(folder)


def checkpass(user, given_passwd):
  passwd = getpass()
  return (AnyUser or user == AdminUser) and passwd == given_passwd
 
def getpass():
  ref = os.path.join(folder, passwd_file)
  if os.path.exists(ref):
    with open(ref,'r') as fh:
      return fh.read()
  else:
    return genpass()

def genpass():
  passwd = randstr()
  ref = os.path.join(folder, passwd_file)
  mkfolder()
  with open(ref, "w") as fh:
    fh.write(passwd)
  return passwd

# doesnt create a cookie,
# if the use has no cookie returns None.
def readcookie(user):
  if not AnyUser and user != AdminUser:
    return None
  #if not re.match(UserRegex, user):
  #  return None
  ref = os.path.join(folder, user + cookies_file_suffix)
  if os.path.exists(ref):
    with open(ref) as fh:
      cookie = fh.read()
      return cookie
  else:
    return None
      
def gencookie(user):
  if not re.match(UserRegex, user):
    return None
  mkfolder()
  ref = os.path.join(folder, user + cookies_file_suffix)
  cookie = randstr()
  with open(ref, "w") as fh:
    fh.write(cookie)
  return cookie

def checkcookie(user, given_cookie):
  cookie = readcookie(user)
  if cookie == None:
    return False
  return cookie == given_cookie
  
def test():
  print("passwd.py test")
  print("getpass: " + str(getpass()))
  c1 = str(gencookie('joe'))
  print("gencookie('joe'): " + c1)
  print(f"checkcookie('joe', '{c1}'): ", checkcookie("joe", c1))


if __name__ == "__main__":
  test()
