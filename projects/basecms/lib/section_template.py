

# raw section has only sections, no pads.
# each section starts with the section name then a colon.
#
# Example:
#   text:
#     the contents of text are here.
#   name: Joe
#   phone: (555)-555-5555
#   email: joe@joe.joe
#
# The section content must be on the same line, or 
# indented starting on the next line, for multiple lines.
def parseRaw(sectionData):
  pass


# parse the pattern into a dictionary,
# with all section content and pads.
# Return: (sections, pads) 
#   sections: [("name", "text"), ...]
#   pads: ["", ...]

def dummyParse():
  sections = []
  sections.append(("html", "<h1> Hello, World.</h1>"))
  sections.append(("js", "console.log('here');"))
  sections.append(("css", 
  pads = []
  pads.append(
  return [sections, pads]

def parsePattern(pattern):
  return dummyParse()





def applyPattern(pattern, target):
  # The regexes we need, not the ones we deserve.
  # each is a different comment type, in which 

  start_section_c = # /* Start: name */
  start_section_slash = # // Start: name
  start_section_html = # <!-- Start: name -->
  
  end_section_c = //
  end_section_slash = //
  end_section_html =

  # hmmm, should we require start and end section comment
  # types to match? I think so, and give a warning if they
  # don't match, and ignore the non-matching end comment.


  # TODO Find sections in target and pattern

  # If target has extra sections, not present in pattern,
  # refuse to do the conversion. TODO



  # TODO Warn about missing sections in target


  # Put the section content from target into outer document
  # of pattern, and return the document. TODO


# list all sections in 'pattern'
def listSections(pattern):
  (sections, _) = parsePattern(pattern)

  
# read the contents of 'sectionName' from 'pattern'
# unindenting as appropriate.
def readSection(pattern, sectionName):
  (sections, pads) = parsePattern(pattern)
  for (name, text) in sections:
    if name == sectionName:
      return text
  raise ArgumentError(f"unknown section: '{sectionName}'")


# overwrite the specified section with 'content'
# return the new pattern.
def writeSection(pattern, sectionName, sectionContent):
  (sections, pads) = parsePattern(pattern)
  writecount = 0
  result = ""
  pad_index = 0

  result += pads[pad_index]

  for (name, text) in sections:
    if name == sectionName:
      writecount += 1
      result += sectionContent
    else:
      result += text
    pad_index += 1
    if pad_index < len(pads):
      result += pads[pad_index]

  if writecount == 0:
    raise ArgumentError(f"unknown section: '{sectionName}'")
  return result




def test():
  with open("pages/note1.html", "r") as file:
    note1 = file.read()


  with open("pages/note2.html", "r") as file:
    note2 = file.read()

 


if __name__ == "__main__":
  test()
