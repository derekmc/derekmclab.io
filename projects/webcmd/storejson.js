

const fs = require('fs/promises')

let datastore = {}
let dirty = false
let saveInterval = 10 * 1000
let dataFile = "app_db.json"

load = async ()=>{
  try{
    datastore = JSON.parse(await fs.readFile(dataFile, {encoding: 'utf8'}))
    return true
  } catch(e){
    console.error("could not load data", e)
    return false
  }
}
save = async ()=>{
  if(!dirty){
    //console.log("no new data to save")
    return
  }
  let content = JSON.stringify(datastore, null, 2)
  await fs.writeFile(dataFile, content, {encoding: 'utf8'})
  console.log("saved at: " + (new Date()))
  dirty = false
}

get = async (key)=>{
  return datastore[key]
}

set = async (key, value)=>{
  dirty = true
  if(value == undefined){
    del(datastore[key])
  }
  datastore[key] = value
}

add = async (key, value)=>{
  if(key in datastore) return false
  if(value == undefined) return false
  dirty = true
  datastore[key] = value
  return true
}

del = async (key)=>{
  delete datastore[key]
}

init = async (params)=>{
  if(params?.dataFile) dataFile = params.dataFile
  if(params?.saveInterval) saveInterval = params.saveInterval

  console.log('init store.js')
  await load()
  setInterval(save, saveInterval)
}
 

module.exports = {
  init,
  get, set, del, add
}

