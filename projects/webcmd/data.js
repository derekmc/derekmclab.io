
const store = require('./storejson.js')
const {get, set, add, del} = store
const COOKIE_LEN = 15

// a general kv store, but with a key prefix
getItem = async (name)=>{
  let key = "item|" + name
  await get(key)
}
setItem = async (name, value)=>{
  let key = "item|" + name
  await set(key, value)
}
delItem = async (name)=>{
  let key = "item|" + name
  await del(key)
}
addItem = async (name, value)=>{
  let key = "item|" + name
  return await add(key, value)
}

init = async ()=>{
  console.log('init data.js')
  await store.init()
}

assert = (x, msg)=>{
  if(!x) throw new Error(msg)
  return x
}


// DB Keys
UserKey = ({username})=>"user|"+assert(username, "UserKey requires username parameter.")
CookieKey = ({cookie})=>"cookie|"+assert(cookie, "CookieKey requires cookie parameter.")
UserCookiesKey = ({username})=>"userCookies|"+assert(username, "UserCookiesKey requires cookie parameter.")


// application actions
getUserInfo = async (username)=>{
  let key = UserKey({username})
  return await get(key)
}

// info contains only non-mutable data
newUser = async (username, password)=>{
  let key = UserKey({username})
  return await add(key, {password})
}

getUserCookies = async (username)=> {
  await get(UserCookiesKey({username}))
}

function makeid(length) {
	let result = '';
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	const charactersLength = characters.length;
	let counter = 0;
	while (counter < length) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
		counter += 1;
	}
	return result;
}

newSession = async (username)=>{
  let cookie = makeid(COOKIE_LEN)
  let uckey = UserCookiesKey({username})
  let cookiekey = CookieKey({cookie})
  let cookies = await getUserCookies(uckey) || []
  cookies.push(cookie)
  await Promise.all[
    set(uckey, cookies), set(cookiekey, username)
  ]
	return cookie
}

rmCookie = async (cookie)=>{
  let username = await getCookieUser(cookie)
  let cookiekey = CookieKey({cookie})
  let uckey = UserCookiesKey({username})
  let cookies = await getUserCookies(uckey)
  if(!cookies) return
  cookies = cookies.filter(x=>x!=cookie)
	await Promise.all([
    set(uckey, cookies),
    del(cookiekey)
  ])
}

getCookieUser = async (cookie)=>{
  let cookiekey = CookieKey({cookie})
  return await get(cookiekey)
}

userAuth = async (username, password)=>{
  let user = await getUserInfo(username)
  if(!user) return false
  return user.password == password
}

module.exports = {
  init, //get, set,
  getItem, setItem,
  addItem, delItem,
  getUserInfo, newUser,
  getUserCookies,
  newSession,
  rmCookie,
  getCookieUser,
  userAuth,
}
