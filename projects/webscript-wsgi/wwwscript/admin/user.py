
import random, string
import os
import json
user_folder = "data/users/"
invite_file = "data/user_invites.txt"

with open('include/user_admin.html') as file:
  userpage = file.read()

def initfolders():
  try:
    os.mkdirs(user_folder)
    os.mkdirs(request_folder)
  except:
    pass

def randomstr(n):
  letters = string.ascii_lowercase
  return ''.join(random.choice(letters) for i in range(n))

def render(environ={}, setcookies=None,
    path="", query="", data="", cookies=""):

  message = ""
  initfolders()
  user_requested = cookies.get('user_requested', False)
  action = data.get('usermanage_action', "")
  if data.get('userform_action',"") == 'create_invites':
    invite_count = data.get('invite_count', 1)
    invites = [randomstr(8) for x in range(invite_count)]
    invite_list = ""
    with open(invite_file, "a") as file:
      for invite in invites:
        file.write(str(invite) + "\n")
        invite_list += f"<a href='signup?code={invite}'>{invite}</a><br>"
      message = invite_list

    #message = "<h2> New User Requested </h2>"
    #setcookies(f"user_requested=True")
  else:
    print("query", str(query))
    print("data", str(data))
    print('cookies', cookies)

  body = userpage
  body = body.replace("{{message}}", message)
  return body

