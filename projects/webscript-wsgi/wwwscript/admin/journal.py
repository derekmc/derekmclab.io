
import os
import re
import urllib.parse
from urllib.parse import unquote, parse_qsl
from html import escape
from datetime import datetime

# Make sure data folder exists.
data_folder = "data"
file_prefix = "journal_"
file_suffix = ".txt"
file_pattern = "^journal_(.*)\\.txt$"

if not os.path.exists(data_folder):
  os.mkdir(data_folder)


# Load the journal template
with open('include/journal.html') as file:
  journalpage = file.read()


MAXLEN = 1000*1000


# render a page
def render(environ={}, setcookies=None,
    path="", query="", data="", cookies=""):

  query_data = dict(parse_qsl(query))
  journal_text = data.get("journal_text", "")
  now_name = datetime.now().strftime("%Y_%m%d_%H%M")
  entry_name = query_data.get("entry_name", "")
  if "entry_name" in data:
    entry_name = data.get("entry_name", now_name)
  entry_name = re.sub(r'[^a-zA-Z0-9_]+', '_', entry_name)
  print("data", data)
  print("entry name: " + entry_name)
  if len(entry_name) == 0:
    entry_name = now_name
  
  file_name = f"{data_folder}/{file_prefix}{entry_name}{file_suffix}"


  if len(journal_text):
    replacements = [
      ["+", " "],
    ]
    for r in replacements:
      journal_text = journal_text.replace(r[0], r[1])

    if len(journal_text) > MAXLEN:
      journal_text = journal_text[:MAXLEN]
    with open(file_name, "w") as file:
      file.write(journal_text)
  else:
    try:
      with open(file_name, "r") as file:
        journal_text = file.read()
    except:
      print("Could not open: " + file_name)
      pass
    
  entry_template = lambda x: f"<li><a href='?entry_name={x}'>{x}</a></li>"
  entry_list = list(filter(lambda s: re.match(file_pattern, s),
      os.listdir(f"{data_folder}")))
  entry_list.sort()
  entries_html = "".join(list(map(lambda s: entry_template(re.match(file_pattern, s).group(1)),
    entry_list)))
  print("entries", entries_html)


  body = journalpage
  body = body.replace("{{journal_text}}", escape(journal_text))
  body = body.replace("{{entry_name}}", escape(entry_name))
  body = body.replace("{{entry_list}}", entries_html)

  return body

   #pass
