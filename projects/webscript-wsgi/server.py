#!/usr/bin/env python3


from wwwscript import wwwscript
from wwwscript.admin import admin as wsadmin

# encoding and hash functions
from hashlib import sha256
from base64 import b64encode, b64decode
from urllib.parse import parse_qsl

# libraries
import random, string, os, csv, datetime, time

b64chars = string.ascii_letters + "0123456789" + "+/"

def randstr(n=10, chars=b64chars):
  return ''.join(random.choice(chars) for x in range(n))


scriptnames = {}
adminscripts = {}
admins = {}

# path references use "/" separation here.
# regardless of the os separator.
static_files = {}

ADMIN_PATH = "admin"
STATIC_PATH = "static"
RENDER_STATIC = os.path.join("web-builder", "out")
ADMIN_COOKIE_NAME = "ADMIN_TOKEN"
ADMIN_COOKIE_FILE = os.path.join("data", "admincookies.csv")
ADMIN_FILE = os.path.join("config", "admins.csv")
COOKIE_LEN = 12

# site pages (loaded in 'setup_app' function)
index_page = ""
page404 = ""
admin_index_page = ""
admin_login_page = ""
redirect_page = ""

# TODO pre-render (ie web-builder)

def load_static(startdir):
  for root, dirs, files in os.walk(startdir):
    for name in files:
      fname = os.path.join(root, name)
      relpath = name if root == startdir else os.path.join(os.path.relpath(root, startdir), name)
      file_ref = "/".join(relpath.split(os.sep))
      with open(fname, 'r') as fh:
        try:
          text = fh.read()

          # only do link substitutions for html files
          # replace {{home}} and {{admin_home}} with relative paths
          if len(file_ref) > 5 and file_ref[-5:] == ".html":

            # Figure out relative path to root.
            path_parts = file_ref.split(".")[0].split("/")
            path_depth = len(path_parts) - 1
            home_link = "." if path_depth <= 0 else "/".join([".."] * path_depth)
            admin_home_link = home_link + "/" + ADMIN_PATH + "/"

            # replace path references.
            text = text.replace("{{home}}", home_link)
            text = text.replace("{{admin_home}}", admin_home_link)

          static_files[file_ref] = text
          
        except:
          print(f"  Could not read '{file_ref}'")

def parseCookies(cookies):
  result = {}
  cookie_parts = cookies.split(";")
  for cookie_str in cookie_parts:
    if not "=" in cookie_str:
      continue

    index = cookie_str.index("=")
    key = cookie_str[:index].strip()
    value = cookie_str[index+1:].strip()
    result[key] = value

  return result


def setup_app():
  global index_page
  global page404
  global admin_index_page
  global admin_login_page
  global redirect_page

  try:
    assert os.path.exists(ADMIN_FILE)
  except:
    print("\nNo admin accounts exist, please run 'python scripts/setup.py' from the wwwscript root directory")
    return False

  try:
    import subprocess
    print("\nRunning web-builder\n")
    subprocess.run("./gen.py", cwd="web-builder")
    print("\nweb-builder finished...\n\n")
  except:
    pass
  try:
    with open(ADMIN_FILE) as file:
      reader = csv.DictReader(file)
      for admin in reader:
        name = admin['name']
        if name in admin:
          print(f"Warning - Duplicate admin '{name}'")
        admins[name] = admin
  except:
    pass

  print("Web Scripts: ")
  for script in wwwscript:
    name = script.__name__[len("wwwscript."):]
    print(f"  {name}")
    scriptnames[name] = script

  print("\nAdmin Scripts: ")
  for script in wsadmin:
    name = script.__name__[len("wwwscript.admin."):]
    print(f"  {name}")
    adminscripts[name] = script

  with open('sitepages/index.html') as file:
    index_page = file.read()

  with open('sitepages/404.html') as file:
    page404 = file.read()

  with open('sitepages/admin_index.html') as file:
    admin_index_page = file.read()

  with open('sitepages/admin_login.html') as file:
    admin_login_page = file.read()

  with open('sitepages/redirect.html') as file:
    redirect_page = file.read()

  print("\nStatic files")

  static_root = os.path.join(os.path.dirname(__file__), STATIC_PATH)
  render_static = os.path.join(os.path.dirname(__file__), RENDER_STATIC)

  load_static(static_root)
  load_static(render_static)

  for file_ref in static_files:
    print(f"  {file_ref}")
    
  return True

def clearAdminCookies(admin_name):
  try:
    lines = []
    with open(ADMIN_COOKIE_FILE, "r") as file:
      reader = csv.DictReader(file, fieldnames=["name", "cookie"])
      for row in reader:
        name = row['name']
        cookie = row['cookie']
        if name != admin_name:
          lines.append(f'"{name}","{cookie}"\n')
        else:
          print(f'logout (name, cookie): {name},{cookie}')
    with open(ADMIN_COOKIE_FILE, "w") as file:
      for line in lines:
        file.write(line)
  except:
    print("Warning - Unable to read admin cookies.\n")
    print("Please run 'python scripts/setup.py' from the wwwscript base directory.\n")

def addAdminCookie(admin_name, admin_cookie):
  try:
    with open(ADMIN_COOKIE_FILE, "a") as file:
      file.write(f'"{admin_name}","{admin_cookie}"\n')
  except:
    print("Warning - Unable to read admin cookies.\n")
    print("Please run 'python scripts/setup.py' from the wwwscript base directory.\n")

def lookupAdminCookie(admin_cookie):
  try:
    with open(ADMIN_COOKIE_FILE) as file:
      reader = csv.DictReader(file, fieldnames=["name", "cookie"])
      for row in reader:
        name = row['name'].strip()
        cookie = row['cookie']
        # print(f"name, cookie: {name}, {cookie}")
        if(cookie == admin_cookie):
          # print(f"cookie lookup: {cookie} -> {name}")
          return name
  except:
    print("Warning - Unable to read admin cookies.\n")
    print("Please run 'python scripts/setup.py' from the wwwscript base directory.\n")

  return None


def application(environ, start_response):
  status = '200 ok'
  path = environ['PATH_INFO']
  query = environ['QUERY_STRING']
  try:
    request_body_size = int(environ.get('CONTENT_LENGTH', 0))
  except (ValueError):
    request_body_size = 0
  try:
    cookies = parseCookies(environ['HTTP_COOKIE'])
  except (KeyError):
    cookies = {}

  headers = [
    ('Content-type', 'text/html'),
  ]

  def cookiedate(hours):
    now = datetime.datetime.now(datetime.timezone.utc)
    now += datetime.timedelta(hours=hours)
    return now.strftime("%a, %d-%b-%Y %T GMT")
  # only call this once.

  def setcookies(cookies, expirehours = None):
    value = cookies + "; Path=/;" 
    if expirehours != None:
        value += cookiedate(expirehours)
    headers.append(('Set-Cookie', value))
 
  req_body = environ['wsgi.input'].read(request_body_size)
  data = dict(parse_qsl(req_body.decode('utf-8')))
  # TODO check headers to know how to parse req_body
  # the 4 parameters we pass to wwwscripts are
  # data, query, path, cookies

  # TODO check wwwscript paths first, then
  # check static web path
  # print("Path: " + str(path))
  # this removes the extension

  path_parts = path[1:].split(".")[0].split("/")
  is_admin_path = path_parts[0] == "admin"
  path_depth = len(path_parts) - 1
  home_link = "." if path_depth <= 0 else "/".join([".."] * path_depth)
  admin_home_link = home_link + "/" + ADMIN_PATH + "/"
  admin_logout_link = admin_home_link + "logout/"
  # print("home_link", home_link)
  # print("path_parts", path_parts)

  static_url = path[1:]
  noextension = path[1:].split(".")[0]
  if len(noextension) > 0 and noextension[-1] == "/":
    noextension = noextension[:-1]
  script_path = noextension.replace("_", "__");
  script_path = script_path.replace("/", "_");

  #print("script path:" + script_path)

  if is_admin_path:
    # print("admin")
    #TODO admin login
    admin_path = script_path[len("admin/"):]
    admin_cookie = None
    admin_name = None
    admin_password = None
    login_submitted = False

    if data and len(data):
      query_dict = data
      if "admin_name" in query_dict:
        admin_login_name = query_dict.get("admin_name", None)
        admin_password = query_dict.get("admin_password", None)
        login_submitted = True
 
        authenticated = False

        # When you create or modify an admin,
        # reset all wsgi instances, to read the new admin data.
        
        for name in admins:
          admin = admins[name]
          if admin_login_name != admin["name"]:
            continue
          stored_hashvalue = admin["hash"]
          print("salt: " + admin["salt"])
          prehash = (admin_password + admin["salt"]).encode()
          given_hashvalue = str(b64encode(sha256(prehash).digest()))
          print("stored hash: " + stored_hashvalue)
          print("given hash: " + given_hashvalue)
          if stored_hashvalue == given_hashvalue:
            print("authenticated")
            authenticated = True
            admin_name = admin_login_name
            admin_cookie = randstr(COOKIE_LEN)
            clearAdminCookies(admin_name)
            addAdminCookie(admin_name, admin_cookie)
            setcookies(f"{ADMIN_COOKIE_NAME}={admin_cookie}")

        if not authenticated:
          print("Authentication failed.")
          admin_name = None


    if not login_submitted:
      if ADMIN_COOKIE_NAME in cookies:
        cookie_token = cookies[ADMIN_COOKIE_NAME]
        print("looking up cookie: " + cookie_token)
        admin_name = lookupAdminCookie(cookie_token)
        if admin_name != None:
          admin_cookie = cookie_token
        
    if(admin_path == "logout"):
      body = redirect_page
      body = body.replace("{{redirect_link}}", admin_home_link)
      body = body.replace("{{redirect_action}}", "Logged out")

      headers.append(('Content-Length', str(len(body))))
      setcookies(f"{ADMIN_COOKIE_NAME}=", -24*365)
      clearAdminCookies(admin_name)
      start_response(status, headers)
      return [body.encode("utf-8")]


    if admin_name == None:
      body = admin_login_page 
    else:
      print(f"Admin Name: {admin_name}")

      if admin_path in adminscripts:
        script = adminscripts[admin_path]
        body = script.render(environ=environ, setcookies=setcookies,
          query=query, data=data, cookies=cookies)
      elif script_path == "admin_index" or script_path == "admin":
        body = admin_index_page
        line = lambda name: f"<li><a href='./{name}'>{name}</a></li>"
        admin_pages = "".join(map(line, adminscripts))
        body = body.replace("{{admin_name}}", admin_name)
        body = body.replace("{{admin_pages}}", admin_pages)
        body = body.replace("{{admin_logout}}", admin_logout_link)
      else:
        body = page404

  elif script_path in scriptnames:
    script = scriptnames[script_path]
    body = script.render(environ=environ, setcookies=setcookies,
        query=query, data=data, cookies=cookies)
  elif script_path == "index" or script_path == "":
    line = lambda name: f"<li><a href='./{name}'>{name}</a></li>"
    script_pages = "".join(map(line, scriptnames))
    body = index_page
    body = body.replace("{{script_pages}}", script_pages)
  elif static_url in static_files:
    # Serve static files
    body = static_files[path[1:]]
  else:
    body = page404
    status = "404 Not Found"

  headers.append(('Content-Length', str(len(body))))

  body = body.replace("{{home}}", home_link)
  body = body.replace("{{admin_home}}", admin_home_link)

  start_response(status, headers)
  return [body.encode("utf-8")]


def run_cherrywsgi():
	# Mostly copied from a digital ocean tutorial
	# "How to deploy python wsgi applications using a cherrypy webserver..."
	import cherrypy

	# Mount the application
	cherrypy.tree.graft(application, "/")

	# Unsubscribe the default server
	cherrypy.server.unsubscribe()

	# Instantiate a new server
	server = cherrypy._cpserver.Server()

	# Configure server
	server.socket_host = "127.0.0.1"
	server.socket_port = 8000
	server.thread_pool = 1

  # For SSL Support
	# server.ssl_module            = 'pyopenssl'
	# server.ssl_certificate       = 'ssl/certificate.crt'
	# server.ssl_private_key       = 'ssl/private.key'
	# server.ssl_certificate_chain = 'ssl/bundle.crt'
	
	# Subscribe this server
	server.subscribe()
	
	# Example for a 2nd server (same steps as above):
	# Remember to use a different port
	
	# server2             = cherrypy._cpserver.Server()
	
	# server2.socket_host = "0.0.0.0"
	# server2.socket_port = 8081
	# server2.thread_pool = 30
	# server2.subscribe()
	
	# Start the server engine (Option 1 *and* 2)
	
	cherrypy.engine.start()
	cherrypy.engine.block()

def run_waitress():
  if setup_app():
    from waitress import serve
    serve(application, port=8000, threads=4)

def run_wsgiref():
  if setup_app():
    from wsgiref.simple_server import make_server
    server = make_server('', 8000, application) 
    print("\nServer port 8000")
    server.serve_forever()

def main():
  if setup_app():
    run_wsgiref()
  #run_waitress()
  #run_cherrywsgi()

if __name__ == "__main__":
  main()
