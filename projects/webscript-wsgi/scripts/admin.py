#/usr/bin/env python3

from hashlib import sha256
from base64 import b64encode, b64decode
import random, string, getpass
import sys, os, csv

b64chars = string.ascii_letters + "0123456789" + "+/"

admin_header = "name,salt,hash"

admin_file = "config/admins.csv"
admins = {}

try:
  if os.path.exists(admin_file):
    with open("config/admins.csv") as file:
      reader = csv.DictReader(file)
      for admin in reader:
        name = admin['name']
        if name in admin:
          print(f"Warning - Duplicate admin '{name}'")
        admins[name] = admin
except:
  print(f"Warning - could not access admin file.")



def randstr(n=10, chars=b64chars):
  return ''.join(random.choice(chars) for x in range(n))
 
def newadmin():
  while True:
    adminname = input("New Admin Name: ")
    if not (adminname in admins):
      break
    print("\nAdmin exists.")

  while True:
    password = getpass.getpass("Password: ")
    password2 = getpass.getpass("Confirm Password: ")

    if password == password2:
      break
    print("\nPassword mismatch!")

  salt = randstr()
  prehash = (password + salt).encode()
  hashvalue = b64encode(sha256(prehash).digest())

  folder = "config"
  if not os.path.exists(folder):
    os.mkdir(folder)

  path = folder + "/admins.csv"
  isempty = (not os.path.exists(path)) or os.path.getsize(path) == 0

  admin_line = f'"{adminname}","{salt}","{hashvalue}"'

  with open(path, "a") as file:
    if isempty:
      file.write(admin_header + "\n")
    file.write(admin_line + "\n")

  print(f"Admin '{adminname}' added successfully.")
  
def help():
  print("Webscript WSGI framework")
  print("scripts/setup.py available commands")
  print(" new")
  

def main(args):
  if len(args) == 0:
    return help()

  command = args[0]

  if command == "new":
    newadmin()
  else:
    help()

if __name__ == "__main__":
  main(sys.argv[1:])
  
