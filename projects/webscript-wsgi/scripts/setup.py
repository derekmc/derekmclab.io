#!/usr/bin/env python3
from admin import main as admin_main

print("==== Webscript setup script. ====")

print()
print("== Create an admin account. ==")
print()

admin_main(["new"])

print()
print("Setup finished.")
