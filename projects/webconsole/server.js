const http = require('http')
const port = 3000
const host = '0.0.0.0'
const fs = require('fs')
const path = require('path')
const suppress_output = false

const {
  resetCommand, handleCommand, commandOutput
} = require("./command.js")

const script_dir = 'scripts'
const notes_dir = 'notes'

// todo script buttons
let pages = {}
let scripts = {}
let notes = {}
let consolePage, infoPage, uploadPage, indexPage,
  editPage, scriptEditPage, confirmPage


main()

async function main(){
  await readPageFiles()
  await readScriptFiles()
  await readNoteFiles()
  resetCommand()
  let server = http.createServer(handler)
  server.listen(port, host)
  console.log(`\nserving on ${host}:${port}`)
}

async function handler(req, res){
  //console.dir(req.param)
  let spliturl = req.url.split('?')
  let path = spliturl[0]
  let queryString = spliturl[1]
  let cmdtext = ''

  if(path == '/'){
    return finish(indexPage)
  }
  else if(path == '/console.html'){
    let command = ''
    if(req.method == "POST"){
      let body = await handlePost()
      let query = parseQuery(body)
      cmdtext = query['cmdinput'] ?? ''
      let old_output = commandOutput()
      commandOutput(commandOutput() + "\n|" + cmdtext)
      if(!handleCommand(cmdtext.trim())){
        commandOutput(old_output)
      }
      finish(consolePage, {cmdtext,
        output:commandOutput()})
        //hist: hist.join("\\n")})
      //hist.pop()
      //hist.push(cmdtext)
    } else {
      finish(consolePage, {cmdtext, output: commandOutput()})
    }
  }
  else if(path == '/upload.html'){
    finish(uploadPage, {})
  }
  else if(path == '/confirm.html'){
    if(req.method == "GET"){
      let query = parseQuery(queryString)
      let action = query.action
      let target = query.target
      return finish(confirmPage, {action, target})
    }
    if(req.method == "POST"){
      let body = await handlePost()
      let query = parseQuery(body)
      if('cancelAction' in query){
        let action = query.cancelAction
        if(action == 'rmnote'){
          return redirectPost('notes.html') }
        if(action == 'rmscript'){
          return redirectPost('scripts.html') }
      }
      if('confirmAction' in query){
        let action = query.confirmAction
        let target = query.actionTarget
        if(action == 'rmnote'){
          await rmNote(target)
          return redirectPost('notes.html')
        }
        if(action == 'rmscript'){
          await rmScript(target)
          return redirectPost('scripts.html')
        }
      }
    }
    //finish(editPage, {
  }
  else if(path == '/notes.html'){
    let page_name = "untitled.txt"
    let page_content = ""
    if(req.method == "POST"){
      let body = await handlePost()
      let query = parseQuery(body)
      if("fileload" in query){
        page_name = query.fileload
        page_content = notes[page_name]
      }else if('rmfile' in query){
        return redirectPost(`/confirm.html?action=rmnote&target=${query.rmfile}`)
      }else{
        page_name = query.pagename
        page_content = query.pagecontent
        notes[page_name] = page_content
        await saveNote(page_name)
      }
    }
    let page_list = await listFilesTemplate(notes)
    finish(editPage, {page_type: "Notes",
      page_list, page_name, page_content})
  }
  else if(path == '/scripts.html'){
    let script_name = "untitled.stack"
    let script_content = ""
    let script_output = ""
    if(req.method == "POST"){
      let body = await handlePost()
      let query = parseQuery(body)
      if("fileload" in query){
        script_name = query.fileload
        script_content = scripts[script_name]
      }else if('rmfile' in query){
        return redirectPost(`/confirm.html?action=rmscript&target=${query.rmfile}`)
      }else{
        script_name = query.scriptname
        script_content = query.scriptcontent
        scripts[script_name] = script_content
        // TODO
        await saveScript(script_name)
        // await readScriptFiles()
        resetCommand()
        let lines = script_content.split("\n")
        lines.forEach(line =>{
          let old_output = commandOutput()
          let linestart = line?.trim()[0]
          //if(line?.trim().length && linestart != ":" && linestart != "#")
           // commandOutput(commandOutput() + "\n| " + line.trim())
          if(!handleCommand(line.trim(), suppress_output)){
            commandOutput(old_output)
          }
          //console.log('script line: ' + line)
        })
        //console.log('output: ' + output)
        script_output = commandOutput()
      }
    }
    let script_list = await listFilesTemplate(scripts)

    //console.log('script_list', script_list)
    finish(scriptEditPage, {
      script_list, script_name,
      script_content, script_output})
  }
  else{
    // TODO 404
    return finish(indexPage)
  }

  function handlePost(){
    return new Promise((yes, no)=>{
      console.log('post request')
      let body = ''
      req.on('data', (data)=>{
        body += data
        //console.log('body: ' + body)
      })
      req.on('end', ()=>{
        yes(body)
      })
    })
  }
  function redirectPost(url){
    res.writeHead(303, {Location: url}).end()
  }
  function redirect(url){
    res.writeHead(307, {Location: url}).end()
  }

  function finish(src, args){
    res.writeHead(200, {'Content-type': 'text/html'})
    let pageSrc = renderPage(src, args)
    res.end(pageSrc)
  }
}



function listFiles(dir){
  return new Promise((yes, no)=>{
    fs.readdir(dir, (err, files)=>{
      if(err){
        console.error("could not read script directory: ", err)
      }
      yes(files) // should be undefined if there was an error
    })
  })
}
function writeFile(filename, content){
  return new Promise((yes, no)=>{
    fs.writeFile(filename, content, (err, data)=>{
      if(err){
        console.error(err)
        return no(err)
      }
      yes()
    })
  })
}
function readFile(filename){
  return new Promise((yes, no)=>{
    fs.readFile(filename, 'utf8', (err, data)=>{
      if(err){
        console.error(err)
        return no(err)
      }
      yes(data)
    })
  })
}
function rmFile(filename){
  return new Promise((yes, no)=>{
    fs.unlink(filename, (err)=>{
      if(err){
        console.error(err)
        return no(err)
      }
      yes()
    })
  })
}
async function rmNote(name){
  delete notes[name]
  await rmFile(path.join(notes_dir, name))
}
async function rmScript(name){
  delete scripts[name]
  await rmFile(path.join(script_dir, name))
}
async function saveNote(name){
  let content = notes[name]
  if(content){
    await writeFile(path.join(notes_dir, name), content)
  }
}

async function saveScript(name){
  let content = scripts[name]
  if(content){
    await writeFile(path.join(script_dir, name), content)
  }
}
async function readNoteFiles(){
  let list = await listFiles(notes_dir)
  notes = {}
  if(list){
    let waitlist = []
    for(let i=0; i<list.length; ++i){
      let name = list[i]
      waitlist.push(readFile(path.join(notes_dir, name)).then(
        ((name)=>(content)=>notes[name] = content)(name)))
    }
    await Promise.all(waitlist)
  }
  console.log("\nnotes:")
  for(let name in notes)
    console.log('\n'+name+':\n'+ notes[name].split('\n').
      slice(0,2).map(l=>"  " + l).join('\n'))
 
}
async function readScriptFiles(){
  let list = await listFiles(script_dir)
  scripts = {}
  if(list){
    let waitlist = []
    for(let i=0; i<list.length; ++i){
      let name = list[i]
      waitlist.push(readFile(path.join(script_dir, name)).then(
        ((name)=>(content)=>scripts[name] = content)(name)))
    }
    await Promise.all(waitlist)
  }
  console.log("\nscripts:")
  for(let name in scripts)
    console.log('\n'+name+':\n'+ scripts[name].split('\n').
      slice(0,3).map(l=>"  " + l).join('\n'))
      
}
async function readPageFiles(){
  let pageList = [
    ['./pages/console.html', (x)=>consolePage = x],
    ['./pages/upload.html', (x)=>uploadPage = x],
    ['./pages/index.html', (x)=>indexPage = x],
    ['./pages/edit.html', (x)=>editPage = x],
    ['./pages/scripts.html', (x)=>scriptEditPage = x],
    ['./pages/confirm.html', (x)=>confirmPage = x],
  ]
  console.log('loading pages.')
  let promises = []
  for(let i=0; i<pageList.length; ++i){
    let [filename, assign] = pageList[i]
    promises[i] = (async ()=>{
      let content = await readFile(filename)
      console.log(`  ${filename} : `, content.length)
      assign(content)
    })()

  }
  await Promise.all(promises)
}


//function scriptListTemplate(){
//}
// async function listDirTemplate(dir){

function listFilesTemplate(files){
  let s = ""
  for(let name in files){
    s += `<form method='POST' style='display: inline-block; margin:3px;'>
            <input type='hidden' name='fileload' id='fileload'
              value='${name}'/>
            <input type='submit' value='${name}' title='Load File'
              style="color:#000; background:#eee;
                font-family: monospace;
                border: 2px solid #000; border-radius: 6px 0 0 6px;
                font-weight: bold;
                font-size: 14px; padding: 6px 14px;"/>
          </form>
          <form method='POST' style='display: inline-block; margin: 3px; margin-left: -8px;'>
            <input type='hidden' name='rmfile' id='rmfile' value='${name}'>
            <input type='submit' value='x' title="Delete"
              style="color:#fff; background:#000;
                font-family: monospace;
                border: 2px solid #000; border-radius: 0 6px 6px 0px;
                font-weight: bold;
                font-size: 14px; padding: 6px 14px;"/>
          </form>
          `
  }
  return s
}

function renderPage(src, args){
  for(let k in args){
    let v = args[k]
    src = src.replaceAll('{{' + k + '}}', v)
  }
  return src
}

function parseQuery(str){
  let result = {}
  let pairs = str.split("&").forEach((x)=>{
    let i = x.indexOf('=')
    let k = x.substr(0,i)
    let v = x.substr(i+1)
    let replacements = [['+', ' ']]
    for(let [s, ss] of replacements){
      v = v.replaceAll(s, ss)}
    v = decodeURIComponent(v)
    if(i > 0) result[x.substr(0, i)] = v
    else result[x] = ''
  })
  return result
}

function escapeChars(s){
  return s.replace("\\", "\\\\").
    replace("\n", "\\n").
    replace("\"", "\\\"")
}
