
const http = require('http')
const port = 3000
const host = '0.0.0.0'
const fs = require('fs')
const path = require('path')
const suppress_output = false
const serve_handler = require('serve-handler')
// let demo_render = require('./www/demo.ws').render

main()
async function main(){
  let server = http.createServer(handler)
  server.listen(port, host)
  console.log(`\nserving on ${host}:${port}`)
}

/*
async function loadPages(){
  let pageList = [
    ['./include/demo.html', (x)=>demoPage = x],
  ]
  console.log('loading pages.')
  let promises = []
  for(let i=0; i<pageList.length; ++i){
    let [filename, assign] = pageList[i]
    promises[i] = (async ()=>{
      let content = await readFile(filename)
      console.log(`  ${filename} : ${content.length}`)
      assign(content)
    })()
  }
  await Promise.all(promises)
}
*/

async function handler(req, res){
  let spliturl = req.url.split('?')
  let path = spliturl[0]
  let queryString = spliturl[1]
  let query = parseQuery(queryString)
  let data = {}


  if(req.method == "POST"){
    let body = await handlePost()
    data = parseQuery(body)
  }
  if(path == '/input'){
    return finish(`<pre>${query.actions}</pre>`)
  }
  if(path == '/update'){
    return finishText('TODO: updates', {})
  }

  return serve_handler(req, res, {
    cleanUrls: true,
    "public": 'www',
  })

  // TODO get cookies and set cookies
  // TODO traverse www to find script paths
  function handlePost(){
    return new Promise((yes, no)=>{
      console.log('post request')
      let body = ''
      req.on('data', (data)=>{
        body += data
        //console.log('body: ' + body)
      })
      req.on('end', ()=>{
        yes(body)
      })
    })
  }
  function redirectPost(url){
    res.writeHead(303, {Location: url}).end()
  }
  function redirect(url){
    res.writeHead(307, {Location: url}).end()
  }

  function finishText(src, args){
    res.writeHead(200, {'Content-type': 'text/plain'})
    let output = renderPage(src, args)
    res.end(output)
  }

  function finish(src, args){
    res.writeHead(200, {'Content-type': 'text/html'})
    let pageSrc = renderPage(src, args)
    res.end(pageSrc)
  }

}
function renderPage(src, args){
  for(let k in args){
    let v = args[k]
    src = src.replaceAll('{{' + k + '}}', v)
  }
  return src
}

function escapeChars(s){
  return s.replace("\\", "\\\\").
    replace("\n", "\\n").
    replace("\"", "\\\"")
}
function parseQuery(str){
  let result = {}
  if(!str) return result
  let pairs = str.split("&").forEach((x)=>{
    let i = x.indexOf('=')
    let k = x.substr(0,i)
    let v = x.substr(i+1)
    let replacements = [['+', ' ']]
    for(let [s, ss] of replacements){
      v = v.replaceAll(s, ss)}
    v = decodeURIComponent(v)
    if(i > 0) result[x.substr(0, i)] = v
    else result[x] = ''
  })
  return result
}

function readFile(filename){
  return new Promise((yes, no)=>{
    fs.readFile(filename, 'utf8', (err, data)=>{
      if(err){
        console.error(err)
        return no(err)
      }
      yes(data)
    })
  })
}

