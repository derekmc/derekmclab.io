
$html = <<HTML
  <h1> Test Rack </h1>
  <p> This is a test rack application </p>
HTML

class App
  def call(env)
    status = 200
    headers = { "Content-Type" => "text/html" }
    body = [$html]
    [status, headers, body]
  end
end

run App.new
