function pressButton(n){
  let homeRow = keyMap['']
  let key = homeRow[n - 1]
  // console.log('press: ' + key)
  if(n in temp.pressedButtons) return
  temp.pressedButtons[n] = true
  temp.buffer.push(key)
  temp.emitted = false
}

function emit(ch){
  temp.emitted = true
  console.log(' emit: ' +  ch)
}

function buttonTouches(list){
  console.log('touches', list)
  if(!temp.lastTouch){
    temp.lastTouch=[]
    temp.emitted = false
  }
  let homeRow = keyMap['']
  temp.pressedButtons = {}
  if(!temp.emitted){
    if(list.length < temp.lastTouch.length){
      let s = temp.buffer.join(' ')
      let ch = keyMap[s] ?? "??"
      emit(ch)
    }
  }

  if(list.length > temp.lastTouch.length){
    temp.emitted = false
  }
  for(let i=0; i<list.length; ++i){
    let n = list[i] - 1
    let key = homeRow[list[i] - 1]
    temp.pressedButtons[n] = true
    if(temp.buffer.indexOf(key) < 0){
      temp.buffer.push(key)
    }
  }
  for(let i=0; i<homeRow.length; ++i){
    let ch = homeRow[i]
    if(!temp.pressedButtons[i]){
      let j = temp.buffer.indexOf(ch)
      if(j >= 0) temp.buffer.splice(j, 1)
    }
  }
  console.log('buffer', temp.buffer)
  temp.lastTouch = list
}
function releaseButton(n){
  let homeRow = keyMap['']
  let key = homeRow[n - 1]
  delete temp.pressedButtons[n]
  let s = temp.buffer.join(' ')
  if(!temp.emitted){
    let ch = keyMap[s] ?? "??"
    emit(ch)
  }
  let i = temp.buffer.indexOf(key)
  if(i >= 0){
    temp.buffer.splice(i, 1)
    //console.log('buffer', temp.buffer)
  }  
}


