let charTableText = `
  :r n o e a t i l space shift
  r: R right s u up * c h alt _
  n: left N m d pgup = p x + -
  o: s m O / w g : $ { }
  e: u d \\ E f k [ ] esc select
  a: down pgdn w f A q j v ~ !
  t: < > g k z T b y ( )
  i: c p ^ # j b i \` ' "
  l: h x & | v y % L 0 @
  space: ; ctrl , . tab back5 back enter space ?
  shift: 1 2 3 4 5 6 7 8 9 shift
`

// for chords, order doesn't matter.
// for slides, order does matter.
/*
let charSource = `
homerow: r n o e a t i l space shift

chords:
  r e u
  n o m
  r o s
  n e d
  r l h
  r i c
  n l x
  n i p
  a e f
  a o w
  t e k
  t o g
  a l v
  a i j
  t i b
  t l y 
:chords

slides:
  a t q
  t a z
  a r down
  r a up
  a n pgdn
  n a pgup
  r t *
  n t =
  t r <
  t n >
  space r ;
  space n ctrl
  space o ,
  space e .
  space a tab 
  space t back5
  space i back
  space l enter
  r space alt
  r shift _
  a space ~
  a shift !
  n space +
  n shift -
  t space (
  t shift )
  o space {
  o shift }
  o e /
  o i :
  o l $
  e o \\
  e i [
  e l ]
  i o ^
  i e #
  i l \`
  i space '
  i shift "
  l o &
  l e |
  l i %
  l space 0
  l shift @

  shift r 1
  shift n 2
  shift o 3
  shift e 4
  shift a 5
  shift t 6
  shift i 7
  shift l 8
  shift space 9
:slides
`
*/

// "quote words" similar to perl
function qw(s){
  return s.trim().split(/\s+/g)
}

function parseCharTable(table){
  let lines = table.split("\n")
  let prefixMap = {}
  for(let line of lines){
    let x = line.indexOf(':')
    if(x < 0) continue
    let prefix = line.substring(0, x).trim()
    let row = qw(line.substring(x + 1))
    prefixMap[prefix] = row
  }
  return buildKeyMap(prefixMap)
}

function buildKeyMap(prefixMap){
  let keyMap = {}
  let n = 0
  let map_count = 1
  let home_row = []
  for(let n=0; map_count > 0; ++n){
    map_count = 0
    for(let pre in prefixMap){
      let row = prefixMap[pre]
      let prefix_n = pre.length
      if(prefix_n == n){
        ++map_count
        if(n == 0) home_row = row
        for(let j=0; j<row.length; ++j){
          let sequence = pre + ' ' + home_row[j]
          keyMap[sequence.trim()] = row[j] 
        }
        keyMap[pre + row]
      }
    }
  }
  keyMap[''] = home_row
  return keyMap
}

