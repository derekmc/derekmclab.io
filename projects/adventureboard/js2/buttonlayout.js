
function buttonOrigin({settings, container, buttonIndex}){
  let {width, height} = container
  let {buttonTop, buttonCenter, mirrorImage, spaceOnTop
    buttonWidth, buttonHeight}  = settings

  let y0 = buttonTop * (height - 3*buttonHeight)
  let x0 = buttonCenter * (width/2 - 2*buttonWidth)
  if([3,4,7,8,10]).indexOf(buttonIndex) > -1){
    x0 = w - x0 - 2 * buttonWidth
  }
  if([2,4,6,8].indexOf(buttonIndex) > -1){
    x0 += buttonWidth
  }
  y0 += (buttonHeight *
    spaceOnTop? [2,2,2,2, 1,1,1,1, 0,0] :
                [1,1,1,1, 0,0,0,0, 2,2])
  
  if(mirrorImage){
    x0 = width - x0 - buttonWidth
  }
  return [x0, y0]
}
function buttonMap({settings, container, x, y}){
  let {buttonWidth, buttonHeight} = settings
  let bw = buttonWidth
  for(let i = 1; i <= 10; ++i){
    let [x0, y0] = buttonOrigin({settings, container, buttonIndex: i})
    if(x > x0 && y > y0 && x < x0 + buttonWidth && y < y0 + buttonHeight){
      return i
    }
  }
  return -1
}
