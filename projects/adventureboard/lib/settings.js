const settings = window.settings = defSettings()

//test()
async function test(){
  settings.options({
    'alpha' : [1, 2, 3],
    'beta' : 'a b c d elefant'.split(' '),
    'gamma' : [1, 2, 4, 5],
    'delta' : [true, false]
  })
  settings.values({
    'alpha': 3
  })
  settings.show()
  let result = await settings.result()
  console.log(JSON.stringify(result))
  setTimeout(test, 1000)
}

function defSettings(){
const settings = {}
let container = document.body
let optionList = {}

let transform = (x)=>{
  try{
    return JSON.parse(x)
  }catch(e){
    return x
  }
}
let parse = x=> isNaN(parseFloat(x))? x : parseFloat(x)
let cmp = (x, y) => parse(x) < parse(y)? -1 : 1
// TODO function to wait to get selected values.
// TODO function to transform values
settings.buttonClick = buttonClick
settings.chooseMenu = chooseMenu
settings.makeMenuChoice = makeMenuChoice
settings.show = showSettings
settings.makeChoice = makeChoice
settings.cancelChoice = cancelChoice
settings.done = done
settings.setTransform = x=>transform = x
settings.values = (x)=>{
  for(let name in x){
    let value = '' + x[name]
    let list = optionList[name]
    if(list){
      for(let i=0; i<list.length; ++i){
        if(value == list[i]){
          while(--i >= 0)
            list.push(list.shift())
          break
        }
      }
    }
  }
}
settings.result = ()=>{
  return new Promise((yes, no)=>{

    finished_callback = ()=>{
      let result = {}
      for(let name in optionList){
        let options = optionList[name]
        let value = transform(options[0])
        result[name] = value;
      }
      yes(result)
    } 
  })
}
settings.options = (x)=>{
  optionList = x
  for(let name in optionList){
    let options = optionList[name]
    for(let i=0; i<options.length; ++i){
      options[i] = "" + options[i]
    }
  }
}

let style = `
  body{
    font-family: sans-serif;
    background: #ddd;
    font-size: 17px;
  }
  .center{ text-align: center; }
  .settings-title{
    background: black;
    color: #fff;
    font-size: 19px;
    font-weight: bold;
    padding: 8px 10px;
  }
  .table-settings{
    background: #000;
    font-family: sans-serif;
    border-radius: 0px;
    padding: 2px;
    border-spacing: 0;
  }
  .table-settings tr:nth-child(odd){
    background: #eee;
  }
  .table-settings th{
    text-align: right;
  }
  .table-settings tr:nth-child(even){
    background: #fff;
  }
  .table-settings td, .table-settings th{
    margin: 0;
    padding: 4px 9px;
  }
  select{
    font-weight: bold;
    background: #fff;
    border: 2px solid black;
    padding: 4px 9px;
    font-size: 17px;
  }
  button{
     border: 2px solid black;
     background: rgba(255,255,255,1.0);
     color: #000;
     font-weight: bold;
     padding: 3px 12px;
     border-radius: 5px;
     font-size: 17px;
  }
  button.cancel{
    background: rgba(0,0,0,1.0);
    color: #fff;
  }
`

function showSettings(title, __container){
  if(!title) title = "Setting Options"
  if(__container) container = __container
  let rows = ''
  for(let name in optionList){
    let choices = optionList[name]
    let value = choices.length? choices[0] : "";
    rows += `<tr><th> ${name} </th>` +
      `<td><button onclick='settings.buttonClick("${name}")'>${value}</button></td></tr>`
  }
  container.innerHTML = `
    <table class='table-settings'>
      <tr> <td class='center settings-title' colspan='2'> ${title} </td></tr>
      ${rows}
    </table>
    <br>
    <button onclick="settings.done()"> Done </button>
    <style>${style}</style>
  `
}

async function buttonClick(name){
  let title = `Select '${name}' Value`
  let list = optionList[name]
  let value = await choose(list, title)
  if(value){
    for(let i=0; i<list.length; ++i){
      if(list[0] == value) break
      list.push(list.shift())
    }
  }
  settings.show()
}
let choice_callback = null
let choice_cancel = null
let finished_callback = null

function makeChoice(){
  let value = choiceList.value
  let action = choice_callback
  choice_callback = null
  choice_cancel = null
  if(action){
    action(value)
  }
}
function cancelChoice(){
  let action = choice_callback
  choice_callback = null
  choice_cancel = null
  if(action) action(null)
}
function done(){
  container.innerHTML = ''
  if(finished_callback)
    finished_callback()
}

function makeMenuChoice(name){
  choice_callback(name)
}

function chooseMenu(__container, items, title){
  container = __container

  let button_list = ''
  let list = items.slice()
  for(let i=0; i<list.length; ++i){
    let item = list[i];
    button_list += `<button onclick="` +
      `settings.makeMenuChoice('${item}')">` +
      ` ${item}</button> `
  }
  return new Promise((yes, no)=>{
    choice_callback = yes
    choice_cancel = yes
    container.innerHTML = `
       <style>${style}</style>
       <h3 class='settings-title'> ${title} </h3>
       ${button_list}
       <br><br>
       <button onclick='settings.cancelChoice()' class='cancel'> Cancel </button>
    `
  })
}

function choose(options, title){
  let option_list = ''
  let value = options[0]
  let list = options.slice(0).sort(cmp)
  for(let i=0; i<list.length; ++i){
    let option = list[i];
    option_list += `<option value="${option}"` +
      ` ${value == option? 'selected': ''}>${option}</option>`
  }
  return new Promise((yes, no)=>{
    choice_callback = yes
    choice_cancel = yes
    if(container)
      container.innerHTML = `
         <style>${style}</style>
         <h3 class='settings-title'> ${title} </h3>
         <select id='choiceList'>
           ${option_list}
         </select>
         <br><br>
         <button onclick='settings.makeChoice()'> Update </button>
         <button onclick='settings.cancelChoice()' class='cancel'> Cancel </button>
      `
  })
}
function id(x){
  return document.getElementById(x)
}
return settings
}
