window.addEventListener("gamepadconnected", gpconnect)

let gp_interval = null
let gp_down = []

function gpconnect(e){
  const gp = navigator.getGamepads()[e.gamepad.index]
  if(!gp_interval)
    gp_interval = setInterval(gp_loop, 8)
  console.info("Gamepad conneted. " + gp.buttons.length + " buttons")
}


// artificial keydown and keyup events
function gp_loop(){
  //console.log('loop')
  let gp
  try{
    gp = navigator.getGamepads()
  }catch(err){
    return
  }
  for(let i=0; i<gp.length; ++i){
    let g = gp[i]
    if(!g) continue
    for(let j=0; j<g.buttons.length; ++j){
      let code = `gp${i}-${j}`
      let pressed = g.buttons[j].pressed
      if(pressed && !gp_down[code]){
        keydown({code, preventDefault})
        //console.info("pressed " + code)
        gp_down[code] = true
      }
      if(!pressed && gp_down[code]){
        keyup({code}, preventDefault)
        gp_down[code] = false
      }
    }
  }
  function preventDefault(){
  }
}
