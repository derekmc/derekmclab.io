let data = window.Note.data
let items = {}

const wordBreakRegex = /[\s.,\/#!$%\^&\*;:{}=\-_`~()]/
const alphabet =
    "Aeronlaitscu\\d]f[kBCusDhEc*FG" +
    "\/sm$w:g}{d/mxHp=-+|h&xv%y@" +
    "0f<wIvjq!~#c^p\"jb\"'k<g>yzb)(413285769.;,JKLMN?"

let temp = {}
// keyboard temp vars
temp.keyemitted = false
temp.keycount = 0
temp.slidekey = 0 // todo slide timeout
temp.activeButtons = []
temp.buffer = ""
temp.ctrl = false
temp.shift = 0
temp.shiftHold = false // the flag used for shiftlock
temp.lastchar = ""
temp.repeatInterval
temp.mousedown = false
temp.repeatDelay = 0
temp.pressedkeys = {}
let hiccup_keytiming = {}
let hiccupSave = {}

//initial character for each button
const buttonAlphabet = "eronlaitsc"
// the display labels for the buttons
const specialChars = {
  "c": "space",
  "cc": "space",
  "cl": "enter",
  "s": "shift",
  "ss": "shift",
  "ra": "up",
  "ar": "down",
  "rn": "right",
  "rc": "rekey",
  "es": "select",
  "an": "pgdn",
  "na": "pgup",
  "nr": "left",
  "cn": "ctrl",
  "ci": "back",
  "ct": "back5",
  "ca": "tab",
  "il": "`",
  "rs": "_",
  "ec": "ctrl",
  //"oe": "esc",
}
const ctrlChars = {
  "e" : "right",
  "o" : "left",
  "i" : "up",
  "l" : "down",
  "c" : "ctrl",
  "space" : "ctrl",
  "t" : "cut",
  "r" : "copy",
  "n" : "paste",
  "a" : "selectall",
}

let table = {}

function activateKeys(){
  temp.activeButtons = []
  let rekey = data.buttonkeys !== undefined && data.buttonkeys.length < 10
  if(rekey) return
  for(let k in temp.pressedkeys){
    temp.activeButtons.push(getKeyButton(k))
  }
}

function buttonMap(x, y){
  const {
    mirrorImage, buttonOpacity,
    buttonWidth, buttonHeight,
    buttonTop, buttonCenter
  } = data.settings
  let W = cc.width
  let H = cc.height
  let vert = H>W
  let MinSize = Math.min(W,H)
  let MaxSize = Math.max(W,H)

  let m = buttonMargin
  let w = buttonWidth
  let h = buttonHeight

  // button map
  // this is mapping the screen geometry to the button indexing
  // order used by the alphabet.
  // the map order confusing because the most frequent letters are earlier,
  // to correspond with the easiest to reach buttons in a two thumb layout.
  // we may want to change the alphabet order in the future to make this less confusing.
  let bmap = "243168750-99".split("")
  //bmap[9] = bmap[10] = -1
  //bmap[8] = '10'
  bmap[9] = bmap[8] = "10"

  if(mirrorImage) x = W - x
  let x0 = buttonCenter*(W/2 - 2*h - m/2)
  ///let offset = (1-buttonTop)*H
  let maxOffset = (H - 3*h - m/2 - marginTop)/H
  let offset = 3*h + m/2 + marginTop + maxOffset*buttonTop*H
  let direct = vert? -1 : -1

  let j = Math.floor((offset + direct * y)/h)
  if(j < 0) return 0
  if(x > x0 && x < 2*bwidth*w + x0){
    let i = Math.floor((x-x0)/bwidth/w)
    return parseInt(bmap[i + 4*j])
  }
  if(x < W - x0 && x > W - x0 - 2 * bwidth*w){
    let i = Math.floor(4 + (x - W + x0)/bwidth/w)
    return parseInt(bmap[i + 4*j])
  }
  return 0
}
function mousedown(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  temp.mousedown = true
  e.changedTouches = [e]
  e.touches = []
  touchstart(e)
}

function mousemove(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  if(!temp.mousedown) return
  e.touches = [e]
  touchmove(e)
}

function mouseout(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  mouseup(e)
}
function mouseup(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  temp.mousedown = false
  e.changedTouches = [e]
  touchend(e)
}
function keyShortName(longname){
  // this maps from the full character name
  // used by key events, to the abbreviated
  // character name shown on the keyboard,
  // and used by this program for each key
  if(longname.indexOf('arrow') == 0){
    return longname.substr(5)
  }
  let shortnamemap = {
    'pageup': 'pgup',
    'pagedown': 'pgdn',
    'control': 'ctrl',
    'backspace': 'back',
    ' ': 'space',
  }
  if(longname in shortnamemap){
    return shortnamemap[longname]
  }
  return longname
}
function keyconvert(k){
  if(k == "enter") k = "E"
  if(k == "tab") k = "T"
  if(k == "alt") k = "A"
  if(k == "shift") k = "S"
  if(k == "backspace") k = "B"
  if(k == "control") k = "C"
  if(k == "arrowleft") k = "L"
  if(k == "arrowright") k = "R"
  if(k == "arrowup") k = "U"
  if(k == "arrowdown") k = "D"
  if(k == "escape") k = "Q"
  if(k == "numlock") k = "N"
  if(k == "meta") k = "M"
  if(k == "pageup") k = "O"
  if(k == "pagedown") k = "P"
  if(k == "home") k = "G"
  if(k == "end") k = "H"
  if(k == "fn") k = "F"
  if(k == "scrolllock") k = "I"
  if(k == "capslock") k = "J"
  //if(k == "printscreen") k = "P"
  //if(k.length > 1) k = "X"
  return k
}
// one handed typing
function keydown(e){
  if(!document.querySelector('#textout')) return
  e.preventDefault()
  let keyname = e.key?.toLowerCase() ?? e.code
  if(keyname == 'unidentified') keyname = e.keyCode
  // ignore these codes because numpads add extra codes
  if(e.keyCode == 0 || e.keyCode == 144) return
  //console.info("keyname: " + keyname)

  if(data.settings.hiccupTimeout > 0){
    let dt = e.timeStamp - hiccup_keytiming[keyname]
    if(dt < data.settings.hiccupTimeout){
      temp = JSON.parse(hiccupSave.temp)
      items['text_window'].setData(hiccupSave.textwin)
      console.trace('hiccup restore state')
      return
    }
  }

  let rekey = data.buttonkeys !== undefined && data.buttonkeys.length < 10
  if(!rekey && data.settings.hardwareInputMode == "direct"){
    temp.buffer = ""
    emitChar(keyShortName(keyname))
    return
  }

  //let k = keyname
  if(temp.pressedkeys[keyname]){
    return false;
  }
  if(temp.keycount == 0) temp.buffer = ""
  ++temp.keycount
  temp.pressedkeys[keyname] = true
  if(rekey){ // rekey in progress
    settings.hardwareInputMode = "chorded"
    let keymap = "1320576498"
    data.buttonkeys.push(keyname)
    e.preventDefault()
    let b = data.buttonkeys.length
    if(b > 9){
      temp.activeButtons = []
      status("Rekey Finished: " + data.buttonkeys.join(','))
    }
    else temp.activeButtons = [parseInt(keymap[b])+1]
    show()
    return
  }
  temp.keyemitted = false
  // status(`${k} pressed`)
  if(temp.keycount <= 2){
    let b = getKeyButton(keyname)
    if(b <= 0) return
    e.preventDefault()
    if(data.settings.shiftLock && temp.buffer?.length == 0 && b == 9){
      temp.shiftHold = true
    }
    if(data.settings.shiftLock && temp.shiftHold){
      if(temp.buffer?.length >= 2){
        temp.buffer = temp.buffer.substring(temp.buffer?.length - 1)
      }
    }
    if(temp.buffer?.length >= 2) return
    temp.buffer += buttonAlphabet[b-1]
    temp.activeButtons.push(b)
    let ch = table[temp.buffer]
    if(ch && ch.length && ch == temp.lastchar){
      repeat(temp.buffer)
    }else{
      halt()
    }
  }
  activateKeys()
  show()
}

function getKeyButton(keyname){
  // this is distinct from the map in the draw function.
  // The map in the draw function is about the button layout
  // from the screen geometry, as such it goes up to twelve.
  let keymap = "1320576498";  

  let buttonkeys
  if(data.buttonkeys){
    buttonkeys = data.buttonkeys
  } else {
    // default keyboard
    buttonkeys = "jkl;uiop '".split('')
    //alternate numpad default.
    if(buttonkeys.indexOf(keyname) < 0){
      buttonkeys = ".369025814".split('') }
  }
  let b = buttonkeys.indexOf(keyname)
  if(b < 0) return 0
  return parseInt(keymap[b]) + 1
}
async function keyup(e){
  if(!document.querySelector('#textout')) return
  let keyname = e.key?.toLowerCase() ?? e.code
  if(keyname == 'unidentified') keyname = e.keyCode
  if(data.settings.hiccupTimeout > 0){
    hiccup_keytiming[keyname] = e.timeStamp
    hiccupSave = {
      textwin: items['text_window'].getData(),
      temp: JSON.stringify(temp)
    }
  }
  if(data.settings.hardwareInputMode == "direct"){
    temp.buffer = ""
    return;
  }
  if(!temp.pressedkeys[keyname]) return
  --temp.keycount
  halt()
  delete temp.pressedkeys[keyname]
  if(data.buttonkeys && data.buttonkeys.length < 10){
    // rekey in progress.
    return
  }
  if(data.settings.shiftLock && temp.shiftHold){
    let b = getKeyButton(keyname)
    // release shift
    if(b == 9){
      temp.shiftHold = false
      // if buffer is one character, prepend shift,
      // otherwise, emit the current buffer
      if(temp.buffer?.length == 1){
        temp.buffer = "s" + temp.buffer
      } else if(temp.buffer?.length > 2){
        temp.buffer = temp.buffer.substring(temp.buffer?.length - 2)
      }
    } else {
      // dont release key due to shiftlock
      if(temp.buffer?.length >= 2){
        temp.buffer = temp.buffer.substring(temp.buffer?.length - 2)
      }
      return
    }
  }
  temp.activeButtons = []
  if(!temp.keyemitted && temp.keycount < 2){
    await emit()
  }
  temp.keyemitted = true
  if(temp.keycount == 0){
    temp.buffer = ""
    temp.keyemitted = false;
  }
  if(temp.keycount == 1)
    for(let key in temp.pressedkeys)
      temp.buffer = buttonAlphabet[getKeyButton(key) - 1]
  activateKeys()
  show()
}
function initKeyboard(){
  initTable()
}
function initTable(){
  let base = 10
  let maxlen = 2
  //table[alphabet[0]] = alphabet[0]
  for(let i=1, k=0; i<alphabet.length; ++i){
    let ch = alphabet[i]
    let x = i+k
    let code = ""
    while(x > 0){
      let j = x%base
      if(j == 0) j = base
      code = alphabet[j] + code
      x = (x - j)/base
    }
    if(code[0] == code[1]){
      ++k; --i
      table[code] = code[0]
    } else {
      table[code] = ch
    }
  }
  for(let q in specialChars){
    table[q] = specialChars[q]
  }
}


let timeout = null
function clearBuffer(){
  temp.buffer = ""
  show()
}
function repeat(_buffer){
  halt()
  if(table[_buffer] == "shift") return
  if(table[_buffer] == "ctrl") return
  temp.repeatInterval = setInterval(()=>{
    if(temp.repeatDelay){
      --temp.repeatDelay
      return
    }
    temp.buffer = _buffer; emit()
  }, repeatdt)
}
function halt(){
  clearInterval(temp.repeatInterval)
}
function pause(){
  temp.repeatDelay = 2
}

async function emitChar(ch){
  let textwin = items['text_window']

  let back = (n)=>()=>{
    textwin.moveLeft(undefined, n)}
  let forward = (n)=>()=>{
    textwin.moveRight(undefined, n)}
  if(temp.ctrl){
    if(ch in ctrlChars){
      ch = ctrlChars[ch]
    }
  }
  if(ch.length == 1){
    if(temp.shift){
      ch = ch.toUpperCase()
    }
    textwin.insert(ch)
    if(temp.shift == 1)
      temp.shift = 0
  }else{
    if(ch=="ctrl"){
      temp.ctrl = !temp.ctrl
      status("ctrl")
      //return
    }
    else if(ch=='cut') await cut()
    else if(ch=='copy') await copy()
    else if(ch=='paste') await paste()
    else if(ch=='selectall') selectAll()
    else if(ch=="select") textwin.startSelect()
    else if(ch=="tab") textwin.insert("    ")
    else if(ch=="enter") textwin.insert("\n")
    else if(ch=="space") textwin.insert(" ")
    else if(ch=="shift") temp.shift = (temp.shift + 1)%3
    else if(ch=="up") textwin.moveUp()
    else if(ch=="down") textwin.moveDown()
    else if(ch=="left") back(1)()
    else if(ch=="right"){
      let behavior = data.settings.rightArrowIsSpace
      let insertSpace = behavior == "Always" ||
        behavior == "End of Text" && textwin.cursor == textwin.text.length
      if(insertSpace) textwin.insert(" ")
      else forward(1)()
    }else if(ch=="pgup") back(textwindow/2)()
    else if(ch=="pgdn") forward(textwindow/2)()
    else if(ch=="rekey"){ 
      temp.ctrl = false; rekey() }
    else if(ch=="back")
      textwin.backspace()
    else if(ch=="back5")
      for(let i=0; i<5; ++i)
        textwin.backspace()
    else if(ch=="back10")
      for(let i=0; i<10; ++i)
        textwin.backspace()
  }
  //temp.buffer = ""
  //if(temp.buffer.length > 1)
    //temp.buffer = temp.buffer.substr(0,1)
  temp.lastchar = ch
  show()
  async function readClip(){
    let s = clipboard
    try{
      s = await navigator.clipboard.readText()
    }catch(e){
      console.warn('cannot read user\'s clipboard', e)
    }
    return s
  }
  async function writeClip(s){
    clipboard = s
    try{
      await navigator.clipboard.writeText(s)
    }catch(e){
      console.warn('cannot write user\'s clipboard', e)
    }
  }
  async function cut(){
    await writeClip(textwin.selection)
    textwin.insert('')
  }
  async function copy(){
    await writeClip(textwin.selection)
  }
  async function paste(){
    textwin.insert(await readClip())
  }
  function selectAll(){
    textwin.endSelect()
    textwin.cursor = textwin.text.length
    textwin.startSelect()
    textwin.cursor = 0
  }
}
async function emit(){
  let ch = table[temp.buffer] ?? ""
  await emitChar(ch)
}

function rekey(){
  data.buttonkeys = [] 
  data.settings.chordedLayout = "Custom"
  setTimeout(()=>{temp.activeButtons = [2]; show()}, 10)
  status("Rekey 1-10")
}


