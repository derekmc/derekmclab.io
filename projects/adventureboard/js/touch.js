
temp.touchemitted = false

function touchstart(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  // if we prevent default then the iframe doesn't get focus.
  // we could pass through key events with messages from the parent frame
  // to avoid this issue.
  //e.preventDefault()
  temp.touchemitted = false
  let touches = e.changedTouches
  let rekey = data.buttonkeys !== undefined && data.buttonkeys.length < 10
  if(rekey){
    delete data['buttonkeys']
    status("Rekey cancelled by touch or click")
    return show()
  }
  for(let i=0; i<touches.length; ++i){
    if(temp.buffer?.length >= 2) break
    let touch = touches[i]
    let b = buttonMap(touch.clientX, touch.clientY)
    temp.buffer += buttonAlphabet[b-1]
  }
  let char = table[temp.buffer]
  if(char && char.length && char == temp.lastchar){
    repeat(temp.buffer)
  }else{
    halt()
  }
  
  activateButtons([...e.touches, ...e.changedTouches])

  show(); 
}
function touchend(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  e.preventDefault()
  halt()
  let rekey = data.buttonkeys !== undefined && data.buttonkeys.length < 10
  if(rekey) return
  if(!temp.buffer?.length){ 0 }
  else if(temp.buffer.length >= 2){
    temp.touchemitted = true
    emit()
  }else if(!temp.touchemitted && temp.buffer.length == 1){
    let touch = e.changedTouches[0]
    let b = buttonMap(touch.clientX, touch.clientY)
    let a = buttonAlphabet[b-1]
    if(a != temp.buffer)
      temp.buffer += buttonAlphabet[b-1]
    temp.touchemitted = true
    emit()
  }
  if(e.touches && e.touches.length){
    activateButtons(e.touches, true)

  } else {
    temp.touchemitted = false
    temp.buffer = ""
    activateButtons([])
  }
  show()
}

function touchmove(e){
  if(!document.querySelector('#textout')) return
  if(data.settings.screenKeyboard == "none" ||
     data.settings.screenKeyboard == "displayOnly") return
  e.preventDefault()
  pause()
  let rekey = data.buttonkeys !== undefined && data.buttonkeys.length < 10
  if(rekey) return
  if(e.touches.length == 1){
    activateButtons(e.touches)
    let touch = e.touches[0]
    let b = buttonMap(touch.clientX, touch.clientY)
    let a = buttonAlphabet[b-1]
    if(a != temp.buffer){
      let char = table[temp.buffer + a]
      if(char && char.length && char == temp.lastchar){
        repeat(temp.buffer + a)
        pause()
      }else{
        halt()
      }
    }
  } else {
    activateButtons(e.touches, true)
  }
  show()
  //drawOverlay()
}

function activateButtons(touches, rebuffer){
  //let rebuffer = false
  temp.activeButtons = []
  if(rebuffer){
    if(temp.buffer.length > 1)
      temp.buffer = temp.buffer.substr(0,1)
  }

  if(touches){
    for(let i=0; i<touches.length; ++i){
      let touch = touches[i]
      let x = touch.clientX
      let y = touch.clientY
      let b = buttonMap(x, y)
      if(b > 0 && !temp.activeButtons.includes(b)){
        temp.activeButtons.push(b)
      }
      if(rebuffer && i >= temp.buffer.length){
        temp.buffer += buttonAlphabet[b-1]
      }
    }
  }
  //status(`active buttons: ${temp.activeButtons.join(", ")}`)
}
