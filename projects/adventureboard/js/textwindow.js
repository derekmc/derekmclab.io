
function sleep(t){
  return new Promise(y=>setTimeout(y, t*1000))
}

function textWindowEditor(target, container, text, config){
  let items = {}
  if(!target) target = window
  if(!container) container = document.body
  if(!text) text = 'replace text'
  text = text + text + text + '\n'
  text = text + text + text
  let tw = items['text_window'] = new TextWindow(text)
  let clipboard = ""

  //loadData(items)
  //setInterval(saveData, Save_Interval, items)
  //tw.text = text
  //tw.config(config)
  tw.config({wrap: true})
  target.addEventListener('keydown', keydown)
  tw.renderHTML(container)
  console.log('editor ready')
  
  async function keydown(e){
    let code = e.keyCode
    //console.log('code: ' + code)
    let key = e.key
    let k = key.toLowerCase()
    if(e.ctrlKey){
      if(k == 'x') await cut()
      if(k == 'c') await copy()
      if(k == 'v') await paste()
      if(k == 'a'){
        tw.endSelect()
        tw.cursor = tw.text.length
        tw.startSelect()
        tw.cursor = 0
      }
      tw.renderHTML(container)
      return
    }
    if(k.indexOf('arrow') == 0) k = k.substring(5)
    //if(k == "shift") tw.startSelect()
    if(k == "left") tw.moveLeft(e.shiftKey)
    if(k == "up") tw.moveUp(e.shiftKey)
    if(k == "right") tw.moveRight(e.shiftKey)
    if(k == "down") tw.moveDown(e.shiftKey)
    if(k == "backspace") tw.backspace()
    if(k == "delete") tw.deleteAction()
    if(k == "enter") tw.insert('\n')
    if(key.length == 1) tw.insert(key)
    tw.renderHTML(container)
  }
  async function readClip(){
    let s = clipboard
    try{
      s = await navigator.clipboard.readText()
    }catch(e){
      console.warn('cannot read user\'s clipboard', e)
    }
    return s
  }
  async function writeClip(s){
    clipboard = s
    try{
      await navigator.clipboard.writeText(s)
    }catch(e){
      console.warn('cannot write user\'s clipboard', e)
    }
  }
  async function cut(){
    await writeClip(tw.selection)
    tw.insert('')
  }
  async function copy(){
    await writeClip(tw.selection)
  }
  async function paste(){
    tw.insert(await readClip())
  }
}
//setTimeout(test, 200)
function test(){
  let text = "This is a test."
  let testdiv = document.getElementById('testoutput')
  textWindowEditor(null, testdiv, text)
  //TextWindow.test('This is a test of the emergency broadcast system. ')
  //TextWindow.test(`Thisisatestoftheemergency12345 broadcastasdfasdfasdfasdfwefwedsfsafsfasdffwefwefwsystem`)

}
//setTimeout(()=>console.log('here'), 4000)
class TextWindow{
  static wordBreak = /[^A-Za-z0-9_]/
  static nowrap(text){
    return text.split('\n').map(x=>x+'\n')
  }
  static wrap(text, width, wordwidth=8){
    let lines = text.split('\n').map(x=>x+'\n')
    for(let i=0; i<lines.length; ++i){
      let l = lines[i]
      let last = l[l.length - 1]
      let wrapline = l.length > width + 1 ||
        l.length == width + 1 && last != '\n'
      if(wrapline){
        let maxback = Math.min(width, l.length, wordwidth) + 1
        let back = 0
        for(;back <= maxback; ++back){
          if(l[width - back].match(TextWindow.wordBreak)){
            if(back > 0) --back // don't put the space on the newline
            break
          }
        }
        if(back > maxback) back = 0
        let rest = l.substring(width - back)
        lines[i] = l.substring(0, width - back)
        lines.splice(i+1, 0, rest)
      }
    }
    return lines
  }
  static getIndex(lines, row, col){
    let index = 0
    if(row < 0) row = 0
    if(row > lines.length - 1) row = lines.length - 1
    for(let i=0; i<row; ++i){
      index += lines[i].length
    }
    let n = lines[row]?.length ?? 0
    let valid = !isNaN(col) && col != undefined
    if(valid && col > n-1) col = n-1
    if(!n) col = 0 
    index += col
    return index
  }
  static getRowCol(lines, index){
    for(let row=0; row<lines.length; ++row){
      let n = lines[row].length
      if(index < n) return [row, index]
      index -= n
    }
    let m = lines.length - 1
    return [m, lines[m].length + 1] // one place beyond the end of the last line. 
  }
  static test(text){
    console.log("TextWindow.test(text)")
    text += text + text + text + '\n'
    text += text + text + text
    //testStatic()
    //testRender()
    testMovement()

    function testRender(){
      let tw = new TextWindow(text, Math.floor(text.length-120))
      console.log(tw.renderText())
      tw.config({wrap: false})
      console.log(tw.renderText())
    }
    async function testMovement(){
      await sleep(0.1)
      let tw = new TextWindow(text, Math.floor(text.length/2+140))
      let testdiv = document.getElementById('testoutput')
      tw.renderHTML(testdiv)
      let motionMap = {"<": "Left", ">": "Right", "^": "Up", "v": "Down"}
      let motions = "<<<^>>vv^>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<v^v^>>v"
      console.log('motions: ' + motions)
      if(0) for(let i=0; i<motions.length; ++i){
        let motion = motionMap[motions[i]]
        tw['move' + motion]()
        await sleep(0.2)
        tw.renderHTML(testdiv)
      }
      //await sleep(1)
      tw.cursor = Math.floor(text.length/2 + 140)
      tw.config({wrap: false})
      // motions = "<<<^>>vv^>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<v^v^vv>>v"
      console.log('motions: ' + motions)
      for(let i=0; i<motions.length; ++i){
        let motion = motionMap[motions[i]]
        tw['move' + motion]()
        await sleep(0.2)
        tw.renderHTML(testdiv)
      }
      //tw.renderHTML(testdiv)
    }
    function testStatic(){
      let lines = TextWindow.wrap(text, 40)
      for(let i=0; i<text.length+1; ++i){
        let [row, col] = TextWindow.getRowCol(lines, i)
        let j = TextWindow.getIndex(lines, row, col)
        console.log('index row col index2: ', i, row, col, j)
      }
      console.log(lines.join('|'))
    }
  }

  #text
  #config
  #cursor
  #selectStart
  getData(){
    return this.data   
  }
  setData(x){
    this.data = x
  }
  get data(){
    return {
      text: this.#text,
      config: this.#config,
      cursor: this.#cursor,
      selectStart: this.#selectStart
    }
  }
  set data(x){
    if('text' in x) this.#text = x.text
    if('config' in x) this.#config = x.config
    if('cursor' in x) this.#cursor = x.cursor
    if('selectStart' in x) this.#selectStart = x.selectStart
  }
  constructor(t = 'replace text', c = 0){
    this.#text = t
    this.#cursor = c
    this.#selectStart = 5
    this.#config = {
      width: 40, height: 10,
      wordwidth: 8, wrap: true,
      halign: 0.65, valign: 0.5
    }
  }
  config(x){
    let props = ['width', 'wordwidth', 'wrap', 'height']
    for(let prop of props){
      if(x && x.hasOwnProperty(prop)){
        this.#config[prop] = x[prop]
      }
    }
  }
  set text(s){
    this.#text = s
  }
  get text(){
    return this.#text
  }
  get selection(){
    let a = this.#cursor
    let b = this.#cursor
    if(this.#selectStart !== null){
      a = Math.min(a, this.#selectStart)
      b = Math.max(b, this.#selectStart)
      return this.#text.substring(a, b)
    }
    return ""
  }
  get cursor(){
    return this.#cursor
  }
  set cursor(c){
    this.#cursor = c
  }
  insert(s){
    let a = this.#cursor
    let b = this.#cursor
    if(this.#selectStart !== null){
      a = Math.min(a, this.#selectStart)
      b = Math.max(b, this.#selectStart)
      this.#cursor = a
      this.#selectStart = null
    }
    let start = this.#text.substring(0, a)
    let rest = this.#text.substring(b)
    this.#text = start + s + rest
    this.#cursor += s.length 
  }
  backspace(){
    if(this.#selectStart !== null){
      this.insert("")
      return
    }
    if(this.#cursor <= 0) return
    let start = this.#text.substring(0, this.#cursor - 1)
    let rest = this.#text.substring(this.#cursor)
    this.#text = start + rest
    --this.#cursor
  }
  deleteAction(){
    if(this.#selectStart !== null){
      this.insert("")
      return
    }
    if(this.#cursor >= this.#text.length) return
    let start = this.#text.substring(0, this.#cursor)
    let rest = this.#text.substring(this.#cursor + 1)
    this.#text = start + rest
  }
  startSelect(){
    if(this.#selectStart == null)
      this.#selectStart = this.#cursor
  }
  endSelect(){
    this.#selectStart = null
  }
  moveUp(select, repeat=1, down){
    if(select !== undefined){
      if(select && this.#selectStart == null) this.startSelect()
      if(!select && this.#selectStart !== null) this.endSelect()
    }
    let getLines = TextWindow[this.#config.wrap? 'wrap' : 'nowrap'] 
    let lines = getLines(this.#text, this.#config.width, this.#config.wordwidth)
    let [row, col] = TextWindow.getRowCol(lines, this.#cursor)
    if(down){
      while(repeat > 0 && row < lines.length - 1){
        ++row; --repeat }
      if(repeat > 0 && select){
        col = lines[lines.length - 1].length }
    } else {
      while(repeat > 0 && row > 0){
        --row; --repeat }
      if(repeat > 0 && select){
        col = 0 }
    }
    this.#cursor = TextWindow.getIndex(lines, row, col)
  }
  moveLeft(select, repeat=1){
    if(select !== undefined){
      if(select && this.#selectStart == null) this.startSelect()
      if(!select && this.#selectStart !== null) this.endSelect()
    }
    if(this.#cursor <= 0) return
    --this.#cursor
    if(repeat > 1) this.moveLeft(select, repeat-1)
  }
  moveRight(select, repeat=1){
    if(select !== undefined){
      if(select && this.#selectStart == null) this.startSelect()
      if(!select && this.#selectStart !== null) this.endSelect()
    }
    if(this.#cursor >= this.#text.length) return
    ++this.#cursor
    if(repeat > 1) this.moveRight(select, repeat-1)
  }
  moveDown(select, repeat=1){
    this.moveUp(select, repeat, true)
  }
  renderCallbacks({append, newline, cursor, openSelect, closeSelect}){
    let cfg = this.#config
    let cs = this.#cursor
    let text = this.#text
    let lines = TextWindow[cfg.wrap? 'wrap':'nowrap'](
      text, cfg.width, cfg.wordwidth)
    let [row, col] = TextWindow.getRowCol(lines, cs)
    let w = cfg.width, h = cfg.height
    let startline = Math.max(0, Math.floor(row - h * cfg.valign)) 
    let endline = Math.min(lines.length, startline + h)
    let startcol = cfg.wrap? 0 : Math.max(0, Math.floor(col - w * cfg.halign))
    let endcol = startcol + w

    let a0, a1, b0, b1
    if(this.#selectStart !== null){
      let a = Math.min(this.#selectStart, cs)
      let b = Math.max(this.#selectStart, cs)
      ;[a0, a1] = TextWindow.getRowCol(lines, a)
      ;[b0, b1] = TextWindow.getRowCol(lines, b)
      if(a1 < startcol && (b0 > a0 || b1 > startcol)) a1 = startcol
      if(b1 > endcol && (a0 < b0 || b0 < endcol)) b1 = endcol
      a1 -= startcol
      b1 -= startcol
    }

    let j = col - startcol
    let selopen = false
    if(a0 < startline){
      a0 = startline
      a1 = 0
    }
    for(let i=startline; i < endline; ++i){
      let s = lines[i].substring(startcol, endcol)
      if(s[s.length - 1] == '\n')  // remove ending newline
        s = s.substring(0, s.length - 1)
      if(i == a0){
        append(s.substring(0, a1))
        openSelect()
        selopen = true
        if(i == b0){
          append(s.substring(a1, b1))
          closeSelect()
          selopen = false
          append(padright(s.substring(b1), w - b1))
        } else {
          append(padright(s.substring(a1), w - a1))
        }
      } else if(i == b0){
        append(s.substring(0, b1))
        closeSelect()
        selopen = false
        append(padright(s.substring(b1), w - b1))
      } else if(i == row && j>=0 && j < w + 1){
        append(s.substring(0, j))
        cursor(s[j], j==w)
        append(padright(s.substring(j + 1), w - j - 1))
      } else {
        append(padright(s, w))
      }
      if(i < endline - 1) newline()
    }
    for(let i=0; i < h - (endline-startline); ++i){
      newline()
      append(padright('', w))
    }
    if(selopen){
      closeSelect()
    }
    function padright(s, n){
      while(s.length < n) s += ' '
      return s
    }
  }
  renderText(){
    let s = ""
    let actions = {
      append: x => s+=x,
      newline: x => s+='\n',
      cursor: x => s+='|' + x,
      openSelect: x => x,
      closeSelect: x=> x
    }
    this.renderCallbacks(actions)
    return s
  }
  renderHTML(container){
    if(!container){
      console.log('no container')
      container = document.createElement('div')
      //document.body.appendChild(container)
    }
    container.innerHTML = ""
    let text = x=>document.createTextNode(x)
    let br = x=>document.createElement('br')
    let cursor = (x, nowidth)=>{
       let span = document.createElement('span')
       span.appendChild(text(x ?? ' '))
       span.classList.add('cursor')
       if(nowidth) span.style.marginRight = '-1em'
       container.appendChild(span)
    }
    let stack = [container]
    let actions = {
      append: x=>container.appendChild(text(x)),
      newline: x=>container.appendChild(br()),
      cursor: cursor,
      openSelect: ()=>{
        let span = document.createElement('span')
        span.classList.add('select')
        container.appendChild(span)
        stack.push(span)
        container = span },
      closeSelect: ()=>{
        stack.pop()
        container = stack[stack.length - 1]}
    }
    this.renderCallbacks(actions)
    return container
  }
  renderCanvas(canvas, width, height){
  }
}
