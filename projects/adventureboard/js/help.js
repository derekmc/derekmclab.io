function showHelp(){
  let container = document.querySelector('#keyboardContainer')
  document.body.className = ''
  document.body.classList[data.settings.mirrorImage? 'add' : 'remove']('mirrorImage')
  container.innerHTML = `
    <div style="max-width: 500px; text-align: justify; margin-left: 2.5%;">
      <button onclick='main()'> Return </button>
      <h2> Adventure Keyboard Help </h2>
      <a href="https://www.termsfeed.com/live/655ec042-66ea-4bf0-9e23-2fd20ecee9fb">
        Privacy Policy</a>
      <p> Adventure Keyboard is a chorded keyboard, which allows typing
       using only 10 keys, either pressed in combination(chords), or sequence(swipes).
       Chords use at most two buttons, meaning that this system can work
       on most if not all low end keyboards and number pads.
      <p> Also, the two button chord system is designed for touchscreens as well,
       where it could be very difficult to touch more than two keys at once.
      <p>
       Adventure Keyboard has two basic chord types: sequential or swipes,
       and traditional non-sequential chords. The non-
       sequential traditional chords are used for the letters, as it
       allows for faster smoother input.  Note that you do not have
       to press the keys simultaneously, it only means that the order
       of the keys pressed doesn't matter.
      <p>Additionally, the chords of these common letters are designed to
       always be on opposite sides of the keyboard, which makes input
       easier on a touchscreen or even a hardware keyboard.
      <p> This project is called adventure keyboard, because it
       is designed to be used in environments where it would be
       difficult to bring a full normal keyboard, whether that
       with a handheld numpad or simply on a smartphone touchscreen.
       You get the benefit of many possible configurations, while
       only having to learn one typing system.

      <h3> Home Keys </h3>
      <image width="300px" src='../../image/keyboard_standard.png'/>
      <h3> Keyboard Overview </h3>
      <image width="300px" src='../../image/keyboard_overview.png'/>

      <h3> Punctuation and Numbers</h3>
      <image width="300px" src='../../image/keyboard_punctuation.png'/>
      <image width="300px" src='../../image/keyboard_digits.png'/>


      <h3> Get Adventure Keyboard </h3>
      <p>
       Adventure Keyboard, is as of this writing, available at
       <a href='https://derekmc.gitlab.io'>https://derekmc.gitlab.io</a>
       for either online use, or a zipped html download.  It is possible to
       extract this zipped html file, and use it locally offline,

      <h3> Saving and Loading Content </h3>

      <p> Currently text entered is stored in the browser's built in "localStorage" only.
      It is possible to view or edit the raw text, so that it can be copied into,
      or pasted from, another app or editor.  Simply click the button "Text", to
      show the raw text format in a textbox.


      <h3> Touch Screen </h3>

      <p> The easiest way to start using Adventure Keyboard
       is with a touchscreen on mobile or a touchscreen laptop.
       There are many different
       possible ways to hold your phone or tablet, but you will
       achieve the best results if you put it down on a flat surface,
       and use your pointer fingers.
       Other possible touchscreen layouts are using your thumbs in portrait,
       or using thumbs or pointer fingers in landscape mode.

      <h3> Mobile Numpad Keyboard </h3>

      <p> Adventure keyboard can be used with a numpad keypad, typically connected
       via bluetooth or usb.  If you press the "rekey" button, then you can choose
       each key you want to use, by pressing the corresponding keyboard key when it
       lights up.  Typically this works better if numlock is on.

      <p> One option for holding a numpad keyboard, is to put it on a lanyard,
        and wear it around your neck.  Experiment with grips and setups, as
        well as what fingers you use, until you find something that works good for you.


      <h3> One Handed Keyboard </h3>

      <p> Adventure keyboard may be useful for people who may only be able to
       use one hand, whether that is because of a disability, or because the
       other hand may be occupied, or simply if they prefer that ergonomically.

      <h3> Full Keyboard Modes and Practice </h3>

      <p> Adventure keyboard can also be used or practiced with a full keyboard.
       some of the options are right handed(jkl;uiop), left handed(asdfqwer),
       split(zx./as;'q[), or home row (asdfjkl;), you can also rekey to use
       any custom layout, as described above.

      <h3> Other Settings </h3>

      <p> Adventure Keyboard has options for laying out and sizing the touchscreen keyboard,
       options for showing or hiding the button laber

      <p> <b>Hiccup Timeout</b>
        Some external numpads will automatically release and repress certain
        keys, such as backspace, when you type chords. 
        We call this rapid, automated release and repress of a key a 'hiccup'.
        To deal with these keypress hiccups, you can set 'hiccupTimeout',
        and if a key is released for less than this time before it is repressed,
        the chordstate and buffer is restored to what it was before the release.


      <h3> How to learn the keyboard </h3>
      
      <p> It is recommended that you learn a few keys at a time, by creating practice texts
      to copy, using only those letters. For example, the first for keys are "enor", so
      for example, you might type the following to practice these letters.

      <blockquote>
        err ren nero en ne rn nr reno erroneo ronn rone ore re 
      </blockquote>

      <blockquote>
        rate late tall ten neat tron rotate net relate orient tree treat tare tear
      </blockquote>

      <p> Once you learn those keys, as well as the next row "aitl"
      you might integrate those by practicing

      <h3> Expectations: Speed vs Comfort vs Endurance </h3>

      <p> While typing speed may be the most important factor in
      the decision to use a keyboard, it is not the only consideration.
      comfort and endurance are important as well, as they will allow
      you to type a long time without injury.

      <p> After first learning the keyboard, you may only have a typing speed
      of 5-10 words per minute, but after practicing a 10-15 minutes a day,
      you should be able to get up to 15-20 words per minute, which is comparable
      to writing by hand.

      <p> With more extensive practice, speeds of 20-30 words per minute are possible.

      <p> Adventure Keyboard is definitely not recommended as complete replacement for
      normal typing, but rather as another option that requires less hardware or
      only one hand, in situations where two handed typing is not possible.

      <p> You can compare this to handwriting or using a calculator, or to the
      difference between biking and driving a car.

      <h3> Advanced Technique</h3>
      <h4>Stacked vs Square Layout </h4>

      <p> In order to work well on touch screens, Adventure Keyboard
      is designed so that common letter chords always use keys on
      the opposite side of the keyboard. It can be easier to press
      two keys at the same time, with opposite hands, whether on
      a touchscreen or hardware keyboard input.

      <p> The most ergonomic hardware layouts, such as "vertical", "numpad"
      or "controller grip" make it difficult to map the 2 halves of the
      keyboard to the two different hands, while preserving the same
      geometric key mapping.

      <p> To work around this, and allow a more ergonomic hardware layout,
      the keys can be remapped so that the leftmost and rightmost keys
      are "stacked" above and below the center keys, rather than
      to the side.

      <p> In such a layout, the 'a' key is above the 't' key, and the 
      'r' key is below the 'n' key.  Similarly the 'l' key is above 
      the 'i' key, and the 'e' key is below the 'o' key.

      <p> Thus the left-hand and right hand keys are all "stacked" in
      a single line, so that common letter chords are always
      pressed using opposite hands, rather than two fingers on the
      same hand, which can be more awkard and require

      <h4> Right arrow as Space Key </h4>

      <p> In some configurations, the space key may be harder
      to reach or require moving your fingers.  In this case,
      to can change the settings to use the right arrow as a
      second space key.

      <p> This means that you can input a space character,
      without your fingers leaving the 'homerow' keys, if
      that makes it difficult on your particular layout, or
      if you want to type with just 8 keys instead of 10.

      <p> "Right arrow as space", can be configured to work
      anywhere in the text, or only at the end of current
      body of text, so that you can still use the right
      arrow for navigational purposes otherwise.
      

      <h3> Other similar Keyboards </h3>
      
      <p>There are other chorded or swiping keyboards avaible, especially on
      mobile devices. Some options are "MessagEase", a swiping 10 button keyboard,
      or "ASETNIOP".  After learning both of these, I decided to create Adventure
      Keyboard as it combines chords for letters, and swiping or chords for less
      common keys, and works well with an off the shelf number pad, whereas as of
      this writing, those other keyboard do not have hardware options.

      <h3> Other uses of an external numberpad</h3>
      <p>External numpads also make great portable gaming controllers
      for mobile devices. Many retro emulators are available on
      mobile, with a wide variety of roms available.
      Other mobile games that work with an external controller, may also
      work with a numpad.

      <p> Of course, it is always possible to bring a fullsized bluetooth
      keyboard as well, but typically those are larger and require a flat
      surface for use, compared to a numpad only.
      <p>
      <br>

      <h3> Suggested Hardware Layouts </h3>
      <image width="700px" src='../../image/keylayout.png'/>
      <h3> Full Keyboard Map </h3>
      <image width="700px" src='../../image/keymap.png'/>
      <button onclick='main()'> Return </button>
    </div>
  `
}
