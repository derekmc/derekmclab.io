
const http = require('http')
const port = 3000
const host = '0.0.0.0'
const fs = require('fs')
const path = require('path')
const suppress_output = false
const serve_handler = require('serve-handler')
let demo_render = require('./www/demo.ws').render

main()
async function main(){
  let server = http.createServer(handler)
  server.listen(port, host)
  console.log(`\nserving on ${host}:${port}`)
}

/*
async function loadPages(){
  let pageList = [
    ['./include/demo.html', (x)=>demoPage = x],
  ]
  console.log('loading pages.')
  let promises = []
  for(let i=0; i<pageList.length; ++i){
    let [filename, assign] = pageList[i]
    promises[i] = (async ()=>{
      let content = await readFile(filename)
      console.log(`  ${filename} : ${content.length}`)
      assign(content)
    })()
  }
  await Promise.all(promises)
}
*/

async function handler(req, res){
  let spliturl = req.url.split('?')
  let path = spliturl[0]
  let queryString = spliturl[1]
  let setCookieHeaders = []
  let cookies = parseCookies(req.headers.cookie)
  let args = {
    path: path,
    query: parseQuery(queryString),
    cookies: cookies,
    setCookie: setCookie,
    data: null,
  }
  if(req.method == "POST"){
    let body = await handlePost()
    args.data = parseQuery(body)
  }
  if(path == '/demo' || path == '/demo.ws'){
    return finish(demo_render(args), {})
  }
  return serve_handler(req, res, {
    cleanUrls: true,
    "public": 'www',
  })

  // TODO get cookies and set cookies
  // TODO traverse www to find script paths
  function handlePost(){
    return new Promise((yes, no)=>{
      console.log('post request')
      let body = ''
      req.on('data', (data)=>{
        body += data
        //console.log('body: ' + body)
      })
      req.on('end', ()=>{
        yes(body)
      })
    })
  }
  function redirectPost(url){
    res.writeHead(303, {Location: url}).end()
  }
  function redirect(url){
    res.writeHead(307, {Location: url}).end()
  }

  function finish(src, args){
    let headers = {}
    headers['Content-type'] = 'text/html'
    headers['Set-Cookie'] = setCookieHeaders
    res.writeHead(200, headers)
    let pageSrc = renderPage(src, args)
    res.end(pageSrc)
  }
  // 
  function setCookie(name, value, extra){
    // remove controlled characters from name 
    name = name.replaceAll(';', '').replaceAll('\n', ' ')
    let str = `${name}=${encodeURIComponent(value)}`
    if(extra){
      if(typeof extra == "string"){
        extra = extra.replaceAll('\n', ' ')
        if(extra.length) str += "; " + extra
      } else if(Array.isArray(extra)){
        for(let i=0; i<extra.length; ++i){
          str += "; " + extra[i]}
      }
    }
    setCookieHeaders.push(str)
  }
}
function renderPage(src, args){
  for(let k in args){
    let v = args[k]
    src = src.replaceAll('{{' + k + '}}', v)
  }
  return src
}

function escapeChars(s){
  return s.replace("\\", "\\\\").
    replace("\n", "\\n").
    replace("\"", "\\\"")
}
function parseQuery(str){
  let result = {}
  if(!str) return result
  let pairs = str.split("&").forEach((x)=>{
    let i = x.indexOf('=')
    let k = x.substr(0,i)
    let v = x.substr(i+1)
    let replacements = [['+', ' ']]
    for(let [s, ss] of replacements){
      v = v.replaceAll(s, ss)}
    v = decodeURIComponent(v)
    if(i > 0) result[x.substr(0, i)] = v
    else result[x] = ''
  })
  return result
}

function readFile(filename){
  return new Promise((yes, no)=>{
    fs.readFile(filename, 'utf8', (err, data)=>{
      if(err){
        console.error(err)
        return no(err)
      }
      yes(data)
    })
  })
}

function parseCookies(str){
  let cookies = {}
  if(!str) return cookies
  str.split(";").forEach(function(cookie){
    let [name, ...rest] = cookie.split('=')
    if(name) name = name.trim()
    else return
    let value = rest.join('=').trim()
    cookies[name] = decodeURIComponent(value)
  })
  return cookies
}
