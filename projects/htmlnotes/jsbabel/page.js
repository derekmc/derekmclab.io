
function renderPage(){
  return `
<div id="main" class="fullscreen pageformat0 disablejs">
  <div id="topbar">
    <span id="fmtchoose">
      <button onclick="selectFormat(0)" class="format0 left" title="Markdown"> html </button>
      <button onclick="selectFormat(1)" class="format1 left"> css </button>
      <button onclick="selectFormat(2)" class="format2 left"
        title="Click tab twice to enable/disable javascript."> &nbsp;-js- </button>
      <button onclick="selectFormat(3)" class="format3 left"> json </button>
    </span>
    <span id="pagesettings" class="floatright">
      <!--button onclick="nextView()" id="viewbutton" title="Layout"> View </button-->

      <button onclick="prevPage()" id="prevbutton" title="Prev"> &lt; </button>
      <button onclick="nextPage()" id="nextbutton" title="Next"> &gt; </button>
      <button onclick="pageMenu()" id="pagebutton" title="Page"> Page 1 </button>
    </span>
    <!-- <button onclick="nextFormat()" id="formattext"> HTML </button> -->
  </div>
  <!--<span id="formattext"></span>-->
  <div id="main-inner">
    <div class="editarea">
      <textarea id="dataline" readonly rows=1 onclick="selectFormat(3);">Note.data = /* json */;
      </textarea>
      <textarea id="edit" rows=10 spellcheck=false></textarea>
      <!--iframe id="edit"></iframe-->
    </div>
    <!-- <input type="checkbox" id="checkjs" name="javascript" value="Javascript"></input>
    <label for="checkjs"> Javascript </label> -->
    <iframe id="output"></iframe>
  </div>
  <button id="exit_fullscreen" onclick="leaveFullscreen()"> X </button>
</div>
<style>
  /* only do vertical split on narrow screens */
  /*@media screen and (max-width: 400px){
    #viewbutton{
      display: none;
    }
  }*/
  .mirror{
    transform: scaleX(-1);
  }
  .narrow #viewbutton{
    display: none;
  }
  .firstpage #prevbutton, .lastpage #nextbutton{
  }
  *{
    font-family: sans-serif;
  }
  .blur{
    filter: blur(2px);
  }
  #exit_fullscreen{
    display: none;
    position: fixed;
    left: 50%;
    transform: translate(-50%, 0);
    top: 1.25em;
    max-width: 150px;
    background: #000;
    color: #fff;
    font-weight: bold;
    padding: 0.6em 0.8em;
    border-radius: 50%;
    border: 3px solid rgba(255,255,255,0.5);
    z-index: 400;
  }
  .fullscreen_view #exit_fullscreen{
    display: block;
    animation: hideIn 4s, fadeIn 1.6s;
    animation-delay: 0s, 4s;
    opacity: 0.85;
    cursor: ne-resize;
  }
  @keyframes hideIn {
    0% { opacity: 0; }
    100% { opacity: 0; }
  }
  @keyframes fadeIn {
    0% { opacity: 0; }
    100% { opacity: 0.85; }
  }
  .fullscreen_view #output{
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    z-index: 300;
  }

  #menubutton{
    border-radius: 0;
    border: none;
    padding: 1px 18px;
    font-size: 120%;
  }
  .menuContainer{
    z-index: 9999;
  }

  #main{
     position: relative;
    z-index: 200;
    margin: 0;
    text-align: left;
    display: flex;
    flex-flow: column;
  }
  .hsplit #main-inner{
    display: flex;
    flex-flow: row;
  }
  body{
    margin: 0;
    padding: 0;
    background: #ccc; 
    overflow: hidden;
  }
  body.night{
    background: #222; 
  }

  .night #dataline{
    border-left: 3px solid #66a;
    border-right: 3px solid #66a;
  }
  #dataline{
    width: 100%;
    border: none;
    border-left: 3px solid #aae;
    border-right: 3px solid #aae;
    padding: 5px;
    box-sizing: border-box;
    display: none;
    overflow: hidden;
   /* pointer-events: none;*/
    user-select: none;
    outline: none;
    background: #dfdff8;
    color: #445;
    z-index: -10;
    margin: 0px;
    font-family: monospace;
  }
  .night #dataline{
    color: #dfdff8;
    background: #445;
  }
  #edit{
    font-family: monospace;
    padding: 5px;
    box-sizing: border-box;
    line-height: 2.5ch;
    background: #fff;
    word-wrap: break-word;
    font-weight: normal;
    font-size: 12px;
    color: #000;
    background-attachment: local;
  }
  #edit::selection{
    background: #284264;
    color: #fff;
  }
  .night #edit{
    color: #fff;
    background: #000;
  }

  .night #edit::selection{
    color: #284264;
    background: #fff;
  }

  .fullscreen_view #main-inner{
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    z-index: 300;
  }
  #output{
    padding: 0px;
    box-sizing: border-box;
    background: #fff;
  }
  .night #output{
    border-color: #66a;
    background: #888;
  }
  .night #edit{
    border-color: #66a;
    background: #000;
    color: #fff;
  }
  #output, #edit{
    border: none;
    border-left: 3px solid #aae;
    border-right: 3px solid #aae;
    outline: none;
    width: 100vw;
    height: calc(50vh - 25px);
    overflow-x: auto;
    overflow-wrap: normal;
    white-space: pre;
  }
  .view #output{
    order: 1;
  }
  .view #edit{
    order: 2;
  }
  .vsplit #output{
    border-top: 2px solid #aae;
  }
  .night .vsplit #output{
    border-top: 2px solid #66a;
  }
  .vsplit #edit{
    border-bottom: 2px solid #aae;
  }
  .night .vsplit #edit{
    border-bottom: 2px solid #66a;
  }
  .hsplit #output{
    border-left: 2px solid #aae;
  }
  .hsplit #edit{
    border-right: 2px solid #aae;
  }
  .night .hsplit #output{
    border-left: 2px solid #66a;
  }
  .night .hsplit #edit{
    border-right: 2px solid #66a;
  }
  .hsplit #output, .hsplit #edit{
    display: inline-block;
    width: calc(50vw - 1px);
    height: calc(100vh - 45px);
    margin: 0px;
  }
  .fullscreen #output, .fullscreen #edit{
    height: calc(100vh - 45px);
  }
  .fullscreen #edit{
    display: inline-block;
  }
  .fullscreen #output{
    display: none;
  }
  .fullscreen.view #output{
    display: inline-block;
  }
  .fullscreen.view #edit{
    display: none;
  }
  /* adjust for dataline in 'js' format */
  .pageformat2 #dataline{
    display: block;
  }
  .pageformat2 #edit, .pageformat2.hsplit #edit{
    height: calc(100vh - 80px);
  }
  .pageformat2.vsplit #edit{
    height: calc(50vh - 51px);
  }
  .pageformat0 .format0,
  .pageformat1 .format1,
  .pageformat2 .format2,
  .pageformat3 .format3
  { z-index: 90; }
  
  .pageformat0 .format1,
  .pageformat1 .format2,
  .pageformat2 .format1,
  .pageformat3 .format2
  { z-index: 80; }
  
  .pageformat0 .format2,
  .pageformat1 .format3,
  .pageformat2 .format0,
  .pageformat3 .format1
  { z-index: 70; }
  
  .pageformat0 .format3,
  .pageformat1 .format0,
  .pageformat2 .format3,
  .pageformat3 .format0
  { z-index: 60; }
  
  .narrow .format0, .narrow .format1, .narrow .format2, .narrow .format3{
    margin-right: -18px;
    border: 3px solid #88c;
    border-bottom: none;
  }
  .night .narrow .format0,
  .night .narrow .format1,
  .night .narrow .format2,
  .night .narrow .format3{
    border: 3px solid #66a;
    border-bottom: none;
  }
  /*
  */

  .format0, .format1, .format2, .format3{
    position: relative;
    background: #66a;
    color: #fff;
    border: 3px solid #66a;
    margin-right: 1px;
    border-bottom: none;
    border-radius: 8px 8px 0px 0px;
    margin-top: 4px;
    margin-bottom: -2px;
    padding-top: 0;
    /*margin-bottom: -4px;*/
  }
  .night .format0, .night .format1,
  .night .format2, .night .format3{
    color: #fff;
    background: #336;
  }
  .disablejs.night .format2,
  .disablejs.night.narrow .format2{
    background: #888;
    color: #ccc;
  }
  .night .pageformat2.disablejs .format2,
  .night .pageformat2.disablejs.narrow .format2{
    background: #888;
    color: #ccc;
  }
  .narrow.disablejs .format2{
    border-color: #aaa;
    background: #888;
    color: #ccc;
  }
  .disablejs .format2{
    font-weight: normal;
    background: #666;
    text-decoration: line-through;
    border-color: #666;
  }
  /*.night .disablejs.pageformat2 .format2,*/
  .disablejs.pageformat2 .format2 {
    border-color: #aaa;
    color: #888;
    background-color: #fff;
  }
  .pageformat0 .format0,
  .pageformat1 .format1,
  .pageformat2 .format2,
  .pageformat3 .format3{
    color: #00a;
    background: #fff;
    border: 3px solid #aae;
    border-bottom: none;
    margin-top: 2px;
    padding-top: 0;
    margin-bottom: -4px;
    border-radius: 8px 8px 0px 0px;
    z-index: 99;
  }
  .night.pageformat0 .format0,
  .night.pageformat1 .format1,
  .night.pageformat2 .format2,
  .night.pageformat3 .format3{
    color: #fff;
    background: #000;
    border-color: #66a;
  }
  .pageformat3 #edit.dirty{
  /*#edit.dirty{*/
    background: #eee;
    color: #888;
  }

  button.disabled{
    color:#fff;
    background: #888;
    box-sizing: border-box;
  }
  #main.night button.disabled{
    background: #444;
    color:#ccc;
  }
  /*
  */
  button.neutral{
    border: 2px solid black;
    background: #ccc;
    color:#000;
  }
  button{
    border: none;
    /*border: 3px solid #666;*/
    display: inline-block;
    font-size: 110%;
    background: #225;
    color: #fff;
    font-weight: bold;
    padding: 1px 12px;
    min-height: 38px;
    min-width: 30px;
    margin: 0px;
    margin-top: 1px;
    overflow-x: hidden;
    box-sizing: border-box;
    border-radius: 8px;
  }
  .night button{
    background: #003;
  }
  #main .night button{
    color: #fff;
    background: #66a;
  }
  #fmtchoose{
    margin-right: -100%;
    z-index: -10;
  }
  .alignright{
    text-align: right;
  }
  .floatright{
    float: right;
  }
</style>`;
}
