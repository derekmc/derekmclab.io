// constants
const EditDelay = 1800
const MinRefreshInterval = 3800
const RenderDelay = 1200 // the delay to turn on javascript for a remote source
const ResizeDelay = 260
const SaveInterval = 500
const NarrowWidth = 500
const Formats = ["html", "css", "js", "json"]
const Views = ["edit", "view", "split"]
const FontSizes = ["8px", "10px", "12px", "15px", "18px", "22px", "28px", "35px", "45px", "60px", "80px", "120px", "180px"]
const MinBodyFontIndex = 2
const MaxBodyFontIndex = 5
const DefaultAppUrl = "https://derekmc.gitlab.io/projects/htmlnotes/htmlnotes.html"
const DemoUrls = {
  "ArrowInput" : "https://gist.githubusercontent.com/derekmc/bf8fc179a235eb237088860658e769d2/raw/be3b6538376ea3a115478669a55aed127d918965/arrowtest.html",
  "MultivariateNewton" : "https://derekmc.gitlab.io/newton.html",
  "TextFade" : "https://gist.githubusercontent.com/derekmc/7af2c0e8487794cfb28feac2f332f7f0/raw/7c0833823506ab2cddb733fcf5901415394ec849/textfade.html",
  //"BasicCanvas": "https://gist.githubusercontent.com/derekmc/a4265a70cdbe7da15629ad25db25aa70/raw/5f36a15fbb68ef9f8a8d777b2f4012c948d1d436/c1example.html",
  "SimpleEditor": "https://gist.githubusercontent.com/derekmc/8fe6d62bdc07d0af10be87a7adfde0d2/raw/a6523c82964cd58dec44027d75621a8698d38f52/simple_editor.html",
  "PrecisionDraw": "https://derekmc.gitlab.io/projects/precisiondraw/precisiondraw.html",
  "CubeRotate": "https://gist.githubusercontent.com/derekmc/c5f1da4ba238eb9137138076d3728915/raw/5cb12add7cc559c4bdbcfd890c45eeb576a91c6d/cuberotate3d.html",
  //"AdventureKeyboard": "https://derekmc.gitlab.io/projects/adventureboard/adventureboard.html",
  "BuildThePlatforms": "https://gist.githubusercontent.com/derekmc/2a3b6b043a518421aba79d389f39a8d6/raw/db788efb1aac6f1b05dd409ac3bc015d9b354c98/platformer.html",
  "MidiButtons": "https://gist.githubusercontent.com/derekmc/73720002c6918a965d871a2ff53857ad/raw/415c9547214db6c20d65415c5b078cf0d1672524/digital_organ.html",
}
const DemoMenus = {
  "Input": ["ArrowInput", "MidiButtons"],
  "Game": ["BuildThePlatforms"],
  "Math": ["MultivariateNewton"],
  "Canvas": ["TextFade", "CubeRotate"],
  //"HTML": [""],
  "Tools": ["SimpleEditor", "PrecisionDraw"],
}
// const SaveKeyName = "NoteDataKey"
//const SaveTime = 10 * 1000

// globals
const initialDataState = {
  dirty: false,
  formatIndex: 0,
  viewIndex: 2,
  pageIndex: 0,
  docList: [],
  customEditor: null,
  font_size_index: 3,
  night_mode: false,
}

let data = cloneObj(initialDataState)
let temp = {
  lastRefresh: 0,
  enablejs: false,
  fullscreen_view: false,
  closefullscreenbutton: false,
  instanceId: -1, // prevent multiple instances
}
let libtemplate = (libtype, lib)=>{
  let s = ''
  let names = Object.keys(lib)
  for(let i=0; i<names.length; ++i){
    let name = names[i]
    let src = lib[name]
    s += `/${''}* ${libtype}: ${name} *${''}/\n`
    s += src + "\n"
    s += `/${''}* END *${''}/\n`
  }
}

// TODO check sections for mismatched comments or tags.
// don't render that section if it has those errors.
//functions
let template = ({html, css, js, json, notedataid, jslib, csslib}, enablejs)=>{
  let libstr = ''
  if(csslib) libstr += libtemplate('CSS', csslib)
  if(jslib) libstr += libtemplate('JS', jslib)
  try{
    if(!json.length){
      json = "{}"
    }else{
      json = JSON.stringify(JSON.parse(json))
    }
  }catch(e){
    console.warn("invalid template json", json, e)
    json = "{}"
  }
  enablejs = enablejs || temp.enablejs
  notedataid = notedataid ?? randomString(10, util.alphanums)

  //return `
  return `
<${''}html>
  <${''}head>
    <${''}meta name="viewport" content="width=device-width, initial-scale=1">
    <${''}style>
/${''}* CSS */
${css}
/${''}* END */
${libstr}
    <${''}/style>
  <${''}/head>
  <${''}body>
    <div class="markdown-src">
<${''}!-- HTML -->
${html}
<${''}!-- END -->
    </div>
    <div style="display: none;"></div>
    <${''}script>
      let datakey = "__HTML_NOTES__:notedataid=${notedataid}"
      let inframe = (window!=window.top)
      window.Note = {}
      window.Note.data =
/${''}* JSON */
${json};
/${''}* END */
      (function(){
        window.Note.save = saveData
        window.Note.load = loadData
        window.Note.autoSave = autoSave
        function getNoteModule(){
          if(!window['Note']) window.Note = {}
          return window.Note
        }
        function loadData(){
          let note = getNoteModule()
          if(!inframe){
            try{
              let savedata = localStorage.getItem(datakey)
              if(savedata && savedata.length){
                note.data = JSON.parse(savedata)
              }
              window.addEventListener("message", onMessage)
            } catch(e){
              console.log("No local data")
              console.log(e)
            }
          } else {
            return note.data
          }
        }

        let firstLogCall = true
        let originalLogFunc = console.log
        function log(x, ...rest){
          if(firstLogCall)
            document.body.appendChild(document.createElement("hr"))
         else
            document.body.appendChild(document.createElement("br"))
          document.body.appendChild(
            document.createTextNode(x + " " + rest.join(" ")))
          firstLogCall = false
          originalLogFunc(x, ...rest)
        }
        console.log = log

        let SaveInterval = ${SaveInterval}

        window.addEventListener("load", ()=>{
          convertMarkdown()
          autoSave()
          loadData() // dont wait for async
        })
        let saveIntervalRef = null
        function autoSave(enable){
          if(enable === undefined) enable = true
          clearInterval(saveIntervalRef)
          if(enable){
            saveIntervalRef = window.setInterval(saveData, SaveInterval)
          }
        }
        // this is preferred over a 'localstorage' polyfill for a frame,
        // so the programmer doesn't assume this is the browsers localstorage
        function saveData(){
          let note = getNoteModule()
          if(inframe){
            let msg_obj = {
              action: "saveData",
              data: JSON.stringify(note['data'])
            }
            let message = JSON.stringify(msg_obj)
            window.parent.postMessage(message)
          } else {
            localStorage.setItem(datakey, JSON.stringify(note['data']))
          }
        }
        let md_subs = [
          /(\\n|^)\\s*######\\s([^\\s#].*)\\n*/g, "\\n<${''}h6 id=\\"$2\\">$2<${''}/h6>\\n",
          /(\\n|^)\\s*#####\\s([^\\s#].*)\\n*/g, "\\n<${''}h5 id=\\"$2\\">$2<${''}/h5>\\n",
          /(\\n|^)\\s*####\\s*([^\\s#].*)\\n*/g, "\\n<${''}h4 id=\\"$2\\">$2<${''}/h4>\\n",
          /(\\n|^)\\s*###\\s*([^\\s#].*)\\n*/g, "\\n<${''}h3 id=\\"$2\\">$2<${''}/h3>\\n",
          /(\\n|^)\\s*##\\s*([^\\s#].*)\\n*/g, "\\n<${''}h2 id=\\"$2\\">$2<${''}/h2>\\n",
          /(\\n|^)\\s*#\\s*([^\\s#].*)\\n*/g, "\\n<${''}h1 id=\\"$2\\">$2<${''}/h1>\\n",
          /\\n(\\s*)[\\*\\-](.*)/g, '\\n<${''}ul><${''}li>$2<${''}/li><${''}/ul>',
          /\\n+\\n(?=[^#\\n])/g, "\\n\\n<${''}br><${''}br>",
          /\\n+\\n/g, "\\n",
          /__([^_\\n]*)__/g, "<${''}b>$1<${''}/b>",
          /\\*\\*([^_\\n]*)\\*\\*/g, "<${''}b>$1<${''}/b>",
          /_([^_\\n]*)_/g, "<${''}i>$1<${''}/i>",
          /\\*([^_\\n]*)\\*/g, "<${''}i>$1<${''}/i>",
          /\\!\\[([^\\]\\n]*)\\]\\(([^\\)\\n]*)\\)/g, "<${''}img src=\\"$2\\" alt=\\"$1\\"><${''}/img>",
          /\\[([^\\]\\n]*)\\]\\(([^\\)\\n]*)\\)/g, "<${''}a href=\\"$2\\" target=\\"_blank\\">$1<${''}/a>",
        ]
        function onMessage(){
          try{
            let message = JSON.parse(e.data)
            // console.log('received message', message)
            if(message.hasOwnProperty("event")){
              if(message.event == "keydown" && typeof keydown != "undefined"){
                keydown(message)
              }
              if(message.event == "keyup" && typeof keyup != "undefined"){
                keydown(message)
              }
            }
          } catch(e){
            console.warn("error processing message: " + e.data)
          }
        }
        function convertMarkdown(){
          let containers = document.getElementsByClassName("markdown-src")
          for(let j=0; j<containers.length; ++j){
            let container = containers[j]
            let src = container.innerHTML
            for(var i=0; i<md_subs.length-1; i += 2){
              var search = md_subs[i]
              var replace = md_subs[i+1]
              src = src.replace(search, replace)
            }
            container.innerHTML = src
          }
        }
      })()
      //window.addEventListener('load', htmlNotesMainFunc)
      //function htmlNotesMainFunc(){
/${''}* JS */
${enablejs? js : ""}
/${''}* END */
      //}
    <${''}/script>
  <${''}/body>
<${''}html>`
}

window.addEventListener("load", init)



function selectFormatWithContent(){
  if(data.viewIndex == 1) return // dont change format in view mode
  if(data.pageIndex == data.docList.length - 1){
    data.formatIndex = 1
    updatePageFormat()
    return
  }
  // change to format with content.
  let currentFormat = data.formatIndex
  let doc = data.docList[data.pageIndex] ?? emptyDoc()
  data.docList[data.pageIndex] = doc
  let content = doc[Formats[data.formatIndex]]
  if(!content || !content.trim().length){
    do{
      data.formatIndex = (data.formatIndex + 1) % Formats.length
      content = doc[Formats[data.formatIndex]]
    }while(data.formatIndex != currentFormat &&
      (!content || !content.trim().length))

    // all the way around, use base format
    if(data.formatIndex == currentFormat)
      data.formatIndex = 0
  }
  updatePageFormat()
}

async function mainMenu(){
  return await Menu.Alert("TODO: Main Menu")
}
async function loadUrl(url){
  let success = true
  let loadOption =  await Menu.Choose(["Run", "Edit", "NoScript"], "Load Page")
  if(loadOption == "NoScript"){
    temp.fullscreen_view = true
    temp.closefullscreenbutton = true
    temp.enablejs = false
  }
  if(loadOption == "Run"){
    temp.fullscreen_view = true
    temp.closefullscreenbutton = false
    temp.enablejs = true
  }
  if(loadOption == "Edit"){
    data.viewIndex = 0
    data.formatIndex = 0
    temp.fullscreen_view = false
    temp.enablejs = false
    temp.closefullscreenbutton = true
  }
  let content = await getUrl(url).catch(()=>{
    success = false
  })
  if(!success){
    await Menu.Alert("Url failed to load: " + url, "Url Error")
    return false
  }
  // console.log('content', content)
  let newdoc = parseDocSrc(content)
  data.docList.splice(data.pageIndex, 0, newdoc)
  retrieveDoc()
  show()
  //await Menu.Alert("Url Loaded: " + url, "Load Url Finished")
  //delayRefresh(ResizeDelay)
}
function loadText(text){
  let newdoc = parseDocSrc(text)
  data.docList.splice(data.pageIndex, 0, newdoc)
  retrieveDoc()
  show()
}
function leaveFullscreen(){
  temp.fullscreen_view = false
  show()
}
async function pageMenu(){
  // TODO
  try{
    cancelRefresh()
    stashDoc()
    let choices = ["File", "Lib", "Demo", "Share", "View"]
    if(temp.fullscreen_view){
      temp.fullscreen_view = false
      temp.closefullscreenbutton = false
      return show()
    }

    if(data.pageIndex > 0){
      choices.unshift("<") }
    if(data.pageIndex < data.docList.length - 1){
      choices.push(">") }
    let action = await Menu.Choose(
      choices, `Page ${data.pageIndex + 1}`)

    if(action == "Lib"){
      let libtype = await Menu.Choose(['css', 'js'], "Type")
      let list = ['lib1', 'lib2', 'lib3']
      list.unshift('New')

      for(let i=0; i<list.length; ++i){
        //ensure unique names
        while(list.indexOf(list[i]) < i)
          list[i] += '!'
      }

      let libchoice = await Menu.Choose(list, `${libtype} Libs`)
      if(libchoice == "New"){
        let libname = await Menu.Prompt("Enter Library Name", `${libtype}`)
        let libload = await Menu.Choose(["Text", "URL"], `New ${libtype}lib : ${libname}`)
        let libvalue = ""
        if(libload == "Text"){
          libvalue = await Menu.PromptText("Paste Lib Content")
        }
        if(libload == "URL"){
          libvalue = todoloadurl(await Menu.Prompt("Load URL"))
        }
      } else {
        let libaction = await Menu.Choose(["View", "Delete"], `${libtype}lib: ${libname}`)
      }
    }

    //alert("action:" + action)
    if(action == "<"){
      prevPage()
      return pageMenu() }
    if(action == ">"){
      nextPage()
      return pageMenu() }
    
    if(action == "Share"){
      let share_url = encodeURIComponent(
        await Menu.Prompt("<div style='text-align: left'><b>Enter URL of external source to share an edit link.</b><br><br>For example, try using the \"raw\" source link for a github gist, gitlab snippet, or other pastebin app.</div>", "Share External Url"))
      if(share_url){
        return await Menu.Alert(
         `Right Click -> Copy Link:<br> <a href='${DefaultAppUrl}#url=${share_url}' onclick="return false;">Edit Link</a>`, "Copy Edit Link")
      }
    }
    if(action == "File"){
      let page_action = await Menu.Choose(["New", "Save", "Load", "Delete", "Duplicate"], 
        `Page ${data.pageIndex + 1} - File`)
      if(page_action == "New")
        await newPage()
      if(page_action == "Delete")
        await deletePage()
      if(page_action == "Duplicate")
        await duplicatePage()
      if(page_action == "Save"){
        let target = "Text"
        //let target = await Menu.Choose([/*"Cloud",*/ "Text", "Archive"], "Save Page")
        if(target == "Text"){
          let doc = data.docList[data.pageIndex] ?? emptyDoc()
          let page_src = template(doc, true)
          await Menu.PromptText("Copy Raw Source Below", "Save Page", page_src)
        }

        if(target == "Archive"){
          if(window.noSaveArchive)
            return await Menu.Alert(
              "Cannot Save Archive from PlayStore app,<br>Try the webapp at:<br>" +
              "<a href='https://derekmc.gitlab.io'>derekmc.gitlab.io</a>", "Not Available")
          let name = await Menu.Prompt("File Name", "Save Page Archive")
          if(name) try{
            await stashPage(name)
          } catch(e){
            await Menu.Alert("Could not save archive")
          }
        }
        return show()
      }
      if(page_action == "Load"){
        let source = await Menu.Choose(["Text", "URL",  "Archive"], "Load Page")
        //console.log("load source", source)
        if(source == "Text"){
          //let clipboard_action = await Menu.Choose(["Copy", "Paste"], `Page ${data.pageIndex + 1} - Clipboard`)
          //if(clipboard_action == "Copy"){
            //let copy_action = await Menu.Choose(["page", "data", "data.text"], "Copy What?")
            //if(copy_action == "page") await copyPage()
            //if(copy_action == "data") await copyData()
            //if(copy_action == "data.text") await copyText()
          //}
          // load and parse document from clipboard
          //if(clipboard_action == "Paste") await pastePage()
          let src = await Menu.PromptText("Copy Page Source Below", "Load Text")
          if(src) await loadText(src)
          return show()
        }
        if(source == "URL"){
          let url = await Menu.Prompt("Enter Url to load", "Load External Url")
          if(url) await loadUrl(url)
          return
        }
        if(source == "Archive"){
          // let protocol = window.location.protocol
          //if(!(protocol == "https:" || protocol == "http:")){
          //  let msg = `Page is served over '${protocol}'. Some permissions require http/https.\n` +
          //    "Try anyway?"
          //  if(!await Menu.Confirm(msg, "Unexpected Protocol"))
          //    return
          //}
          await retrievePage()
          return show()
          //await Menu.Alert("TODO")
        }
      }

      return show()
    }
    if(action == "Demo"){
      let demo_type = await Menu.Choose(Object.keys(DemoMenus), "Demo Categories")
      let demo_choices = DemoMenus[demo_type]
      let demo_choice = await Menu.Choose(demo_choices, demo_type)
      let demo_url = DemoUrls[demo_choice]
      console.log('url', demo_url)
      if(demo_url) await loadUrl(demo_url)
      return
    }
    if(action == "Menu"){
      return await mainMenu()
    }
    if(action == "View"){
      let view_action = await Menu.Choose([
        "Style", "Single", "Split", "Fullscreen"],
        `Page ${data.pageIndex + 1} - View`)
      if(view_action == "Fullscreen"){
        temp.enablejs = true
        temp.fullscreen_view = true
        temp.closefullscreenbutton = false
        return show()
      }
      if(view_action == "Single"){
        return selectView("edit")
      }
      if(view_action == "Split"){
        // let toosmall = window.innerWidth < NarrowWidth && window.innerHeight < NarrowWidth
        //return toosmall? await Menu.Alert("Window too small to split.", "Not Allowed") : selectView("split")
        return selectView("split")
      }
      if(view_action == "Style"){
        let style_action = await Menu.Choose(["Font Size", "Color Scheme", "Mirror Image"], `Page ${data.pageIndex + 1} - Style`)
        // console.log("style action: ", style_action)
        if(style_action == "Mirror"){
          let mirror_choice = await Menu.Choose(["Normal", "Reverse"], 'Mirror Image')
          if(mirror_choice == "Normal"){
            document.body.classList.remove("mirror")
          }
          if(mirror_choice == "Reverse"){
            document.body.classList.add("mirror")
          }
        }
        if(style_action == "Font"){
          let font_action
          while(font_action = await Menu.Choose(["Smaller", "Bigger"], FontSizes[data.font_size_index])){
            if(font_action == "Smaller") await smallerFont()
            else if(font_action == "Bigger") await biggerFont()
            else break
          }
          return show()
        }
        if(style_action == "Color"){
          let color_choice = await Menu.Choose(["Day", "Night"], "Color Scheme")
          if(color_choice == "Day") data.night_mode = false
          if(color_choice == "Night") data.night_mode = true
          return show()
        }

      }
    }

  }catch(e){
    alert("err: " + e)
  }
}
function dayMode(){
  data.night_mode = false
  show()
}
function nightMode(){
  data.night_mode = true
  show()
}
function biggerFont(){
  if(data.font_size_index >= FontSizes.length - 1) return
  ++data.font_size_index
  show()
}
function smallerFont(){
  if(data.font_size_index <= 0) return
  --data.font_size_index
  show()
}
function prevPage(){
  if(data.pageIndex == 0) return
  stashDoc()
  --data.pageIndex
  if(data.pageIndex >= data.docList.length) data.pageIndex = 0
  if(data.pageIndex < 0) data.pageIndex = data.docList.length - 1
  //selectFormatWithContent()
  retrieveDoc()
  show()
}
function nextPage(){
  if(data.pageIndex == data.docList.length - 1) return
  stashDoc()
  ++data.pageIndex
  if(data.pageIndex >= data.docList.length) data.pageIndex = 0
  if(data.pageIndex < 0) data.pageIndex = data.docList.length - 1
  //selectFormatWithContent()
  retrieveDoc()
  show()
}
function retrieveDoc(){
  if(data.docList.length <= 0) data.docList = [emptyDoc()]
  if(data.pageIndex >= data.docList.length) data.pageIndex = 0
  if(data.pageIndex < 0) data.pageIndex = data.docList.length - 1
  let doc = data.docList[data.pageIndex] ?? emptyDoc()
  data.docList[data.pageIndex] = doc
  let format = Formats[data.formatIndex]
  edit.value = doc[format]
}
function emptyDoc(){
  let result = {}
  for(let i=0; i < Formats.length; ++i){
    result[Formats[i]] = ""
  }
  result["notedataid"] = randomString(10, util.alphanums)
  return result
}
function stashDoc(){
  // if(data.viewIndex == 1) return // don't stash in view mode
  let text = edit.value
  data.docList[data.pageIndex] = data.docList[data.pageIndex] ?? emptyDoc()
  let format = Formats[data.formatIndex]
  data.docList[data.pageIndex][format] = text
}


function init(){
  //Menu.AlertTry(x=>{throw new Error("asdf: " + x)})
  document.body.innerHTML = renderPage()
  edit.value = ""
  //return
  load(async (success) => {
    let params = new URLSearchParams(window.location.hash.replace("#", "?"))
    let url = params.get('url')
    if(url){
      await loadUrl(url)
      history.pushState("", document.title, window.location.pathname + window.location.search);
    //selectFormatWithContent()
    }else{
      retrieveDoc()
      show()
    }
    edit.addEventListener("change", onEdit)
    edit.addEventListener("keydown", onEdit)
    window.addEventListener("keydown", keydown)
    window.addEventListener("resize", onResize)
    window.addEventListener("message", onMessage)
  })
}
function onMessage(e){
  try{
    let message = JSON.parse(e.data)
    // console.log('received message', message)
    if(message.hasOwnProperty("action") && message.action == "saveData"){
      let msg_data = JSON.parse(message.data)
      let doc = data.docList[data.pageIndex] = data.docList[data.pageIndex] ?? emptyDoc()
      let format = Formats[data.formatIndex]
      let view = Views[data.viewIndex]
      if(view == "edit" && format == "json" && document.activeElement == edit){
        console.log("data is dirty")
        data.dirty = true
        edit.classList.add("dirty")
      } else {
        data.dirty = false
        let data_text = JSON.stringify(msg_data, null, 2)
        if(data_text == "{}") data_text = ""
        doc["json"] = data_text
        if(Formats[data.formatIndex] == "json"){
          edit.value = doc["json"]
        }
        edit.classList.remove("dirty")
        save()
      }
    }
  } catch(e){
    console.warn("error processing message: " + e.data)
  }
}
function keyup(e){
  let key = e.key
  if(key == "Tab" || key == "Escape"){
    return
  }
  // child window gets all keyboard events except tab or escape.
  if(Views[data.viewIndex] == "view"){
    output.contentWindow.postMessage(JSON.stringify({event: "keyup", key: key}))
  }
}
function keydown(e){
  let key = e.key

  // Handle specific menus.
  if(document.body.classList.contains("menuActive")){
    // don't use this key handler when menu is active.
    return
  }

  if(key == "Tab"){
    e.preventDefault()
    if(e.shiftKey) prevFormat()
    else nextFormat()
    document.querySelector("#edit").focus()
    return false
  }
  if(key == "Escape"){
    if(Menu.activeWindow.length == 0){
      // console.log("opening menu")
      e.preventDefault()
      pageMenu()
      return false
    }
  }

  // child window gets all keyboard events except tab or escape.
  if(Views[data.viewIndex] == "view"){
    //console.log("todo: passthrough key to iframe: " + key)
    //output.contentWindow.postMessage(JSON.stringify({event: "keydown", key: key}))
  }
  if(e.ctrlKey){
    // console.log("key", key)
    if(key == "p" || key == " "){
      e.preventDefault()
      pageMenu()
      return false
    }
    /*
    if(e.keyCode == 37){
      e.preventDefault()
      prevPage()
      return false
    }
    if(e.keyCode == 39){
      e.preventDefault()
      nextPage()
      return false
    }
    */
    if(e.key == "Enter"){
      e.preventDefault()
      nextView()
      return false
    }
  }
}
const Key = "HtmlNotesData"
const InstanceKey = "HtmlNotesInstance"
async function save(){
  let datastr = JSON.stringify(data, null, 2)
  if(localStorage.getItem(InstanceKey) == temp.instanceId){
    localStorage.setItem(Key, datastr)
  } else {
    await Menu.Alert("Another HTMLNotes window is open. Cannot save.", "Duplicate Window Error")
  }
}
function load(after){
  let instanceId = localStorage.getItem(InstanceKey)
  if(!(instanceId && instanceId >= 0)){
    instanceId = 0
  }
  instanceId = parseInt(instanceId) + Math.round(1000*Math.random())
  if(instanceId > 10*1000*1000) instanceId -= 10*1000*1000
  localStorage.setItem(InstanceKey, temp.instanceId = instanceId)
  let datastr = localStorage.getItem(Key)

  // console.log("stored data: " + datastr)
  let success = false
  if(datastr){
    try{
      // store in global "data"
      let values = JSON.parse(datastr)
      for(let k in values){
        data[k] = values[k]
      }
    } catch(e){
      console.warn("parse failed for saved data. ")
    }
    retrieveDoc()
    success = true
  }
  after(success)
}


function show(){
  cancelRefresh()
  temp.lastRefresh = Date.now()
  function $(x){ return document.querySelector(x) }

  let narrow = window.innerWidth < NarrowWidth
  let toosmall = narrow && window.innerHeight < NarrowWidth
  //if(!temp.fullscreen_view && toosmall && data.viewIndex == 2)
  //  data.viewIndex = 0
  if(temp.fullscreen_view)
    data.viewIndex = 1

  // set fontsize
  $("#edit").style.fontSize = FontSizes[data.font_size_index]
  let bodyFontIndex = Math.max(MinBodyFontIndex,
    Math.min(MaxBodyFontIndex, data.font_size_index))
  $("#main").style.fontSize = FontSizes[bodyFontIndex]
  $("#dataline").style.fontSize = FontSizes[bodyFontIndex]
  //deletebutton.style.display = narrow? "none" : "" 
  let firstpage = data.pageIndex==0
  let lastpage = data.pageIndex == data.docList.length -1

  setClass($("#prevbutton"), "disabled", firstpage)
  setClass($("#nextbutton"), "disabled", lastpage)
  // console.log(`first: ${firstpage}, Last: ${lastpage}`)

  pagebutton.innerHTML =
     `${narrow? '' : 'Page '}` +
     `${data.pageIndex + 1}`
  let landscape = window.innerWidth > window.innerHeight

  if(data.viewIndex == 1 && data.formatIndex != 0){
    data.formatIndex = 0
    retrieveDoc()
  }
  //viewbutton.innerHTML = ["Edit", "View", "Split"][data.viewIndex]

  let maindiv = document.querySelector("#main")

  let fullscreen = temp.fullscreen_view? "fullscreen_view" : "fullscreen"
  maindiv.className = [
    "edit " + fullscreen,
    "view " + fullscreen,
    landscape? "hsplit" : "vsplit",
  ][data.viewIndex] ?? ""
  if(temp.closefullscreenbutton){
    document.body.classList.add("showclose")
  } else {
    document.body.classList.remove("showclose")
  }
  if(data.night_mode){
    maindiv.classList.add("night")
    document.body.classList.add("night")
  } else{
    //maindiv.classList.add("night")
    document.body.classList.remove("night")
  }


  if(!temp.enablejs)
    maindiv.classList.add("disablejs")

  let htmlbracket =
    !(data.viewIndex == 1 && data.formatIndex == 0 || narrow && data.formatIndex != 0)

  document.querySelector(".format0").innerHTML =
    (data.viewIndex == 2? "htmd" :
      (htmlbracket? "&lt;htmd&gt;" : "&nbsp;<u>htmd</u>&nbsp;"))
                     
  // add or remove narrow class
  setClass(maindiv, "narrow", narrow)

  if(data.viewIndex > 0) render()
  else clear()
  // data is no longer dirty.
  edit.classList.remove("dirty")
  data.dirty = false
  updatePageFormat()
}

async function stashPage(name){
  let extension = ".html"
  let filename = name.endsWith(extension)?
    name : name + extension

  let fileprefix = filename.substring(0, filename.length - extension.length)
  // let zipname = prefix + ".zip"
  let zipname = fileprefix + "_htmlnotes"
  let now = new Date()
  let timestamp_suffix = "_" + now.getFullYear() + "_" +
    ("0" + (now.getMonth() + 1)).slice(-2) + now.getDate() + "_" +
    ("0" + now.getHours()).slice(-2) + now.getMinutes()

  let folder2_name = fileprefix + timestamp_suffix

  let doc = data.docList[data.pageIndex] ?? emptyDoc()
  let src = template(doc, true)
  let zip = new JSZip()
  let folder = zip.folder("htmlnotes")
  let folder2 = folder.folder(folder2_name)
  //try{
  //  let serverpy_content  = await downloadFile("./server.py")
  //  folder2.file("server.py", serverpy_content) 
  //} catch (e){
  //  console.warn("Cannot load server.py")
  //}
  folder2.file(fileprefix + ".html", src)
  let content = await zip.generateAsync({type:"blob"})
  saveAs(content, zipname)
}

function fileReaderPromise(file, type){
  type = type ?? "text"
  return new Promise((yes, no)=>{
    let reader = new FileReader()
    reader.onload = ()=>{
      yes(reader.result)
    }
    reader.onerror = (e)=>{
      no(e)
    }
    if(type == "text"){
      reader.readAsText(file)
    }
    if(type == "arraybuffer"){
      reader.readAsArrayBuffer(file)
    }
    if(type == "binarystring"){
      reader.readAsBinaryString(file)
    }
    if(type == "dataurl"){
      reader.readAsDataUrl(file)
    }
  })
}

function parseDocSrc(src){
  let htmlprefix = `<${''}!-- HTML --${''}>`
  let cssprefix = `\/${''}* CSS *${''}\/`
  let jsprefix = `\/${''}* JS *${''}\/`
  let jsonprefix = `\/${''}* JSON *${''}\/`
  let jslibprefix = `\/${''}* JSLIB:\s*(\w*) *${''}\/`
  let csslibprefix = `\/${''}* CSSLIB:\s*(\w*) *${''}\/`
  let end1 = `<${''}!-- END --${''}>`
  let end2 = `\/${''}* END *${''}\/`
  let doc = emptyDoc()

  // get each section of document.
  let i = 0, j = 0
  let block = ""

  let has_section = false

  // html
  i = src.indexOf(htmlprefix)
  j = i + src.substr(i).indexOf(end1)
  if(i >= 0 && j >= i){
    block = src.substring(
      i + htmlprefix.length, j)
    block = block.trim()
    doc["html"] = block
    has_section = true
  }

  // json
  i = src.indexOf(jsonprefix)
  j = i + src.substr(i).indexOf(end2)
  if(i >= 0 && j >= 0){
    block = src.substring(i + jsonprefix.length, j)
    block = block.trim()
    if(block.length && block[block.length - 1] == ";"){
      block = block.substring(0, block.length - 1)
    }
    doc["json"] = block
    has_section = true
  }

  // js
  i = src.indexOf(jsprefix)
  j = i + src.substr(i).indexOf(end2)
  if(i >= 0 && j >= i){
    block = src.substring(i + jsprefix.length, j)
    block = block.trim()
    doc["js"] = block
    has_section = true
  }

  // css
  i = src.indexOf(cssprefix)
  j = i + src.substr(i).indexOf(end2)
  if(i >= 0 && j >= i){
    block = src.substring(i + cssprefix.length, j)
    block = block.trim()
    doc["css"] = block
    has_section = true
  }
  //csslib
  for(let start=0; start < src.length; ){
    i = src.indexOf(csslibprefix, start)
    j = src.substr(i).indexOf(end2)
    if(i>=0 && j>=0){
      start = i + j + end2.length
      let libname = src.match(csslibprefix)[1]
      block = src.substr(i + cssprefix.length, j)
      block = block.trim()
      doc.csslib ??= {}
      doc.csslib[libname] = block
      has_section = true
    } else {
      break
    }
  }
  // jslib
  
  for(let start=0; start < src.length; ){
    i = src.indexOf(jslibprefix)
    j = src.substr(i).indexOf(end2)
    if(i>=0 && j>=0){
      start = i + j + end2.length
      let libname = src.match(jslibprefix)[1]
      block = src.substr(i + jsprefix.length, j)
      block = block.trim()
      doc.jslib ??= {}
      doc.jslib[libname] = block
      has_section = true
    } else {
      break
    }
  }
  if(!has_section){
    // no content parsed for any format,
    // so we load content into the first format.
    doc[Formats[0]] = src
  }
  // console.log("parsed doc", doc)
  return doc
}


async function retrievePage(){
  let filelist = await Menu.PromptFile("Load Zip", "Choose File")
  if(!filelist || !filelist.length || !filelist[0]){
    console.warn("archive->retrieve: no file chosen:", filelist)
    return
  }
  let file = filelist[0]
  let extension = ".html"
  let filedata = await fileReaderPromise(file, "arraybuffer")

  let ziplib = new JSZip()
  let zip = await ziplib.loadAsync(filedata)
  //console.log(zip)
  for(let name in zip.files){
    //console.log("name", name, "file", file)
    file = zip.files[name]
    if(name.endsWith(extension)){
      let filesrc = await file.async("string")
      let newdoc = parseDocSrc(filesrc)
      data.docList.splice(data.pageIndex, 0, newdoc)
      retrieveDoc()
      show()
    }
  }
  //return await Menu.Alert("Retrieve Page Not Implemented", "TO DO")
}

function newPage(){
  let newdoc = emptyDoc()

  data.docList.splice(data.pageIndex, 0, newdoc)
  retrieveDoc()
  show()
}

function duplicatePage(){
  let olddoc = data.docList[data.pageIndex]
  let newdoc = emptyDoc()
  for(let i=0; i < Formats.length; ++i){
    newdoc[Formats[i]] = olddoc[Formats[i]]
  }

  data.docList.splice(data.pageIndex, 0, newdoc)
  retrieveDoc()
  show()
}
async function deletePage(){
  if(await Menu.Confirm("Delete Page?")){
    if(data.docList.length <= 1){
      data.docList = [emptyDoc()]
      data.pageIndex = 0
      retrieveDoc()
      show()
      return
    }
    if(data.pageIndex < 0){
      data.pageIndex = 0
      console.warn("page index below zero")
      return
    }
    if(data.pageIndex >= data.docList.length){
      data.pageIndex = data.docList.length - 1
      console.warn("page index out of bounds")
      return
    }

    data.docList.splice(data.pageIndex, 1)
    if(data.pageIndex >= data.docList.length){
      data.pageIndex = data.docList.length - 1
    }
    retrieveDoc()
    show()
  }
}
function selectView(name){
  let i = Views.indexOf(name)
  if(i < 0) console.warn("unknown view: " + name)
  stashDoc()
  data.viewIndex = i
  show()
}
function nextView(){
  stashDoc()
  data.viewIndex = (data.viewIndex + 1)%3
  show()
}
function clear(){
  output.srcdoc = ""
  edit.classList.remove("dirty")
}
function render(){
  stashDoc()
  let doc = data.docList[data.pageIndex] ?? emptyDoc()
  data.docList[data.pageIndex] = doc
  output.srcdoc = template(doc)
  save()
}
async function copyPage(){
  try{
    let doc = data.docList[data.pageIndex] ?? emptyDoc()
    let src = template(doc, true)
    navigator.clipboard.writeText(src)
  } catch(e){
    console.error("Clipboard Copy error.", e)
    await Menu.Alert("" + e, "Copy Failed")
    return
  }
  await Menu.Alert("Copied Page to Clipboard", "Copy")
}
// copies data.text
async function copyText(){
  try{
    let doc = data.docList[data.pageIndex] ?? emptyDoc()
    let s = JSON.parse(doc['json']).text
    navigator.clipboard.writeText(s)
  } catch(e){
    console.error("Clipboard Copy error.", e)
    await Menu.Alert("" + e, "Copy Failed")
    return
  }
  await Menu.Alert("Copied 'data.text' to Clipboard", "Copy")
}
async function copyData(){
  try{
    let doc = data.docList[data.pageIndex] ?? emptyDoc()
    let s = doc['json']
    navigator.clipboard.writeText(s)
  } catch(e){
    console.error("Clipboard Copy error.", e)
    await Menu.Alert("" + e, "Copy Failed")
    return
  }
  await Menu.Alert("Copied 'data' to Clipboard", "Copy")
}
async function pastePage(){
  try{
    let src = await navigator.clipboard.readText()
    let newdoc = parseDocSrc(src)
    data.docList.splice(data.pageIndex, 0, newdoc)
    retrieveDoc()
    show()
  } catch(e){
    console.error("Paste error.", e)
    await Menu.Alert("" + e, "Paste Failed")
    return
  }
}
function updatePageFormat(){
  let maindiv = document.querySelector("#main")
  maindiv.classList.remove("pageformat0")
  maindiv.classList.remove("pageformat1")
  maindiv.classList.remove("pageformat2")
  maindiv.classList.remove("pageformat3")
  maindiv.classList.add(`pageformat${data.formatIndex}`)
}
async function selectFormat(index){
  stashDoc()
  if(data.formatIndex == 2 && index == 2 || !temp.enablejs && index == 2){
    if(temp.enablejs){
      document.querySelector("#main").classList.add("disablejs")
      temp.enablejs = ("Enable" == await Menu.Choose(["Enable", "Disable"], "Enable Javascript?"))
    } else {
      document.querySelector("#main").classList.remove("disablejs")
      temp.enablejs = ("Enable" == await Menu.Choose(["Enable", "Disable"], "Enable Javascript?"))
    }
  }
  if(index == 0 && data.formatIndex == 0){
    // if html is clicked, toggle edit/view
    if(data.viewIndex < 2)
      data.viewIndex = (data.viewIndex + 1) % 2
  } else if(data.viewIndex == 0){
    // when a html is clicked once, on narrow screens, render page, otherwise show the source.
    if(index == 0)
      data.viewIndex = 1
    else
      data.viewIndex = 0
  } else if(data.viewIndex == 1){
    data.viewIndex = 0
  }
  data.formatIndex = index % Formats.length
  retrieveDoc()
  updatePageFormat()
  show()
}
function nextFormat(){
  stashDoc()
  if(data.formatIndex == 2 && !temp.enablejs) selectFormat(2) // enablejs on tabthrough
  else if(data.formatIndex == 0 && data.viewIndex == 1) selectFormat(0)
  else if(data.formatIndex == Formats.length - 1){
    if(data.viewIndex < 2) data.viewIndex = 0
    selectFormat(0)
  }else{
    selectFormat(data.formatIndex + 1)
  }
}
function prevFormat(){
  stashDoc()
  //if(data.formatIndex == 2 && !temp.enablejs){  // enable js on tab through
  //      selectFormat(2)
  //} else
  if(data.formatIndex == 1 && data.viewIndex == 0){ // on the html format, cycle through view and edit.
    data.viewIndex = 1
    selectFormat(0)
  } else if(data.formatIndex == 0 && data.viewIndex == 0){
    data.viewIndex = 0
    selectFormat(0)
  } else {
    selectFormat((data.formatIndex + Formats.length - 1) % Formats.length)
  }
}
let updateTimeout = null
function onEdit(){
  delayRefresh(EditDelay)
}
function onResize(){
  delayRefresh(ResizeDelay)
}
function cancelRefresh(){
  if(updateTimeout) clearTimeout(updateTimeout)
  updateTimeout = null
}
function delayRefresh(delay){
  if(updateTimeout) clearTimeout(updateTimeout)
  let now = Date.now()
  if(now - temp.lastRefresh + delay < MinRefreshInterval){
    return }
  updateTimeout = setTimeout(show, delay)
  if(Formats[data.formatIndex] == "json"){
    clear()
  }
}
