
Objects are one of the hardest things to design, because you mix many layers of scope,
static properties are essentially modularized global variables, and static
methods are similarly general purpose functions.

Having too many different objects is a terrible design decision, because
objects create isolation between program parts, but having no objects
makes maintenance more difficult, precisely for that reason, there is
no isolation so it is hard to reason about the code.

The Effective isolation principle:

Objects should correspond to abstracted visual components, meaning
they have isolated state not directly related to other parts of the program.

For example, a menu would be a good example of an isolated visual component,
but a radio button would not be, because the state of the radio button is not
effectively isolated, but the state of an entire menu is effectively isolated.

All visual componenents require a full blown object. If you don't

Making deliberate decisions is how you can learn(which are related to things
you care about). General life principle.

Sequence of Object based program design
=================
 - Global variables and procedures, a procedure can modify global state
 - structured globals, (data, temp)
 - an impure function only modifies passed variables, but it can be affected
   by global variables
 - a pure function modifies nothing, it only returns a value, and is not
   affected by global variables.
 - procedure -> impure function -> pure functions
 - not all procedures need to be converted, because we have objects!
 - An object creates an isolated context for global state, so we can
   have "boxed globals", both instance and static.
 - pure functions should be static on objects, because they don't
   depend on object state directly.
 - factoring complex logic into pure functions and making them static,
   makes it easier to debug that complicated logic.
 - types are a second priority after factoring complex logic into pure
   functions that can be tested.
 - just like you don't have to factor all procedures, you don't need to
   typecheck all values. It makes most sense for the most complex and mathematical
   parts of your program.


Minimal debugging tools
================

 - If you can write a whole program in one go, and then debug later, that reduces
   time. If you don't need a syntax checker, that reduces complexity. Use the fewest tools
   you can get away with.
 - Incremental testing is not the same thing as an intermittent(interrupted) development flow.
   Incremental testing can be done even if you write many parts of your program in the initial step,
 - pure functions are the easiest to test incrementally, but the hardest to design.
 - code comments are your friend in minimal development. rely on binary search tactics.
   comment out as much as you can until everything is working, then uncomment half of what you just commented.

Benefits of objects
=================

- modularized naming, the names are relative to the object.

- objects allow for a soft 'persistence' so you can setup a call in mulitple
steps, before performing an action. This helps a lot if you have many potential
options or properties related to a function call.

