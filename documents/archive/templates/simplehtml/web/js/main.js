
window.addEventListener("load", simple_main);

function simple_main(){

  init();

  function init(){
    let state = {x: 30, y:30, message: "Simple HTML Program"};
    let ui = {};
    ui.container = document.querySelector("#message_container");

    console.log("in simple_main.init");
    render(state, ui);
  }

  function render(state, ui){
    ui.container.innerHTML = "<h5 style='padding: 0 4px;'>" + state.message + "</h5>";
  }
}
