#!/usr/bin/env python

from flask import Flask, request
app = Flask(__name__)
import os

homepage_path = ["pages", "home.html"]
form_path = ["pages", "form.html"]

with open(os.path.join(*homepage_path), "r") as file:
  homepage = file.read()
  
with open(os.path.join(*form_path), "r") as file:
  formpage = file.read()


@app.route('/')
def home():
  return homepage

@app.route('/form', methods=["GET", "POST"])
def form():
  page = formpage
  text_value = request.form.get("text_value", "")
  print("text_value", text_value)
  page = page.replace("{{text_value}}", text_value)
  return page

if __name__ == "__main__":
  app.debug = True
  app.run()
