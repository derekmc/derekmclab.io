

Okay, I have set it up so that I can type
on my phone even with the screen not
visible.  The point of that is not
having a screen at all, is in fact the
most ergonomic setup.

In fact, I can simply turn up, or down
the brightness with my keyboard.  It is
difficult to tywork outside, when the
sund can create quite a glare.

One trick I have discovered is to shield
the phone from the sun using a shoe.

You pretty much always have a shoe on
you, and when you are working on your
phone you are likely not even using it.
another added bonus is that screen glare
typically comes from the thing opposite
your screen, whatever would be reflected
by the screen if it were a mirror,
because in essence, that's what it is.

By putting your phone in your shoe, it
is situated at an angle where the
reflection is not on any bright or
lighted area, which avoids glare.

Once you put your phone like that, the
challenge is that your are viewing it at
an angle, so the text is squashed.  So
it is also helpful to be able to adjust
the height of text in your editor, so
that even when viewing your phone from
an extremely shallow angle, almost
parallel to the screen the text is still
legible and mostly square.
