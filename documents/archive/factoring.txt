

# Factoring
## How reorganizing code is a superpower

Factoring is one of the hardest things you have to do in web
development.

If you start with your code too fractured between different
parts, then it will be hard to get a sense of the structure
and design you are using.  Similarly, if you continue development
without separation, then it will be difficult to fill in the details
you need.

## Factor once

The best way to factor is to do it once, and then never look back.


## Project lifcycles and growth



## Incremental balanced growth

The biggest trick in factoring well, is to find a proper lifecycle
for your projects, and then to development them at a proper rate.

If you grow your projects too quickly...
too slowly
I
